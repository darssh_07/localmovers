<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="noindex, nofollow">
<style type="text/css">
@import url('https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Roboto+Slab:300,400,700&display=swap');

* {
    box-sizing: border-box;
    outline: none;
    margin: 0;
    padding: 0;
    font-family: 'Open Sans', sans-serif;
}

html,
body {
    margin: 0px;
    padding: 0;
    height: 100%;
    min-height: 100%;
    text-size-adjust: none;
    -webkit-text-size-adjust: none;
}

body {
    background: #F1F8FC;
}

figure {
    display: inline-block;
    outline: none;
}

.full-width {
    float: left;
    width: 100%;
    position: relative;
}

header {
    text-align: center;
    height: 84px;
    box-shadow: 0 1px 5px 0 rgba(5,5,5,0.30);
}

header,
footer {
    background: #FFF; 
}

header figure {
    width: 173px;
    height: 49px;
    background: url("../../../img/2-movers-logo.svg") no-repeat center;
    background-size: 100%;
    margin-top: 20px;
}

.content {
    padding-top: 125px;
    text-align: center;
}

.tick-container {
    text-align: center;
    padding-bottom: 30px;
}

.tick-container figure {
    width: 75px;
    height: 75px;
    background-size: 100% !important; 
    background: url("../../../img/2movers-check.svg") no-repeat center;
}

.text-content {
    padding-bottom: 40px;
}

.text-content span {
    display: block;
    text-align: center;
    padding-bottom: 15px;
    font-family: 'Roboto Slab', serif;
    font-size: 30px;
    color: #000000;
}

.text-content p {
    display: block;
    text-align: center;
    font-size: 18px;
    color: #9B9B9B;
    letter-spacing: 0.23px;
    line-height: 30px;
}

.movers-photo {
    text-align: center;
    padding-bottom: 50px;
}
.movers-photo figure {
    width: 200px;
    height: 170px;
    background: url("../../../img/2movers-truck.svg") no-repeat center;
    background-size: 100%; 
}

.footer-logos {
    text-align: center;
}

.footer-logos img {
    display: inline-block;
    margin: 0 12px;
    vertical-align: middle;
}


footer {
    position: absolute;
    left: 0;
    bottom: 0;
    text-align: center;
    width: 100%;
    height: 74px;
    padding-top: 27px;
    color: #8EB9C1;
}

footer a {
    display: inline-block;
    font-family: 'Roboto Slab', serif;
    color: #8EB9C1;
    font-size: 12px;
    padding: 0 22px;
    line-height: 22px;
    cursor: pointer;
}

footer a:hover {
    text-decoration: underline;
}

footer a:first-child {
    border-right: 1px solid #8EB9C1;
}

/****| Popup CSS |*****/

.popup {
    width: 100%;
    height: 100%;
    position: fixed;
    top: 0px;
    left: 0px;
    background: rgba(21, 47, 68, 0.75);
    z-index: 9999;
    display: none;
    overflow: auto;
}

.popup-inner {
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    width: 90%;
    padding: 20px;
    background: #fff;
    box-shadow: 0 4px 10px 0 rgba(0, 0, 0, 0.26);
    -webkit-box-shadow: 0 4px 10px 0 rgba(0, 0, 0, 0.26);
    border-radius: 10px;
    -webkit-border-radius: 10px;
    z-index: 9999;
    max-width: 600px;
    overflow: hidden;
}

.popup-close {
    position: absolute;
    top: 20px;
    right: 20px;
    width: 13px;
    height: 13px;
    background: url(../images/x.svg) no-repeat;
    background-size: cover;
    z-index: 9999;
    cursor: pointer;
}

.terms-privacy {
    overflow: auto;
}

.terms-privacy .title {
    padding: 10px 0;
    font-size: 24px;
    font-weight: bold;
    color: #3B3C3D;
    letter-spacing: -0.07px;
    line-height: 24px;
}

.terms-privacy .content {
    height: 565px;
    overflow: auto;
    padding: 0 20px 0 0;
    text-align: left;
}

.terms-privacy .content b {
    color: #333;
    font-size: 14px;
}

.terms-privacy .content p {
    display: block;
    padding-bottom: 15px;
    font-size: 14px;
    font-weight: bold;
    color: #CCCCCC;
    line-height: 18px;
    font-family: 'Lato', sans-serif;
    font-weight: 400;
}

.terms-privacy .content ul {
    padding-bottom: 15px;
}

.terms-privacy .content ul li {
    font-size: 14px;
    color: #CCCCCC;
    line-height: 18px;
}

.terms-privacy .content::-webkit-scrollbar-track {
    background-color: #F7F7F7;
}

.terms-privacy .content::-webkit-scrollbar {
    width: 6px;
}

.terms-privacy .content::-webkit-scrollbar-thumb {
    background-color: rgba(216, 216, 216, 0.61);
    border-radius: 25px;
    -webkit-border-radius: 25px;
}

.terms-privacy .content::-webkit-scrollbar:horizontal {
    height: 6px;
}


/****| Responsive CSS |*****/

@media (max-height: 900px) {
    .content {
        padding-top: 30px;
    }
    .tick-container {
        padding-bottom: 20px;
    }
    .text-content {
        padding-bottom: 30px;
    }

}

@media only screen and (max-width:767px) {
    
    header,
    footer {
        height: 15.7333vw;
    }
    header figure {
        width: 27.7333vw;
        height: 7.7333vw;
        margin-top: 4.8vw;
    }
    
    .content {
        padding-top: 18.6667vw;
    }
    
    .tick-container {
        padding-bottom: 6.9333vw;
    }
    
    .tick-container figure {
        width: 14.6667vw;
        height: 14.6667vw;
    }
    
    .text-content span {
        font-size: 6.4vw;
    }
    .text-content p {
        font-size: 4vw;
        line-height: 6.4vw;
        padding: 0 9.3333vw;
    }
    
    .text-content p br {
        display: none;
    }
    
    .movers-photo {
        padding-bottom: 4.2667vw;
    }
    .movers-photo figure {
        width: 53.3333vw;
        height: 45.3333vw;
    }
    
    .popup-inner {
        top: 5vh;
        left: 5%;
        transform: none;
        -webkit-transform: none;
        width: 90%;
    }

    .terms-privacy {
        height: 90vh;
    }

    .terms-privacy .content {
        height: 77vh;
    }
    
    footer {
        padding-top: 4.5333vw;
    }
}

@media only screen and (max-width:360px) {
 
    .content {
        padding-top: 12vw;
    }
    
}
</style>
<title>Thank You</title>
</head>
<body>
<header class="full-width">
    <figure></figure>
</header>

<div class="full-width content">
    
    <div class="full-width tick-container">
        <figure></figure>
    </div>
    
    <div class="full-width text-content">
        @if($response == 'yes') 
            <span>Request received</span>
            <p>Professional movers will contact you shortly <br/>with your quote.</p>
        @else
            <span>Thank You.</span>
            <p>We’ve updated your request status. Thanks for using 2Movers!</p>    
        @endif
    </div>
    
    <div class="full-width movers-photo">
        <figure></figure>
    </div>
    
    <div class="footer-logos">
        <img src="../../../img/shopper-approved.svg" alt="Shopper Approved" height="35" width="98"><img src="../../../img/verisign-darker.svg" alt="Verisign" height="32" width="74">
    </div>
    
</div>

<footer>
    <a data-popup-open="privacy-policy">Privacy Policy</a><a data-popup-open="terms-conditions">Terms of Use</a>
</footer>

<!--| Terms &amp; Conditions |-->

<div class="popup" data-popup="terms-conditions">
    <div class="popup-inner terms-privacy">
        <a class="popup-close" data-popup-close="terms-conditions"></a>
        <div class="full-width title">Terms &amp; Conditions</div>
        <div class="full-width content">
            <p><b>Agreement between User and www.2movers.com</b></p>
            <p>Welcome to www.2movers.com. The www.2movers.com website (the "Site") is comprised of various web pages operated by Two Movers Network. www.2movers.com is offered to you conditioned on your acceptance without modification of the terms, conditions, and notices contained herein (the "Terms"). Your use of www.2movers.com constitutes your agreement to all such Terms. Please read these terms carefully, and keep a copy of them for your reference. </p>
            <p>www.2movers.com is an E-Commerce Site.</p>
            <p>Connect Shippers With Moving Companies</p>

            <p><b>Privacy</b></p>
            <p>Your use of www.2movers.com is subject to Two Movers Network's Privacy Policy. Please review our Privacy Policy, which also governs the Site and informs users of our data collection practices. </p>

            <p><b>Electronic Communications</b></p>
            <p>Visiting www.2movers.com or sending emails to Two Movers Network constitutes electronic communications. You consent to receive electronic communications and you agree that all agreements, notices, disclosures and other communications that we provide to you electronically, via email and on the Site, satisfy any legal requirement that such communications be in writing. </p>

            <p><b>Children Under Thirteen</b></p>
            <p>Two Movers Network does not knowingly collect, either online or offline, personal information from persons under the age of thirteen. If you are under 18, you may use www.2movers.com only with permission of a parent or guardian. </p>

            <p><b>Links to Third Party Sites/Third Party Services</b></p>
            <p>www.2movers.com may contain links to other websites ("Linked Sites"). The Linked Sites are not under the control of Two Movers Network and Two Movers Network is not responsible for the contents of any Linked Site, including without limitation any link contained in a Linked Site, or any changes or updates to a Linked Site. Two Movers Network is providing these links to you only as a convenience, and the inclusion of any link does not imply endorsement by Two Movers Network of the site or any association with its operators. </p>
            <p>Certain services made available via www.2movers.com are delivered by third party sites and organizations. By using any product, service or functionality originating from the www.2movers.com domain, you hereby acknowledge and consent that Two Movers Network may share such information and data with any third party with whom Two Movers Network has a contractual relationship to provide the requested product, service or functionality on behalf of www.2movers.com users and customers. </p>

            <p><b>No Unlawful or Prohibited Use/Intellectual Property</b></p>
            <p>You are granted a non-exclusive, non-transferable, revocable license to access and use www.2movers.com strictly in accordance with these terms of use. As a condition of your use of the Site, you warrant to Two Movers Network that you will not use the Site for any purpose that is unlawful or prohibited by these Terms. You may not use the Site in any manner which could damage, disable, overburden, or impair the Site or interfere with any other party's use and enjoyment of the Site. You may not obtain or attempt to obtain any materials or information through any means not intentionally made available or provided for through the Site. </p>
            <p>All content included as part of the Service, such as text, graphics, logos, images, as well as the compilation thereof, and any software used on the Site, is the property of Two Movers Network or its suppliers and protected by copyright and other laws that protect intellectual property and proprietary rights. You agree to observe and abide by all copyright and other proprietary notices, legends or other restrictions contained in any such content and will not make any changes thereto. </p>
            <p>You will not modify, publish, transmit, reverse engineer, participate in the transfer or sale, create derivative works, or in any way exploit any of the content, in whole or in part, found on the Site. Two Movers Network content is not for resale. Your use of the Site does not entitle you to make any unauthorized use of any protected content, and in particular you will not delete or alter any proprietary rights or attribution notices in any content. You will use protected content solely for your personal use, and will make no other use of the content without the express written permission of Two Movers Network and the copyright owner. You agree that you do not acquire any ownership rights in any protected content. We do not grant you any licenses, express or implied, to the intellectual property of Two Movers Network or our licensors except as expressly authorized by these Terms. </p>

            <p><b>International Users</b></p>
            <p>The Service is controlled, operated and administered by Two Movers Network from our offices within the USA. If you access the Service from a location outside the USA, you are responsible for compliance with all local laws. You agree that you will not use the Two Movers Network Content accessed through www.2movers.com in any country or in any manner prohibited by any applicable laws, restrictions or regulations. </p>

            <p><b>Indemnification</b></p>
            <p>You agree to indemnify, defend and hold harmless Two Movers Network, its officers, directors, employees, agents and third parties, for any losses, costs, liabilities and expenses (including reasonable attorney's fees) relating to or arising out of your use of or inability to use the Site or services, any user postings made by you, your violation of any terms of this Agreement or your violation of any rights of a third party, or your violation of any applicable laws, rules or regulations. Two Movers Network reserves the right, at its own cost, to assume the exclusive defense and control of any matter otherwise subject to indemnification by you, in which event you will fully cooperate with Two Movers Network in asserting any available defenses. </p>

            <p><b>Class Action Waiver</b></p>
            <p>Any arbitration under these Terms and Conditions will take place on an individual basis; class arbitrations and class/representative/collective actions are not permitted. THE PARTIES AGREE THAT A PARTY MAY BRING CLAIMS AGAINST THE OTHER ONLY IN EACH'S INDIVIDUAL CAPACITY, AND NOT AS A PLAINTIFF OR CLASS MEMBER IN ANY PUTATIVE CLASS, COLLECTIVE AND/ OR REPRESENTATIVE PROCEEDING, SUCH AS IN THE FORM OF A PRIVATE ATTORNEY GENERAL ACTION AGAINST THE OTHER. Further, unless both you and Two Movers Network agree otherwise, the arbitrator may not consolidate more than one person's claims, and may not otherwise preside over any form of a representative or class proceeding. </p>

            <p><b>Liability Disclaimer</b></p>
            <p>THE INFORMATION, SOFTWARE, PRODUCTS, AND SERVICES INCLUDED IN OR AVAILABLE THROUGH THE SITE MAY INCLUDE INACCURACIES OR TYPOGRAPHICAL ERRORS. CHANGES ARE PERIODICALLY ADDED TO THE INFORMATION HEREIN. TWO MOVERS NETWORK AND/OR ITS SUPPLIERS MAY MAKE IMPROVEMENTS AND/OR CHANGES IN THE SITE AT ANY TIME. </p>
            <p>TWO MOVERS NETWORK AND/OR ITS SUPPLIERS MAKE NO REPRESENTATIONS ABOUT THE SUITABILITY, RELIABILITY, AVAILABILITY, TIMELINESS, AND ACCURACY OF THE INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS CONTAINED ON THE SITE FOR ANY PURPOSE. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, ALL SUCH INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS ARE PROVIDED "AS IS" WITHOUT WARRANTY OR CONDITION OF ANY KIND. TWO MOVERS NETWORK AND/OR ITS SUPPLIERS HEREBY DISCLAIM ALL WARRANTIES AND CONDITIONS WITH REGARD TO THIS INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS, INCLUDING ALL IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. </p>
            <p>TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL TWO MOVERS NETWORK AND/OR ITS SUPPLIERS BE LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF USE, DATA OR PROFITS, ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OR PERFORMANCE OF THE SITE, WITH THE DELAY OR INABILITY TO USE THE SITE OR RELATED SERVICES, THE PROVISION OF OR FAILURE TO PROVIDE SERVICES, OR FOR ANY INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS OBTAINED THROUGH THE SITE, OR OTHERWISE ARISING OUT OF THE USE OF THE SITE, WHETHER BASED ON CONTRACT, TORT, NEGLIGENCE, STRICT LIABILITY OR OTHERWISE, EVEN IF TWO MOVERS NETWORK OR ANY OF ITS SUPPLIERS HAS BEEN ADVISED OF THE POSSIBILITY OF DAMAGES. BECAUSE SOME STATES/JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, THE ABOVE LIMITATION MAY NOT APPLY TO YOU. IF YOU ARE DISSATISFIED WITH ANY PORTION OF THE SITE, OR WITH ANY OF THESE TERMS OF USE, YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE USING THE SITE. </p>


            <p><b>Termination/Access Restriction</b></p>
            <p>Two Movers Network reserves the right, in its sole discretion, to terminate your access to the Site and the related services or any portion thereof at any time, without notice. To the maximum extent permitted by law, this agreement is governed by the laws of the State of Wyoming and you hereby consent to the exclusive jurisdiction and venue of courts in Wyoming in all disputes arising out of or relating to the use of the Site. Use of the Site is unauthorized in any jurisdiction that does not give effect to all provisions of these Terms, including, without limitation, this section. </p>
            <p>You agree that no joint venture, partnership, employment, or agency relationship exists between you and Two Movers Network as a result of this agreement or use of the Site. Two Movers Network's performance of this agreement is subject to existing laws and legal process, and nothing contained in this agreement is in derogation of Two Movers Network's right to comply with governmental, court and law enforcement requests or requirements relating to your use of the Site or information provided to or gathered by Two Movers Network with respect to such use. If any part of this agreement is determined to be invalid or unenforceable pursuant to applicable law including, but not limited to, the warranty disclaimers and liability limitations set forth above, then the invalid or unenforceable provision will be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision and the remainder of the agreement shall continue in effect. </p>
            <p>Unless otherwise specified herein, this agreement constitutes the entire agreement between the user and Two Movers Network with respect to the Site and it supersedes all prior or contemporaneous communications and proposals, whether electronic, oral or written, between the user and Two Movers Network with respect to the Site. A printed version of this agreement and of any notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or relating to this agreement to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form. It is the express wish to the parties that this agreement and all related documents be written in English. </p>

            <p><b>Changes to Terms</b></p>
            <p>Two Movers Network reserves the right, in its sole discretion, to change the Terms under which www.2movers.com is offered. The most current version of the Terms will supersede all previous versions. Two Movers Network encourages you to periodically review the Terms to stay informed of our updates. </p>

            <p><b>Contact Us</b></p>
            <p>Two Movers Network welcomes your questions or comments regarding the Terms:</p>
            <address class="address">Two Movers Network<br>
                412 N. Main St Ste 100 <br>
                Buffalo, Wyoming 82834
            </address>

            <address class="address">Email Address: <br>
                info@2movers.com <br>
                Telephone number: <br>
                877-773-7350  <br>
                Effective as of July 01, 2017 
            </address>
        </div>
    </div>
</div>

<!--| Privacy Policy |-->

<div class="popup" data-popup="privacy-policy">
    <div class="popup-inner terms-privacy">
        <a class="popup-close" data-popup-close="privacy-policy"></a>
        <div class="full-width title">Privacy Policy</div>
        <div class="full-width content">
            <p>Protecting your private information is our priority. This Statement of Privacy applies to www.2movers.com and Two Movers Network and governs data collection and usage. For the purposes of this Privacy Policy, unless otherwise noted, all references to Two Movers Network include www.2movers.com. The Two Movers Network website is a Moving Company Network site. By using the Two Movers Network website, you consent to the data practices described in this statement. </p>

            <p><b>Collection of your Personal Information</b></p>

            <p>Two Movers Network may collect personally identifiable information, such as your:</p>

            <p>
            - Name<br> 
            - E-mail Address<br> 
            - Phone Number
            </p>

            <p>Two Movers Network encourages you to review the privacy statements of websites you choose to link to from Two Movers Network so that you can understand how those websites collect, use and share your information. Two Movers Network is not responsible for the privacy statements or other content on websites outside of the Two Movers Network website. </p>

            <p><b>Use of your Personal Information</b>

            <p>Two Movers Network collects and uses your personal information to operate its website(s) and deliver the services you have requested. </p>

            <p>Two Movers Network may also use your personally identifiable information to inform you of other products or services available from Two Movers Network and its affiliates. Two Movers Network may also contact you via surveys to conduct research about your opinion of current services or of potential new services that may be offered. </p>

            <p>Two Movers Network may sell, rent, or lease customer information to third parties for the following reasons: </p>



            <p><b>Provide Related Home Services Quotes</b>
            <p>Two Movers Network may, from time to time, contact you on behalf of external business partners about a particular offering that may be of interest to you. In those cases, your unique personally identifiable information (e-mail, name, address, telephone number) is transferred to the third party. Two Movers Network may share data with trusted partners to help perform statistical analysis, send you email or postal mail, provide customer support, or arrange for deliveries. All such third parties are prohibited from using your personal information except to provide these services to Two Movers Network, and they are required to maintain the confidentiality of your information. </p>

            <p>Two Movers Network may keep track of the websites and pages our users visit within Two Movers Network, in order to determine what Two Movers Network services are the most popular. This data is used to deliver customized content and advertising within Two Movers Network to customers whose behavior indicates that they are interested in a particular subject area.</p>

            <p>Two Movers Network will disclose your personal information, without notice, only if required to do so by law or in the good faith belief that such action is necessary to: (a) conform to the edicts of the law or comply with legal process served on Two Movers Network or the site; (b) protect and defend the rights or property of Two Movers Network; and, (c) act under exigent circumstances to protect the personal safety of users of Two Movers Network, or the public.</p>

            <p><b>Automatically Collected Information</b>
            <p>Information about your computer hardware and software may be automatically collected by Two Movers Network. This information can include: your IP address, browser type, domain names, access times and referring website addresses. This information is used for the operation of the service, to maintain quality of the service, and to provide general statistics regarding use of the Two Movers Network website. </p>

            <p><b>Use of Cookies</b></p>
            <p>The Two Movers Network website may use "cookies" to help you personalize your online experience. A cookie is a text file that is placed on your hard disk by a web page server. Cookies cannot be used to run programs or deliver viruses to your computer. Cookies are uniquely assigned to you, and can only be read by a web server in the domain that issued the cookie to you. </p>

            <p>One of the primary purposes of cookies is to provide a convenience feature to save you time. The purpose of a cookie is to tell the Web server that you have returned to a specific page. For example, if you personalize Two Movers Network pages, or register with Two Movers Network site or services, a cookie helps Two Movers Network to recall your specific information on subsequent visits. This simplifies the process of recording your personal information, such as billing addresses, shipping addresses, and so on. When you return to the same Two Movers Network website, the information you previously provided can be retrieved, so you can easily use the Two Movers Network features that you customized. 
            </p>

            <p>You have the ability to accept or decline cookies. Most Web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. If you choose to decline cookies, you may not be able to fully experience the interactive features of the Two Movers Network services or websites you visit. </p>

            <p><b>Security of your Personal Information</b></p>
            <p>Two Movers Network secures your personal information from unauthorized access, use, or disclosure. Two Movers Network uses the following methods for this purpose: </p>
            <p>
            - Verisign<br>
            - SSL Protocol 
            </p>
            <p>Verisign is an independent, third-party company engaged in the development of digital trust. Verisign provides authentication of Internet services, digital identity, and intellectual property. By clicking on the Verisign logo, the Verisign website will serve up a web page that confirms that Two Movers Network is "Verisign Authenticated." 
            </p>
            <p>When personal information (such as a credit card number) is transmitted to other websites, it is protected through the use of encryption, such as the Secure Sockets Layer (SSL) protocol. </p>

            <p><b>Children Under Thirteen</b></p>
            <p>Two Movers Network does not knowingly collect personally identifiable information from children under the age of thirteen. If you are under the age of thirteen, you must ask your parent or guardian for permission to use this website. </p>


            <p><b>Opt-Out &amp; Unsubscribe</b></p>
            <p>We respect your privacy and give you an opportunity to opt-out of receiving announcements of certain information. Users may opt-out of receiving any or all communications from Two Movers Network by contacting us here: 
            <br>
            - Web page: _________________<br> 
            - Email: info@2movers.com <br>
            - Phone: _________________ 
            </p>

            <p><b>Changes to this Statement</b></p>
            <p>Two Movers Network will occasionally update this Statement of Privacy to reflect company and customer feedback. Two Movers Network encourages you to periodically review this Statement to be informed of how Two Movers Network is protecting your information. </p>

            <p><b>Contact Information</b></p>
            <p>Two Movers Network welcomes your questions or comments regarding this Statement of Privacy. If you believe that Two Movers Network has not adhered to this Statement, please contact Two Movers Network at: 
            </p>
              <p>Two Movers Network welcomes your questions or comments regarding the Terms:</p>

            <address class="address">Two Movers Network<br>
                412 N. Main St Ste 100 <br>
                Buffalo, Wyoming 82834
            </address>

            <address class="address">Email Address: <br>
                info@2movers.com <br>
                Telephone number: <br>
                877-884-4738  <br>
                Effective as of July 01, 2017 
            </address>
        </div>
    </div>
</div>
                       
<script type="text/javascript" src="../../js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    
    /****| Popup js Code |*****/
    
    $('[data-popup-open]').on('click', function(e)  {   
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
        e.preventDefault();             
    });
    $('[data-popup-close]').on('click', function(e)  {      
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
        e.preventDefault();
    });

});
</script>

</body>
</html>

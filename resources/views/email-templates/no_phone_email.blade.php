<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
<style type="text/css">
*{box-sizing:border-box;}
body {margin:0; padding:0;}
@import url('https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700');
@media only screen and (max-width:599px){
    /* iPhone 6  */
    .phone-icon {display:inline-block !important;}
    .phone_nub {display:none !important;}
    .first-table {padding:10px 15px 15px !important;} 
    .second-table {padding:10px 15px !important;}
    .table-container {padding:30px 15px 0px !important;} 
    .details-container {width:100% !important;}
    .second-table {padding:10px 15px !important;}
    .move-details-cell {padding-left:10px !important; padding-right:10px !important;}  
}
</style>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background: #f4f4f4; font-family: 'Open Sans', sans-serif;">
    <tr>
        <td>
            <table cellspacing="0" cellpadding="0" align="center" style="max-width:600px; width:100%; margin:0 auto; padding:0; -webkit-font-smoothing:antialiased;">
            
                <tr>
                    <td style="padding-top:15px;">
                        <table width="100%" cellspacing="0" cellpadding="0" align="center">
                        <tr>
                            <td width="33%">&nbsp;</td>
                            <td width="33%" style="text-align:center;"><img src="https://www.2movers.com/img/no_phone_email/2movers-logo.png" width="107" height="30" style="width:107px; height:30px; display:inline-block;"/></td>
                            <td width="33%" style="text-align:right;">
                                <a href="tel:18666429107" style="display:none; margin-right:15px;" class="phone-icon"><img src="https://www.2movers.com/img/no_phone_email/phone-mobile.png" width="30" height="30" style="width:30px; height:30px; vertical-align:top; display:inline-block;"/></a>
                                <a href="tel:18666429107" class="phone_nub" style="text-decoration:none; font-weight:bold; font-family: 'Open Sans', sans-serif;font-size: 16px;color:#56B5E1; padding:4px 0px 10px 10px; display:inline-block;"><img src="https://www.2movers.com/img/no_phone_email/phone.png" width="15" height="17" style="display:inline; vertical-align:top;"/> 866-642-9107</a></td>
                        </tr>
                      </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="padding:10px 0 15px;" class="first-table">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#fff; padding:30px 0 40px; text-align:center; font-family: 'Open Sans', sans-serif; font-size: 16px; color: #002F54; letter-spacing: 0;">
                            <tr>
                                <td style="padding:0 20px 28px;">
                                    <p style="font-size:18px; display:block; font-size:36px; font-weight:300; color:#000; padding:0 0 10px; margin:0; font-family: 'Open Sans', sans-serif;">Your quote is ready!</p>
                                    <p style="font-size: 18px; color: #9B9B9B; font-style:italic; font-weight:300; padding:0 5px; margin:0;">Just one more step and your quotes are on their way. </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center; padding-bottom:20px;"><img src="https://www.2movers.com/img/no_phone_email/timeline-labels.png" width="275" height="44" style="width:275px; height:44px; display:inline-block;"/></td>
                            </tr>
                            <tr>
                                <td style="text-align:center;">
                                    <a class="schedule-a-call" href="https://www.2movers.com/quotes?plp_token={{ $session_id }}&utm_source=Email&utm_medium=2M&utm_campaign=2M-NP-1" style="width:240px;display: inline-block; background:#F5A623;color:#fff; border-radius:55px;-webkit-border-radius:55px; text-decoration: none;font-family: 'Open Sans', sans-serif; font-weight: bold; font-size: 20px; text-align: center; padding: 12px 45px; text-transform:uppercase;">VIEW QUOTES</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="padding:10px 0;" class="second-table">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#fff; padding:20px 0 30px; text-align:center; font-family: 'Open Sans', sans-serif; font-size: 16px; color: #002F54; letter-spacing: 0;">
                            <tr>
                                <td style="font-size:18px; font-size:36px; font-weight:300; color:#000; padding:0 0 15px; text-align:center;">Move Details</td>
                            </tr>
                        
                        
                            <tr>
                                <td class="table-container">
                                    <table width="420" border="0" cellspacing="0" cellpadding="0" align="center" style="width:420px; font-size:18px; font-weight:300; color:#9B9B9B;" class="details-container">
                                        <tr>
                                            <td width="35%" style="border-bottom:1px solid #d8d8d8; padding:4px 20px; text-align:left;" class="move-details-cell">Move Date</td>
                                            <td width="50%" style="border-bottom:1px solid #d8d8d8; padding:4px 20px; text-align:left;" class="move-details-cell">{{ $move_date }}</td>
                                            <td width="15%" style="border-bottom:1px solid #d8d8d8; padding:4px 20px; text-align:right;" class="move-details-cell"><img src="https://www.2movers.com/img/no_phone_email/checkmark.png" width="20" height="20"/></td>
                                        </tr>
                                        <tr>
                                            <td style="border-bottom:1px solid #d8d8d8; padding:4px 20px; text-align:left;" class="move-details-cell">Move Size</td>
                                            <td style="border-bottom:1px solid #d8d8d8; padding:4px 20px; text-align:left;" class="move-details-cell">{{ $move_size }}</td>
                                            <td style="border-bottom:1px solid #d8d8d8; padding:4px 20px; text-align:right;" class="move-details-cell"><img src="https://www.2movers.com/img/no_phone_email/checkmark.png" width="20" height="20"/></td>
                                        </tr>
                                        <tr>
                                            <td style="border-bottom:1px solid #d8d8d8; padding:4px 20px; text-align:left;" class="move-details-cell">ZIP Code</td>
                                            <td style="border-bottom:1px solid #d8d8d8; padding:4px 20px; text-align:left;" class="move-details-cell">{{ $from_zip }}</td>
                                            <td style="border-bottom:1px solid #d8d8d8; padding:3px 20px; text-align:right;" class="move-details-cell"><img src="https://www.2movers.com/img/no_phone_email/checkmark.png" width="20" height="20"/></td>
                                        </tr>
                                        <tr>
                                            <td style="padding:4px 20px; text-align:left;" class="move-details-cell">Email</td>
                                            <td style="padding:4px 20px; text-align:left;" class="move-details-cell">{{ $email }}</td>
                                            <td style="padding:4px 20px; text-align:right;" class="move-details-cell"><img src="https://www.2movers.com/img/no_phone_email/checkmark.png" width="20" height="20"/></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>   
                        </table>
                    </td>
                </tr>
            </table>            
        </td>
    </tr>
</table>
        <br />
<div style="text-align:center;font-size:11px;font-family:arial,helvetica,sans-serif;font-weight:normal;text-decoration:none;outline:none"><a href="https://www.2movers.com/unsubscribe?email={{ $email }}">Unsubscribe from this mailing list</a></div>
        <br />
        <div style="text-align:center;font-size:11px;font-family:arial,helvetica,sans-serif;font-weight:normal;text-decoration:none;outline:none">Sent to:<a href="mailto:{{ $email }}" target="_blank">{{ $email }}</a></div>
        <br />
        <div style="text-align:center;font-size:11px;font-family:arial,helvetica,sans-serif;font-weight:normal;text-decoration:none;outline:none">@<?php echo date('Y'); ?> www.2movers.com, 9081 W Sahara Ave #100, Las Vegas, NV 89117, United States</span>
</body>
</html>
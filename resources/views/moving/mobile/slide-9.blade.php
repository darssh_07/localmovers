<!--| Slide 9 |-->
<div class="slide slide9">
    <div class="box">
        <h1>Confirm Phone Number</h1>
        <p class="para1">We’ll quickly send you a 2-digit code to<br /> verify your number</p>
        <div class="form-container">
            <input type="tel" placeholder="(000)000-0000" class="phone_us" id="confirm_number_input"
                onkeypress="remove_error(this.id)" maxlength="15" autocomplete="off">
            <button type="submit" class="btn" id="validate_by_text">Confirm By Text</button>
            <button type="submit" class="btn" id="validate_by_phone">Confirm By Phone</button>
        </div>
    </div>
    <div class="verifying-note">
        By clicking on the "Get My Quotes" button I am verifying that I am over 18 years of age and agree to the Terms
        of Use and Privacy Policy. My "click" is my expressed written consent to be contacted via email, text, SMS or
        phone (including the use of autodialed calls or prerecorded calls) by Quote Runner LLC. and its moving partners
        that can quote or service my move, as well as companies offering other useful products or services. My consent
        supercedes any registration on any do-not-call list. Consent is required to proceed, but is not a condition of
        any purchase.
    </div>
</div>
<!-- Slide 5 Circle Loader -->
<div class="slide slide5 circle-loader">
    <div class="full-width success-icon-container">
        <div class="full-width movers-found"><span>Movers Found!</span></div>
        <div id="progress-text" class="full-width progress-text">Finding <span>Local</span> Movers…</div>
    </div>
    <div class="full-width center-align">
        <div class="progress-bar-wrap text-center raleway-italic">
            <div id="myProgress">
                <div id="myBar">0%</div>
            </div>
            <span class="progress-value" id="progress-value">0%</span>
        </div>
    </div>
</div>
<!--| Slide 1 |-->
<div class="slide slide1">
    <div class="box">
        <h1>Where are you moving FROM?</h1>
        <div class="form-container">
            <div class="error-msg">Enter Valid City or ZIP</div>
            <div class="city-name">City Name</div>
            <input type="tel" placeholder="City or ZIP" id="zip_from_input" onkeypress="remove_error(this.id)"
                autocomplete="off" maxlength="5">
            <button type="submit" class="btn" id="zip_from_btn">Get Quotes</button>
            <div class="city-result">
                <ul class="full-width" id="zip_from_code">
                    <li><a class="result-zipcode" href="#" data-zipcode="73301">Austin, TX</a></li>
                    <li><a class="result-zipcode" href="#" data-zipcode="87101">Albuquerque, NM</a></li>
                    <li><a class="result-zipcode" href="#" data-zipcode="30301">Atlanta, GA</a></li>
                    <li><a class="result-zipcode" href="#" data-zipcode="76001">Arlington, TX</a></li>
                    <li><a class="result-zipcode" href="#" data-zipcode="92801">Anaheim, CA</a></li>
                </ul>
            </div>
        </div>
        <p>Enter Zip • View Rates &amp; Availability Now</p>
    </div>
    <div class="truck">
        <img src="img/truck.svg" />
    </div>
</div>
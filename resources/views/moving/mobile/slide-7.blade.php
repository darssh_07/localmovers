<!--| Slide 7 |-->
<div class="slide slide7">
    <div class="box">
        <h1>Enter Your Name</h1>
        <p>Who should we send the quote to?</p>
        <div class="form-container">
            <div class="name-error" style="display: none;">Please provide your full name</div>
            <input type="text" placeholder="First" id="fn_input" value="" onkeypress="remove_error(this.id)"
                autocomplete="off">
            <input type="text" placeholder="Last" id="ln_input" value="" onkeypress="remove_error(this.id)"
                autocomplete="off">
            <button type="submit" class="btn" id="name_btn">SUBMIT</button>
        </div>
    </div>
</div>
<!--| Slide 6 |-->
<div class="slide slide6">
    <div class="box">
        <h1>Enter Your Email</h1>
        <p class="para1">Almost done! Enter your email to<br />view your quote.</p>
        <div class="form-container">
            <div class="error-msg">Enter Valid Email</div>
            <input type="email" placeholder="Email address" id="email_input" onkeypress="remove_error(this.id)"
                autocomplete="off">
            <button type="submit" class="btn" id="email_btn">GET QUOTES</button>
        </div>
    </div>
</div>
<!--| Slide 10 |-->
<div class="slide slide10">
    <div class="box">
        <h1>Enter 2-Digit Code</h1>
        <p class="full-width para1">Enter the code below to complete the verification:</p>
        <div class="full-width form-container">
            <input type="tel" placeholder="00" class="xx_input" id="xx_input" maxlength="2"
                onkeydown="isNumberKey(this.id)" onkeypress="remove_error(this.id)" autocomplete="off">
            <button type="submit" class="btn" id="xx_btn">VERIFY</button>
        </div>
        <p class="full-width para1">Didn’t receive a code? <a id="resend_code">Click here to resend.</a></p>
    </div>
</div>
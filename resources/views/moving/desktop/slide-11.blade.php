<!--| Slide 11 |-->
<div class="slide slide11">
    <div class="full-width ty-title"><span>Thank You</span></div>
    <div class="box" id="matches">
        <p class="full-width para1">Below are your matched movers, who will reach out<br /> shortly to provide your
            detailed quotes:</p>
        <ul class="full-width">
            <li>ABC MOVING</li>
            <li>(832)342-3245</li>
            <li>Joe’s Moving Crew</li>
            <li>(234)756-0123</li>
            <li>All My Sons</li>
            <li>(832)342-3245</li>
            <li>Budget Van Lines</li>
            <li>(832)342-3245</li>
        </ul>
    </div>
</div>
@extends('layouts.moving.desktop.master')

@section('content')
<div class="full-width main-container">
	<div class="selected-values">
		<ul>
			<li id="from-selected">
				<div>From: <span>Test</span></div>
			</li>
			<li id="to-selected">
				<div>To: <span>Test</span></div>
			</li>
			<li id="move-size-selected">
				<div>Size: <span>Test</span></div>
			</li>
			<li id="move-date-selected">
				<div><span>Test</span></div>
			</li>
		</ul>
	</div>

	<div class="ready-to-quote">
		<span>Movers found, ready to quote!</span>
	</div>

	<div class="all-slides">
		@include('moving.desktop.slide-1')
		@include('moving.desktop.slide-2')
		@include('moving.desktop.slide-3')
		@include('moving.desktop.slide-4')
		@include('moving.desktop.slide-5')
		@include('moving.desktop.slide-6')
		@include('moving.desktop.slide-7')
		@include('moving.desktop.slide-8')
		@include('moving.desktop.slide-9')
		@include('moving.desktop.slide-10')
		@include('moving.desktop.slide-11')
	</div>
</div>
@endsection
@extends('layouts.admin.master')

@section('content')

@include('layouts.errors')

<h1>Sign In</h1>
<form action="{{ route('auth_user') }}" method="post">
	{{ csrf_field() }}
	<div class="form-group"></div>
	<div class="form-group">
		<label for="username">Username</label>
		<input name="username" type="text" class="form-control" style="max-width:200px;" value="">
	</div>
	<div class="form-group">
		<label for="password">Password</label>
		<input name="password" type="password" class="form-control" style="max-width:200px;" value="">
	</div>
	<div class="form-group">
		<button type="submit" name="submit" class="btn btn-primary" value="">Sign In</button>
	</div>
</form>

@endsection
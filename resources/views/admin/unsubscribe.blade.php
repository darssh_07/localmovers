@extends('layouts.moving.desktop.master')

@section('content')
<!--middle-->
<div class="form-main-container">
<section class="steps firstSlide">
    <div class="row">
        <div class="col-right">
            <h1>YOU ARE NOW UNSUBSCRIBED</h1>
            <div class="clearfix"></div>
            <h3 class="content-area">
                <p>We're sorry to see you go. Thanks for checking out moving options with 2Movers. Please allow 24 hours for your request to be processed.</p>
            </h3>
        </div>
    </div>
    <figure><img src="../../../img/truck.svg" width="238" height="200" alt="Truck"/></figure>
</section>

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
</div>
@endsection
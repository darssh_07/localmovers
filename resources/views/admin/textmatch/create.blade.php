@extends('layouts.admin.master')

@section('title', 'Add Textmatch')

@section('content')

@include('layouts.errors')


<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				@yield('title')
			</div>
			<div class="panel-body">
				<form action="{{ route('textmatch.store') }}" method="POST" name="textmatch" class="field-group" id="textmatch">
					{{ csrf_field() }}
					<input type="hidden" name="id" value="" />
					<div class="form-group">
						<label>
							<span>Match Type</span>
							<select name="type" class="form-control">
								<option value="exact">exact</option>
								<option value="name-set">name-set</option>
								<option value="full-email">full-email</option>
								<option value="address-email">address-email</option>
								<option value="domain-email">domain-email</option>
							</select>
						</label>
					</div>
					<div class="form-group">
						<label for="text">Text List</label>
						<textarea name="text" class="form-control"></textarea>
					</div>
					<div class="form-group">
						<input type="submit" name="submit" id="add_textmatch" class="btn btn-primary" value="Add" />
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


@endsection
@extends('layouts.admin.master')

@section('title', 'Textmatch List')

@section('content')


<div class="row">
	<div class="col-md-12">
		<div class="well">
			<a class="btn btn-primary" href="{{ route('textmatch.create') }}">Add Textmatch</a>
		</div>
	</div>
</div>

@foreach ($textmatches as $type)
	@if(isset($type['data']) && $type['data']->count())
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary">
					<div class="panel-heading">{{ $type['name'] }}</div>
					<div style="max-height: 187px; overflow-y: scroll;">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Text</th>
									<th>Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($type['data'] as $textmatch)
								<tr>
									<td>{{ $textmatch->text }}</td>
									<td>{{ date('l (M d, Y)',strtotime($textmatch->date_time)) }}</td>
									<td>
										<a href="{{ route('textmatch.edit', $textmatch) }}">Edit</a>
										<a href="{{ route('textmatch.deleteConfirmation', $textmatch) }}">Delete</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>	
		</div>
	@endif
@endforeach


@endsection
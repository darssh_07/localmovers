@extends('layouts.admin.master')

@section('title', 'Delete Textmatch')

@section('content')

@include('layouts.errors')


<div class="row">
	<div class="col-md-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				@yield('title')
			</div>
			<div class="panel-body">
				<form action="{{ route('textmatch.destroy', $textmatch) }}" class="field-group" method="post">
					{{ method_field('DELETE') }}
					{{ csrf_field() }}
					<p>Are you sure that you want to delete
					'{{ $textmatch->text }}'
					</p>
					<div>						
						<input type="submit" class="btn btn-primary" name="del" value="Yes" />
						<input type="submit" class="btn btn-default" name="del" value="No" />
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection
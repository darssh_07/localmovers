@extends('layouts.admin.master')

@section('title', 'Edit Textmatch')

@section('content')

@include('layouts.errors')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				@yield('title')
			</div>
			<div class="panel-body">
				<form action="{{ route('textmatch.update', $textmatch) }}" method="POST" name="textmatch" class="field-group" id="textmatch">
					{{ method_field('PATCH') }}
					{{ csrf_field() }}
					<input type="hidden" name="id" value="{{$textmatch->id}}">
					<input type="hidden" name="type" class="form-control" value="{{$textmatch->type}}">
					<div class="form-group">
						<label for="text">Text List</label>
						<textarea name="text" class="form-control">{{ $textmatch->text }}</textarea>
					</div>
					<div class="form-group">
						<input type="submit" name="submit" id="add_textmatch" class="btn btn-primary" value="Edit">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection
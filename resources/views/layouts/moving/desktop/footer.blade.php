<!--| Footer |-->
<footer class="full-width">
	<ul>
		<li>&copy; {{ date('Y') }} Localmovers.org</li>
		<li>All Rights Reserved</li>
		<li><a data-popup-open="privacy-policy">Privacy Policy</a></li>
		<li><a data-popup-open="terms-conditions">Terms of Use</a></li>
		<li><a>Don’t Sell My Info</a></li>
	</ul>
</footer>

<div id="jon-overlay">
	<div id="jon-popup">
		<div class="form">
			<h3>Join Our Network</h3>
			<p>Call us at (844) 301-2720 to start receiving leads today, or fill out the form below.</p>
			<form id="form_network" name="form_network" method="post" autocomplete="off">
				{{ csrf_field() }}
				<input name="lead_name" id="lead_name" placeholder="Full Name" onkeypress="remove_error(this.id)">
				<input name="lead_business" id="lead_business" placeholder="Business Name"
					onkeypress="remove_error(this.id)">
				<input name="lead_number" id="lead_number" placeholder="Phone Number" maxlength="16"
					onkeypress="remove_error(this.id)">
				<input name="lead_email" id="lead_email" placeholder="Email (optional)" autocomplete="off"
					onkeypress="remove_error(this.id)">
				<button type="submit" id="form_network_submit">Submit</button>
			</form>

		</div>
		<div class="thanks">
			<h3><span class="checkmark"></span> Thank You!</h3>
			<p>Your submission has been received. We will get back to you shortly. You can also reach us directly at
				(844) 301-2720</p>
		</div>
		<a class="back-btn">+</a>
	</div>
	<div class="popup-outer-close"></div>
</div>
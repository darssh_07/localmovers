<div id="privacy-policy" class="popup" data-popup="privacy-policy">
    <div class="popup-inner terms-privacy">
        <a class="popup-close" data-popup-close="privacy-policy"></a>
        <div class="full-width title">Privacy Policy</div>
        <div class="full-width content">
            <p>Protecting your private information is our priority. This Statement of Privacy applies to
                www.LocalMovers.org and Local Movers Network and governs data collection and usage. For the purposes of
                this Privacy Policy, unless otherwise noted, all references to Local Movers Network include
                www.LocalMovers.org. The Local Movers Network website is a Moving Company Network site. By using the
                Local Movers Network website, you consent to the data practices described in this statement. </p>

            <h1 class="title">Collection of your Personal Information </h1>

            <p>Local Movers Network may collect personally identifiable information, such as your:</p>

            <p>
                - Name<br>
                - E-mail Address<br>
                - Phone Number
            </p>

            <p>Local Movers Network encourages you to review the privacy statements of websites you choose to link to
                from Local Movers Network so that you can understand how those websites collect, use and share your
                information. Local Movers Network is not responsible for the privacy statements or other content on
                websites outside of the Local Movers Network website. </p>

            <h1 class="title">Use of your Personal Information</h1>

            <p>Local Movers Network collects and uses your personal information to operate its website(s) and deliver
                the services you have requested. </p>

            <p>Local Movers Network may also use your personally identifiable information to inform you of other
                products or services available from Local Movers Network and its affiliates. Local Movers Network may
                also contact you via surveys to conduct research about your opinion of current services or of potential
                new services that may be offered. </p>

            <p>Local Movers Network may sell, rent, or lease customer information to third parties for the following
                reasons: </p>



            <h1 class="title">Provide Related Home Services Quotes </h1>
            <p>Local Movers Network may, from time to time, contact you on behalf of external business partners about a
                particular offering that may be of interest to you. In those cases, your unique personally identifiable
                information (e-mail, name, address, telephone number) is transferred to the third party. Local Movers
                Network may share data with trusted partners to help perform statistical analysis, send you email or
                postal mail, provide customer support, or arrange for deliveries. All such third parties are prohibited
                from using your personal information except to provide these services to Local Movers Network, and they
                are required to maintain the confidentiality of your information. </p>

            <p>Local Movers Network may keep track of the websites and pages our users visit within Local Movers
                Network, in order to determine what Local Movers Network services are the most popular. This data is
                used to deliver customized content and advertising within Local Movers Network to customers whose
                behavior indicates that they are interested in a particular subject area.</p>

            <p>Local Movers Network will disclose your personal information, without notice, only if required to do so
                by law or in the good faith belief that such action is necessary to: (a) conform to the edicts of the
                law or comply with legal process served on Local Movers Network or the site; (b) protect and defend the
                rights or property of Local Movers Network; and, (c) act under exigent circumstances to protect the
                personal safety of users of Local Movers Network, or the public.</p>

            <h1 class="title">Automatically Collected Information</h1>
            <p>Information about your computer hardware and software may be automatically collected by Local Movers
                Network. This information can include: your IP address, browser type, domain names, access times and
                referring website addresses. This information is used for the operation of the service, to maintain
                quality of the service, and to provide general statistics regarding use of the Local Movers Network
                website. </p>

            <h1 class="title">Use of Cookies </h1>
            <p>The Local Movers Network website may use "cookies" to help you personalize your online experience. A
                cookie is a text file that is placed on your hard disk by a web page server. Cookies cannot be used to
                run programs or deliver viruses to your computer. Cookies are uniquely assigned to you, and can only be
                read by a web server in the domain that issued the cookie to you. </p>

            <p>One of the primary purposes of cookies is to provide a convenience feature to save you time. The purpose
                of a cookie is to tell the Web server that you have returned to a specific page. For example, if you
                personalize Local Movers Network pages, or register with Local Movers Network site or services, a cookie
                helps Local Movers Network to recall your specific information on subsequent visits. This simplifies the
                process of recording your personal information, such as billing addresses, shipping addresses, and so
                on. When you return to the same Local Movers Network website, the information you previously provided
                can be retrieved, so you can easily use the Local Movers Network features that you customized.
            </p>

            <p>You have the ability to accept or decline cookies. Most Web browsers automatically accept cookies, but
                you can usually modify your browser setting to decline cookies if you prefer. If you choose to decline
                cookies, you may not be able to fully experience the interactive features of the Local Movers Network
                services or websites you visit. </p>

            <h1 class="title">Security of your Personal Information</h1>
            <p>Local Movers Network secures your personal information from unauthorized access, use, or disclosure.
                Local Movers Network uses the following methods for this purpose: </p>
            <p>
                - Verisign<br>
                - SSL Protocol
            </p>
            <p>Verisign is an independent, third-party company engaged in the development of digital trust. Verisign
                provides authentication of Internet services, digital identity, and intellectual property. By clicking
                on the Verisign logo, the Verisign website will serve up a web page that confirms that Local Movers
                Network is "Verisign Authenticated."
            </p>
            <p>When personal information (such as a credit card number) is transmitted to other websites, it is
                protected through the use of encryption, such as the Secure Sockets Layer (SSL) protocol. </p>

            <h1 class="title">Children Under Thirteen</h1>
            <p>Local Movers Network does not knowingly collect personally identifiable information from children under
                the age of thirteen. If you are under the age of thirteen, you must ask your parent or guardian for
                permission to use this website. </p>


            <h1 class="title">Opt-Out &amp; Unsubscribe</h1>
            <p>We respect your privacy and give you an opportunity to opt-out of receiving announcements of certain
                information. Users may opt-out of receiving any or all communications from Local Movers Network by
                contacting us here:
                <br>
                - Web page: _________________<br>
                - Email: info@LocalMovers.org <br>
                - Phone: _________________
            </p>

            <h1 class="title">Changes to this Statement </h1>
            <p>Local Movers Network will occasionally update this Statement of Privacy to reflect company and customer
                feedback. Local Movers Network encourages you to periodically review this Statement to be informed of
                how Local Movers Network is protecting your information. </p>

            <h1 class="title">Contact Information </h1>
            <p>Local Movers Network welcomes your questions or comments regarding this Statement of Privacy. If you
                believe that Local Movers Network has not adhered to this Statement, please contact Local Movers Network
                at:
            </p>
            <p>Local Movers Network welcomes your questions or comments regarding the Terms:</p>

            <address class="address">Local Movers Network<br>
                412 N. Main St Ste 100 <br>
                Buffalo, Wyoming 82834
            </address>

            <address class="address">Email Address: <br>
                info@LocalMovers.org <br>
                Telephone number: <br>
                877-884-4738 <br>
                Effective as of July 01, 2017
            </address>


            <h1 class="title">For California Consumers The Following Applies</h1>

            <h1 class="title">Your Rights and Choices</h1>
            <p>The CCPA provides consumers (California residents) with specific rights regarding their personal
                information. This section describes your CCPA rights and explains how to exercise those rights.</p>

            <h3 class="address">Access to Specific Information and Data Portability Rights</h3>

            <p class="address">You have the right to request that we disclose certain information to you about our
                collection and use of your personal information over the past 12 months. Once we receive and confirm
                your verifiable consumer request, we will disclose to you:</p>

            <ul style="padding: 20px 30px;list-style:circle;">
                <li>The categories of personal information we collected about you.</li>
                <li>The categories of sources for the personal information we collected about you.</li>
                <li>Our business or commercial purpose for collecting or selling that personal information.</li>
                <li>The categories of third parties with whom we share that personal information.</li>
                <li>The specific pieces of personal information we collected about you (also called a data portability
                    request)</li>
                <li>
                    If we sold or disclosed your personal information for a business purpose, two separate lists
                    disclosing:
                    <ul style="list-style: circle;padding-left: 30px;">
                        <li>sales, identifying the personal information categories that each category of recipient
                            purchased; and</li>
                        <li>disclosures for a business purpose, identifying the personal information categories that
                            each category of recipient obtained.</li>
                    </ul>
                </li>

            </ul>

            <h3 class="address">Deletion Request Rights</h3>
            <p class="address">You have the right to request that we delete any of your personal information that we
                collect from you and retained, subject to certain exceptions. Once we receive and confirm your
                verifiable consumer request, we will delete (and direct our service providers to delete) your personal
                information from our records, unless an exception applies.</p>
            <p class="address">We may deny your deletion request if retaining the information is necessary for us or our
                service providers to:</p>

            <ol style="padding: 20px 30px;list-style:decimal;">
                <li>Complete the transaction for which we collected the personal information, provide a good or service
                    that you requested, take actions reasonably anticipated within the context of our ongoing business
                    relationship with you, or otherwise perform our contract with you.</li>
                <li>Detect security incidents, protect against malicious, deceptive, fraudulent, or illegal activity, or
                    prosecute those responsible for such activities.</li>
                <li>Debug products to identify and repair errors that impair existing intended functionality.</li>
                <li>Comply with a legal obligation.</li>
            </ol>


            <h3 class="address">Exercising Access, Data Portability, and Deletion Rights</h3>
            <p class="address">To exercise the access, data portability, and deletion rights described above, please
                submit a verifiable consumer request to us by email:</p>
            <ul style="padding: 20px 30px;list-style:circle;">
                <li>Request Information Do Not Be Sold Go To <a class="ca_popups"
                        data-id="do_not_sell_ca">www.LocalMovers.org/Do-not-sell-ca</a></li>
                <li>Request Right To Know Information Go To <a class="ca_popups"
                        data-id="right_to_know_ca">www.LocalMovers.org/right-to-know-ca</a></li>
                <li>Request To Delete Your Information Go To <a class="ca_popups"
                        data-id="delete_my_info_ca">www.LocalMovers.org/delete-my-info-ca</a></li>
                <li>Emailing us at <a href="mailto:CCPA@LocalMovers.org">CCPA@LocalMovers.org</a></li>
                <li>Toll Free CCPA Hotline - <a href="tel:18772745928">877-274-5928</a></li>

            </ul>
            <p class="address">Only you or a person registered with the California Secretary of State that you authorize
                to act on your behalf, may make a verifiable consumer request related to your personal information. You
                may also make a verifiable consumer request on behalf of your minor child.</p>
            <p class="address">You may only make a verifiable consumer request for access or data portability twice
                within a 12-month period. The verifiable consumer request must:</p>
            <ul style="padding: 20px 30px;list-style:circle;">
                <li>Provide sufficient information that allows us to reasonably verify you are the person about whom we
                    collected personal information or an authorized representative. (At times we may require you to
                    confirm a link sent to your phone via SMS or email)</li>
                <li>Describe your request with sufficient detail that allows us to properly understand, evaluate, and
                    respond to it.</li>
            </ul>

            <p class="address">We cannot respond to your request or provide you with personal information if we cannot
                verify your identity or authority to make the request and confirm the personal information relates to
                you. Making a verifiable consumer request does not require you to create an account with us. We will
                only use personal information provided in a verifiable consumer request to verify the requestor's
                identity or authority to make the request.</p>

            <h3 class="address">Response Timing and Format</h3>
            <p class="address">We endeavor to respond to a verifiable consumer request within 45 days of its receipt. If
                we require more time (up to 90 days), we will inform you of the reason and extension period in writing.
                We will deliver our written response by email to the email that was verified. Any disclosures we provide
                will only cover the 12-month period preceding the verifiable consumer request's receipt. The response we
                provide will also explain the reasons we cannot comply with a request, if applicable. We do not charge a
                fee to process or respond to your verifiable consumer request unless it is excessive, repetitive, or
                manifestly unfounded. If we determine that the request warrants a fee, we will tell you why we made that
                decision and provide you with a cost estimate before completing your request.</p>


            <h1 class="title">Non-Discrimination</h1>
            <p class="address">We will not discriminate against you for exercising any of your CCPA rights. Unless
                permitted by the CCPA, we will not:</p>
            <ul style="padding: 20px 30px;list-style:circle;">
                <li>Deny you goods or services.</li>
                <li>Charge you different prices or rates for goods or services, including through granting discounts or
                    other benefits, or imposing penalties.</li>
                <li>Provide you a different level or quality of goods or services.</li>
                <li>Suggest that you may receive a different price or rate for goods or services or a different level or
                    quality of goods or services.</li>
            </ul>
        </div>
    </div>
</div>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="title" content="LocalMovers.org">
    <meta charset="UTF-8">
    <title>LocalMovers.org</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="css/moving/mobile/mobile.css" media="all" />


    @include('layouts.moving.conversion')
</head>

<body>
    @include('layouts.moving.mobile.header')
    <div class="full-width main-container">

        <div class="selected-values">
            <ul>
                <li id="from-selected">
                    <div>From: <span>Test</span></div>
                </li>
                <li id="to-selected">
                    <div>To: <span>Test</span></div>
                </li>
                <li id="move-size-selected">
                    <div>Size: <span>Test</span></div>
                </li>
                <li id="move-date-selected">
                    <div><span>Test</span></div>
                </li>
            </ul>
        </div>

        <div class="ready-to-quote">
            <span>Movers found, ready to quote!</span>
        </div>

        <!--| All Slides |-->
        <div class="all-slides">
            @yield('content')
        </div>

    </div>

    @include('layouts.moving.mobile.footer')
    @include('layouts.moving.mobile.privacy_policy')
    @include('layouts.moving.mobile.terms_conditions')

    <input type="hidden" id="source" name="source"
        value="{{ isset($this->source) ? $this->source : '' }}" />
    <input type="hidden" id="utm_source" name="utm_source"
        value="{{ (isset($_GET['utm_source'])) ? $_GET['utm_source']: "" }}">
    <input type="hidden" id="utm_medium" name="utm_medium"
        value="{{ (isset($_GET['utm_medium'])) ? $_GET['utm_medium']: "" }}">
    <input type="hidden" id="utm_campaign" name="utm_campaign"
        value="{{ (isset($_GET['utm_campaign'])) ? $_GET['utm_campaign']: "" }}">
    <input type="hidden" id="_campaign" name="_campaign"
        value="{{ (isset($_GET['_campaign'])) ? $_GET['_campaign']: "" }}">
    <input type="hidden" id="ag" name="ag"
        value="{{ (isset($_GET['ag'])) ? $_GET['ag']: "" }}">
    <input type="hidden" id="qs" name="qs"
        value="{{ (isset($_GET['qs'])) ? $_GET['qs']: "" }}">
    <input type="hidden" id="network" name="network"
        value="{{ (isset($_GET['network'])) ? $_GET['network']: "" }}">
    <input type="hidden" id="device" name="device"
        value="{{ (isset($_GET['device'])) ? $_GET['device']: "" }}">
    <input type="hidden" id="adid" name="adid"
        value="{{ (isset($_GET['adid'])) ? $_GET['adid']: "" }}">
    <input type="hidden" id="kw" name="kw"
        value="{{ (isset($_GET['kw'])) ? $_GET['kw']: "" }}">
    <input type="hidden" id="oii" name="oii"
        value="{{ (isset($_GET['oii'])) ? $_GET['oii']: "" }}">
    <input type="hidden" id="gclid" name="gclid"
        value="{{ (isset($_GET['gclid'])) ? $_GET['gclid']: (isset($_GET['msclkid']) ? $_GET['msclkid'] : "") }}">
    <input type="hidden" id="campid" name="campid"
        value="{{ (isset($_GET['campid'])) ? $_GET['campid']: "" }}">
    <input type="hidden" id="domain" name="domain"
        value="{{ (isset($_GET['domain'])) ? $_GET['domain']: "" }}">

    <script>
        var jon = document.getElementById("jon-overlay");
        if (window.location.href.indexOf('joinournetwork') > 0) {
            jon.style.display = "block";
            document.body.style.overflow = "hidden";
        }
    </script>

    <script
        src='https://maps.googleapis.com/maps/api/js?v=3&sensor=false&libraries=places&&region=us,&key=AIzaSyDW2Ef82EFKNPb7XAqCxAi1jRxbppUgpJ4'>
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/7.0.0/polyfill.min.js" type="text/javascript">
    </script>
    <script type="text/javascript" src="js/moving/mobile/mobile.js"></script>
    <script src="https://browser.sentry-cdn.com/5.17.0/bundle.min.js"
        integrity="sha384-lowBFC6YTkvMIWPORr7+TERnCkZdo5ab00oH5NkFLeQUAmBTLGwJpFjF6djuxJ/5" crossorigin="anonymous"
        defer></script>
</body>

</html>
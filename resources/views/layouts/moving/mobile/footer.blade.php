<footer class="full-width">
	<ul>
		<li><a data-popup-open="privacy-policy">Privacy Policy</a></li>
		<li><a data-popup-open="terms-conditions">Terms of Use</a></li>
		<li><a>Don’t Sell My Info</a></li>
	</ul>
</footer>
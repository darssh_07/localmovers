<header class="full-width">
    <div class="left-logo"><img src="../../../img/localmovers-logo.svg" /></div>
    <div class="right-logo">
        <span>A trusted partner of:</span>
        <img src="../../../img/moveorg-logo.png" />
    </div>
</header>
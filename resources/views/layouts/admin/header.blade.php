<head>
    <meta charset="utf-8">

    <title>@yield('title') 2Movers</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Le styles -->
    <link href="/css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="/css/bootstrap-theme.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="/css/admin.css" media="screen" rel="stylesheet" type="text/css">

</head>
<html lang="en">
	@include('layouts.admin.header')
	<body>
		@include('layouts.admin.nav')
		<div class="container">
            @yield('content')
        </div>
		<div class="fixed-footer">
		@include('layouts.admin.footer')
		</div>
 		<!--[if lt IE 9]><script type="text/javascript" src="/js/html5shiv.js"></script><![endif]-->
 		<!--[if lt IE 9]><script type="text/javascript" src="/js/respond.min.js"></script><![endif]-->
 		<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
 		<script type="text/javascript" src="/js/bootstrap.min.js"></script>
 		<script type="text/javascript" src="/js/global.js"></script> 
 		<script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
 		<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script> 
 		<script type="text/javascript" src="/js/admin/jquery.dataTables.min.js"></script> 
 		<script src="/js/admin/company.js"></script>
	</body>
</html>
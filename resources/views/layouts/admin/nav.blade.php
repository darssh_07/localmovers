<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">            
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="/admin">2Movers</a>
	</div>
	<div class="collapse navbar-collapse">
		@if (Auth::check())
		<ul class="nav navbar-nav">
			<li><a href="/admin">Admin</a></li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">TextMatch Validation <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="/admin/textmatch">textmatch</a></li>
				</ul>
			</li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
		<li><a href="/admin/logout">Logout</a></li>
		</ul>
		@endif
	</div><!--/.nav-collapse -->
</nav>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="robots" content="Noindex, Nofollow">
    <title>2 Movers™</title>
    <link type="text/css" href="css/contact/ccpa.css" rel="stylesheet">
</head>

<body>

    <header class="header">
        <div class="logo">
            <a href="javascript:void(0)"><img src="img/logo.svg" width="130" alt="2 Movers" title="2 Movers"></a>
        </div>
        <div class="phone_no">
            <a href="tel:18772745928"><img src="img/icon-phone.svg" class="icons">877-274-5928</a>
        </div>
    </header>

    <div class="main-container">

        <div class="full-width">
            <h2 class="full-width">Do Not Sell</h2>
            <p class="full-width">2Movers connects you with movers in our network that can service your move. 2Movers receives compensation from the movers for this service. By law this is considered “selling of information”. The essence of our service is to connect you with 3rd party service providers, if you do not wish to continue please click “EXIT SITE”. If you would like to receive multiple quotes click “I understand please continue”. For more information, please see our <a data-popup-open="popup-1">Privacy Policy</a>.</p>
            <button class="btn" id="do-not-sell-btn" onclick="location.href = '/';">I UNDERSTAND,<br />PLEASE CONTINUE</button><br />
            <button onclick="open(location, '_self').close();" class="exit">EXIT SITE</button>

        </div>

    </div>

    <footer class="full-width">
        <a data-popup-open="popup-1" class="privacy_policy">Privacy Policy</a>
        |
        <a data-popup-open="popup-2" class="terms_conditions">Terms &amp; Conditions</a>
        <br />
        ©2Movers, Inc.
    </footer>

    <div class="popup" data-popup="popup-1">
        <div class="popup-inner">

            <a class="popup-close" data-popup-close="popup-1"></a>
            <div class="content">
                <h1 class="main-title">Privacy Policy</h1>
                <div class="privacy_policy outer">

                <p>Protecting your private information is our priority. This Statement of Privacy applies to www.2movers.com and Two Movers Network and governs data collection and usage. For the purposes of this Privacy Policy, unless otherwise noted, all references to Two Movers Network include www.2movers.com. The Two Movers Network website is a Moving Company Network site. By using the Two Movers Network website, you consent to the data practices described in this statement. </p>

                <h1 class="title">Collection of your Personal Information </h1>

                <p>Two Movers Network may collect personally identifiable information, such as your:</p>

                <p>
                    &mdash; Name<br />
                    &mdash; E-mail Address<br />
                    &mdash; Phone Number
                </p>

                <p>Two Movers Network encourages you to review the privacy statements of websites you choose to link to from Two Movers Network so that you can understand how those websites collect, use and share your information. Two Movers Network is not responsible for the privacy statements or other content on websites outside of the Two Movers Network website. </p>

                <h1 class="title">Use of your Personal Information</h1>

                <p>Two Movers Network collects and uses your personal information to operate its website(s) and deliver the services you have requested. </p>

                <p>Two Movers Network may also use your personally identifiable information to inform you of other products or services available from Two Movers Network and its affiliates. Two Movers Network may also contact you via surveys to conduct research about your opinion of current services or of potential new services that may be offered. </p>

                <p>Two Movers Network may sell, rent, or lease customer information to third parties for the following reasons: </p>



                <h1 class="title">Provide Related Home Services Quotes </h1>
                <p>Two Movers Network may, from time to time, contact you on behalf of external business partners about a particular offering that may be of interest to you. In those cases, your unique personally identifiable information (e-mail, name, address, telephone number) is transferred to the third party. Two Movers Network may share data with trusted partners to help perform statistical analysis, send you email or postal mail, provide customer support, or arrange for deliveries. All such third parties are prohibited from using your personal information except to provide these services to Two Movers Network, and they are required to maintain the confidentiality of your information. </p>

                <p>Two Movers Network may keep track of the websites and pages our users visit within Two Movers Network, in order to determine what Two Movers Network services are the most popular. This data is used to deliver customized content and advertising within Two Movers Network to customers whose behavior indicates that they are interested in a particular subject area.</p>

                <p>Two Movers Network will disclose your personal information, without notice, only if required to do so by law or in the good faith belief that such action is necessary to: (a) conform to the edicts of the law or comply with legal process served on Two Movers Network or the site; (b) protect and defend the rights or property of Two Movers Network; and, (c) act under exigent circumstances to protect the personal safety of users of Two Movers Network, or the public.</p>

                <h1 class="title">Automatically Collected Information</h1>
                <p>Information about your computer hardware and software may be automatically collected by Two Movers Network. This information can include: your IP address, browser type, domain names, access times and referring website addresses. This information is used for the operation of the service, to maintain quality of the service, and to provide general statistics regarding use of the Two Movers Network website. </p>

                <h1 class="title">Use of Cookies </h1>
                <p>The Two Movers Network website may use "cookies" to help you personalize your online experience. A cookie is a text file that is placed on your hard disk by a web page server. Cookies cannot be used to run programs or deliver viruses to your computer. Cookies are uniquely assigned to you, and can only be read by a web server in the domain that issued the cookie to you. </p>

                <p>One of the primary purposes of cookies is to provide a convenience feature to save you time. The purpose of a cookie is to tell the Web server that you have returned to a specific page. For example, if you personalize Two Movers Network pages, or register with Two Movers Network site or services, a cookie helps Two Movers Network to recall your specific information on subsequent visits. This simplifies the process of recording your personal information, such as billing addresses, shipping addresses, and so on. When you return to the same Two Movers Network website, the information you previously provided can be retrieved, so you can easily use the Two Movers Network features that you customized.
                </p>

                <p>You have the ability to accept or decline cookies. Most Web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. If you choose to decline cookies, you may not be able to fully experience the interactive features of the Two Movers Network services or websites you visit. </p>

                <h1 class="title">Security of your Personal Information</h1>
                <p>Two Movers Network secures your personal information from unauthorized access, use, or disclosure. Two Movers Network uses the following methods for this purpose: </p>
                <p>
                    �&mdash; Verisign<br />
                    �&mdash; SSL Protocol
                </p>
                <p>Verisign is an independent, third-party company engaged in the development of digital trust. Verisign provides authentication of Internet services, digital identity, and intellectual property. By clicking on the Verisign logo, the Verisign website will serve up a web page that confirms that Two Movers Network is "Verisign Authenticated."
                </p>
                <p>When personal information (such as a credit card number) is transmitted to other websites, it is protected through the use of encryption, such as the Secure Sockets Layer (SSL) protocol. </p>

                <h1 class="title">Children Under Thirteen</h1>
                <p>Two Movers Network does not knowingly collect personally identifiable information from children under the age of thirteen. If you are under the age of thirteen, you must ask your parent or guardian for permission to use this website. </p>


                <h1 class="title">Opt-Out & Unsubscribe</h1>
                <p>We respect your privacy and give you an opportunity to opt-out of receiving announcements of certain information. Users may opt-out of receiving any or all communications from Two Movers Network by contacting us here:
                    <br />
                    &mdash; Web page: _________________<br />
                    &mdash; Email: info@2movers.com <br />
                    &mdash; Phone: _________________
                </p>

                <h1 class="title">Changes to this Statement </h1>
                <p>Two Movers Network will occasionally update this Statement of Privacy to reflect company and customer feedback. Two Movers Network encourages you to periodically review this Statement to be informed of how Two Movers Network is protecting your information. </p>

                <h1 class="title">Contact Information </h1>
                <p>Two Movers Network welcomes your questions or comments regarding this Statement of Privacy. If you believe that Two Movers Network has not adhered to this Statement, please contact Two Movers Network at:
                </p>
                <p>Two Movers Network welcomes your questions or comments regarding the Terms:</p>

                <address class="address">Two Movers Network<br>
                    412 N. Main St Ste 100 <br>
                    Buffalo, Wyoming 82834
                </address>

                <address class="address">Email Address: <br>
                    info@2movers.com <br>
                    Telephone number: <br>
                    877-884-4738 <br>
                    Effective as of July 01, 2017
                </address>
                
                <h1 class="title">For California Consumers The Following Applies</h1>

				<h1 class="title">Your Rights and Choices</h1>
				<p>The CCPA provides consumers (California residents) with specific rights regarding their personal information. This section describes your CCPA rights and explains how to exercise those rights.</p>

				<h3 class="address">Access to Specific Information and Data Portability Rights</h3>
				
				<p class="address">You have the right to request that we disclose certain information to you about our collection and use of your personal information over the past 12 months. Once we receive and confirm your verifiable consumer request, we will disclose to you:</p>
                
                <ul style="padding: 20px 30px;list-style:circle;">
                	<li>The categories of personal information we collected about you.</li>
                	<li>The categories of sources for the personal information we collected about you.</li>
                	<li>Our business or commercial purpose for collecting or selling that personal information.</li>
                	<li>The categories of third parties with whom we share that personal information.</li>
                	<li>The specific pieces of personal information we collected about you (also called a data portability request)</li>
                	<li>
                		If we sold or disclosed your personal information for a business purpose, two separate lists disclosing:
                		<ul style="list-style: circle;padding-left: 30px;">
                		<li>sales, identifying the personal information categories that each category of recipient purchased; and</li>
                		<li>disclosures for a business purpose, identifying the personal information categories that each category of recipient obtained.</li>
                		</ul>
                	</li>
                		
                </ul>
                
                <h3 class="address">Deletion Request Rights</h3>
                <p class="address">You have the right to request that we delete any of your personal information that we collect from you and retained, subject to certain exceptions. Once we receive and confirm your verifiable consumer request, we will delete (and direct our service providers to delete) your personal information from our records, unless an exception applies.</p>
                <p class="address">We may deny your deletion request if retaining the information is necessary for us or our service providers to:</p>
                
                <ol style="padding: 20px 30px;list-style:decimal;">
                	<li>Complete the transaction for which we collected the personal information, provide a good or service that you requested, take actions reasonably anticipated within the context of our ongoing business relationship with you, or otherwise perform our contract with you.</li>
                	<li>Detect security incidents, protect against malicious, deceptive, fraudulent, or illegal activity, or prosecute those responsible for such activities.</li>
                	<li>Debug products to identify and repair errors that impair existing intended   functionality.</li>
                	<li>Comply with a legal obligation.</li>
                </ol>
                
                
                <h3 class="address">Exercising Access, Data Portability, and Deletion Rights</h3>
                <p class="address">To exercise the access, data portability, and deletion rights described above, please submit a verifiable consumer request to us by email:</p>
                <ul style="padding: 20px 30px;list-style:circle;">
                	<li>Request Information Do Not Be Sold Go To <a target="_blank" href="/Do-not-sell-ca">www.2movers.com/Do-not-sell-ca</a></li>
                	<li>Request Right To Know Information Go To <a target="_blank"href="/right-to-know-ca">www.2movers.com/right-to-know-ca</a></li>
                	<li>Request To Delete Your Information Go To <a target="_blank" href="/delete-my-info-ca">www.2movers.com/delete-my-info-ca</a></li>
                	<li>Emailing us at <a href="mailto:CCPA@2movers.com">CCPA@2movers.com</a></li>
                	<li>Toll Free CCPA Hotline - <a href="tel:18772745928">877-274-5928</a></li>
                		
                </ul>
                <p class="address">Only you or a person registered with the California Secretary of State that you authorize to act on your behalf, may make a verifiable consumer request related to your personal information. You may also make a verifiable consumer request on behalf of your minor child.</p>
                <p class="address">You may only make a verifiable consumer request for access or data portability twice within a 12-month period. The verifiable consumer request must:</p>
                <ul style="padding: 20px 30px;list-style:circle;">
                	<li>Provide sufficient information that allows us to reasonably verify you are the person about whom we collected personal information or an authorized representative. (At times we may require you to confirm a link sent to your phone via SMS or email)</li>
                	<li>Describe your request with sufficient detail that allows us to properly understand, evaluate, and respond to it.</li>
                </ul>
                
                <p class="address">We cannot respond to your request or provide you with personal information if we cannot verify your identity or authority to make the request and confirm the personal information relates to you.  Making a verifiable consumer request does not require you to create an account with us.  We will only use personal information provided in a verifiable consumer request to verify the requestor's identity or authority to make the request.</p>
                
                <h3 class="address">Response Timing and Format</h3>
                <p class="address">We endeavor to respond to a verifiable consumer request within 45 days of its receipt.  If we require more time (up to 90 days), we will inform you of the reason and extension period in writing.  We will deliver our written response by email to the email that was verified.  Any disclosures we provide will only cover the 12-month period preceding the verifiable consumer request's receipt.  The response we provide will also explain the reasons we cannot comply with a request, if applicable.  We do not charge a fee to process or respond to your verifiable consumer request unless it is excessive, repetitive, or manifestly unfounded.  If we determine that the request warrants a fee, we will tell you why we made that decision and provide you with a cost estimate before completing your request.</p>
                
                
                <h1 class="title">Non-Discrimination</h1>
                <p class="address">We will not discriminate against you for exercising any of your CCPA rights. Unless permitted by the CCPA, we will not:</p>
                <ul style="padding: 20px 30px;list-style:circle;">
                	<li>Deny you goods or services.</li>
                	<li>Charge you different prices or rates for goods or services, including through granting discounts or other benefits, or imposing penalties.</li>
                	<li>Provide you a different level or quality of goods or services.</li>
                	<li>Suggest that you may receive a different price or rate for goods or services or a different level or quality of goods or services.</li>
                </ul>
            </div>
            </div>
        </div>
        <a class="popup-outer-close"></a>

    </div>
    <div class="popup" data-popup="popup-2">
        <div class="popup-inner">
            <a class="popup-close" data-popup-close="popup-2" href="#"></a>
            <div class="content">
                <h1 class="main-title">Terms of Use</h1>
                <div class="privacy_policy outer">
                <h1 class="title">Agreement between User and www.2movers.com</h1>

                <p>Welcome to www.2movers.com. The www.2movers.com website (the "Site") is comprised of various web pages operated by Two Movers Network. www.2movers.com is offered to you conditioned on your acceptance without modification of the terms, conditions, and notices contained herein (the "Terms"). Your use of www.2movers.com constitutes your agreement to all such Terms. Please read these terms carefully, and keep a copy of them for your reference. </p>
                <p>www.2movers.com is an E-Commerce Site.</p>
                <p>Connect Shippers With Moving Companies</p>

                <h1 class="title">Privacy</h1>
                <p>Your use of www.2movers.com is subject to Two Movers Network's Privacy Policy. Please review our Privacy Policy, which also governs the Site and informs users of our data collection practices. </p>

                <h1 class="title">Electronic Communications</h1>
                <p>Visiting www.2movers.com or sending emails to Two Movers Network constitutes electronic communications. You consent to receive electronic communications and you agree that all agreements, notices, disclosures and other communications that we provide to you electronically, via email and on the Site, satisfy any legal requirement that such communications be in writing. </p>

                <h1 class="title">Children Under Thirteen</h1>
                <p>Two Movers Network does not knowingly collect, either online or offline, personal information from persons under the age of thirteen. If you are under 18, you may use www.2movers.com only with permission of a parent or guardian. </p>

                <h1 class="title">Links to Third Party Sites/Third Party Services</h1>
                <p>www.2movers.com may contain links to other websites ("Linked Sites"). The Linked Sites are not under the control of Two Movers Network and Two Movers Network is not responsible for the contents of any Linked Site, including without limitation any link contained in a Linked Site, or any changes or updates to a Linked Site. Two Movers Network is providing these links to you only as a convenience, and the inclusion of any link does not imply endorsement by Two Movers Network of the site or any association with its operators. </p>
                <p>Certain services made available via www.2movers.com are delivered by third party sites and organizations. By using any product, service or functionality originating from the www.2movers.com domain, you hereby acknowledge and consent that Two Movers Network may share such information and data with any third party with whom Two Movers Network has a contractual relationship to provide the requested product, service or functionality on behalf of www.2movers.com users and customers. </p>

                <h1 class="title">No Unlawful or Prohibited Use/Intellectual Property</h1>
                <p>You are granted a non-exclusive, non-transferable, revocable license to access and use www.2movers.com strictly in accordance with these terms of use. As a condition of your use of the Site, you warrant to Two Movers Network that you will not use the Site for any purpose that is unlawful or prohibited by these Terms. You may not use the Site in any manner which could damage, disable, overburden, or impair the Site or interfere with any other party's use and enjoyment of the Site. You may not obtain or attempt to obtain any materials or information through any means not intentionally made available or provided for through the Site. </p>
                <p>All content included as part of the Service, such as text, graphics, logos, images, as well as the compilation thereof, and any software used on the Site, is the property of Two Movers Network or its suppliers and protected by copyright and other laws that protect intellectual property and proprietary rights. You agree to observe and abide by all copyright and other proprietary notices, legends or other restrictions contained in any such content and will not make any changes thereto. </p>
                <p>You will not modify, publish, transmit, reverse engineer, participate in the transfer or sale, create derivative works, or in any way exploit any of the content, in whole or in part, found on the Site. Two Movers Network content is not for resale. Your use of the Site does not entitle you to make any unauthorized use of any protected content, and in particular you will not delete or alter any proprietary rights or attribution notices in any content. You will use protected content solely for your personal use, and will make no other use of the content without the express written permission of Two Movers Network and the copyright owner. You agree that you do not acquire any ownership rights in any protected content. We do not grant you any licenses, express or implied, to the intellectual property of Two Movers Network or our licensors except as expressly authorized by these Terms. </p>

                <h1 class="title">International Users </h1>
                <p>The Service is controlled, operated and administered by Two Movers Network from our offices within the USA. If you access the Service from a location outside the USA, you are responsible for compliance with all local laws. You agree that you will not use the Two Movers Network Content accessed through www.2movers.com in any country or in any manner prohibited by any applicable laws, restrictions or regulations. </p>

                <h1 class="title">Indemnification</h1>
                <p>You agree to indemnify, defend and hold harmless Two Movers Network, its officers, directors, employees, agents and third parties, for any losses, costs, liabilities and expenses (including reasonable attorney's fees) relating to or arising out of your use of or inability to use the Site or services, any user postings made by you, your violation of any terms of this Agreement or your violation of any rights of a third party, or your violation of any applicable laws, rules or regulations. Two Movers Network reserves the right, at its own cost, to assume the exclusive defense and control of any matter otherwise subject to indemnification by you, in which event you will fully cooperate with Two Movers Network in asserting any available defenses. </p>

                <h1 class="title">Class Action Waiver </h1>
                <p>Any arbitration under these Terms and Conditions will take place on an individual basis; class arbitrations and class/representative/collective actions are not permitted. THE PARTIES AGREE THAT A PARTY MAY BRING CLAIMS AGAINST THE OTHER ONLY IN EACH'S INDIVIDUAL CAPACITY, AND NOT AS A PLAINTIFF OR CLASS MEMBER IN ANY PUTATIVE CLASS, COLLECTIVE AND/ OR REPRESENTATIVE PROCEEDING, SUCH AS IN THE FORM OF A PRIVATE ATTORNEY GENERAL ACTION AGAINST THE OTHER. Further, unless both you and Two Movers Network agree otherwise, the arbitrator may not consolidate more than one person's claims, and may not otherwise preside over any form of a representative or class proceeding. </p>

                <h1 class="title">Liability Disclaimer</h1>
                <p>THE INFORMATION, SOFTWARE, PRODUCTS, AND SERVICES INCLUDED IN OR AVAILABLE THROUGH THE SITE MAY INCLUDE INACCURACIES OR TYPOGRAPHICAL ERRORS. CHANGES ARE PERIODICALLY ADDED TO THE INFORMATION HEREIN. TWO MOVERS NETWORK AND/OR ITS SUPPLIERS MAY MAKE IMPROVEMENTS AND/OR CHANGES IN THE SITE AT ANY TIME. </p>
                <p>TWO MOVERS NETWORK AND/OR ITS SUPPLIERS MAKE NO REPRESENTATIONS ABOUT THE SUITABILITY, RELIABILITY, AVAILABILITY, TIMELINESS, AND ACCURACY OF THE INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS CONTAINED ON THE SITE FOR ANY PURPOSE. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, ALL SUCH INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS ARE PROVIDED "AS IS" WITHOUT WARRANTY OR CONDITION OF ANY KIND. TWO MOVERS NETWORK AND/OR ITS SUPPLIERS HEREBY DISCLAIM ALL WARRANTIES AND CONDITIONS WITH REGARD TO THIS INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS, INCLUDING ALL IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. </p>
                <p>TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL TWO MOVERS NETWORK AND/OR ITS SUPPLIERS BE LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF USE, DATA OR PROFITS, ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OR PERFORMANCE OF THE SITE, WITH THE DELAY OR INABILITY TO USE THE SITE OR RELATED SERVICES, THE PROVISION OF OR FAILURE TO PROVIDE SERVICES, OR FOR ANY INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS OBTAINED THROUGH THE SITE, OR OTHERWISE ARISING OUT OF THE USE OF THE SITE, WHETHER BASED ON CONTRACT, TORT, NEGLIGENCE, STRICT LIABILITY OR OTHERWISE, EVEN IF TWO MOVERS NETWORK OR ANY OF ITS SUPPLIERS HAS BEEN ADVISED OF THE POSSIBILITY OF DAMAGES. BECAUSE SOME STATES/JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, THE ABOVE LIMITATION MAY NOT APPLY TO YOU. IF YOU ARE DISSATISFIED WITH ANY PORTION OF THE SITE, OR WITH ANY OF THESE TERMS OF USE, YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE USING THE SITE. </p>


                <h1 class="title">Termination/Access Restriction</h1>
                <p>Two Movers Network reserves the right, in its sole discretion, to terminate your access to the Site and the related services or any portion thereof at any time, without notice. To the maximum extent permitted by law, this agreement is governed by the laws of the State of Wyoming and you hereby consent to the exclusive jurisdiction and venue of courts in Wyoming in all disputes arising out of or relating to the use of the Site. Use of the Site is unauthorized in any jurisdiction that does not give effect to all provisions of these Terms, including, without limitation, this section. </p>
                <p>You agree that no joint venture, partnership, employment, or agency relationship exists between you and Two Movers Network as a result of this agreement or use of the Site. Two Movers Network's performance of this agreement is subject to existing laws and legal process, and nothing contained in this agreement is in derogation of Two Movers Network's right to comply with governmental, court and law enforcement requests or requirements relating to your use of the Site or information provided to or gathered by Two Movers Network with respect to such use. If any part of this agreement is determined to be invalid or unenforceable pursuant to applicable law including, but not limited to, the warranty disclaimers and liability limitations set forth above, then the invalid or unenforceable provision will be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision and the remainder of the agreement shall continue in effect. </p>
                <p>Unless otherwise specified herein, this agreement constitutes the entire agreement between the user and Two Movers Network with respect to the Site and it supersedes all prior or contemporaneous communications and proposals, whether electronic, oral or written, between the user and Two Movers Network with respect to the Site. A printed version of this agreement and of any notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or relating to this agreement to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form. It is the express wish to the parties that this agreement and all related documents be written in English. </p>

                <h1 class="title">Changes to Terms</h1>
                <p>Two Movers Network reserves the right, in its sole discretion, to change the Terms under which www.2movers.com is offered. The most current version of the Terms will supersede all previous versions. Two Movers Network encourages you to periodically review the Terms to stay informed of our updates. </p>

                <h1 class="title">Contact Us</h1>
                <p>Two Movers Network welcomes your questions or comments regarding the Terms:</p>
                <address class="address">Two Movers Network<br>
                    412 N. Main St Ste 100 <br>
                    Buffalo, Wyoming 82834
                </address>

                <address class="address">Email Address: <br>
                    info@2movers.com <br>
                    Telephone number: <br>
                    877-884-4738 <br>
                    Effective as of July 01, 2017
                </address>
            </div>
            </div>
        </div>
        <a class="popup-outer-close"></a>
    </div>

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/contact/ccpa.js"></script>

</body>

</html>

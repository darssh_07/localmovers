//classList polyfil
"document"in self&&("classList"in document.createElement("_")&&(!document.createElementNS||"classList"in document.createElementNS("http://www.w3.org/2000/svg","g"))||function(t){"use strict";if("Element"in t){var e=t.Element.prototype,n=Object,i=String.prototype.trim||function(){return this.replace(/^\s+|\s+$/g,"")},s=Array.prototype.indexOf||function(t){for(var e=0,n=this.length;e<n;e++)if(e in this&&this[e]===t)return e;return-1},r=function(t,e){this.name=t,this.code=DOMException[t],this.message=e},o=function(t,e){if(""===e)throw new r("SYNTAX_ERR","An invalid or illegal string was specified");if(/\s/.test(e))throw new r("INVALID_CHARACTER_ERR","String contains an invalid character");return s.call(t,e)},c=function(t){for(var e=i.call(t.getAttribute("class")||""),n=e?e.split(/\s+/):[],s=0,r=n.length;s<r;s++)this.push(n[s]);this._updateClassName=function(){t.setAttribute("class",this.toString())}},a=c.prototype=[],l=function(){return new c(this)};if(r.prototype=Error.prototype,a.item=function(t){return this[t]||null},a.contains=function(t){return t+="",-1!==o(this,t)},a.add=function(){var t,e=arguments,n=0,i=e.length,s=!1;do{t=e[n]+"",-1===o(this,t)&&(this.push(t),s=!0)}while(++n<i);s&&this._updateClassName()},a.remove=function(){var t,e,n=arguments,i=0,s=n.length,r=!1;do{for(t=n[i]+"",e=o(this,t);-1!==e;)this.splice(e,1),r=!0,e=o(this,t)}while(++i<s);r&&this._updateClassName()},a.toggle=function(t,e){t+="";var n=this.contains(t),i=n?!0!==e&&"remove":!1!==e&&"add";return i&&this[i](t),!0===e||!1===e?e:!n},a.toString=function(){return this.join(" ")},n.defineProperty){var u={get:l,enumerable:!0,configurable:!0};try{n.defineProperty(e,"classList",u)}catch(t){void 0!==t.number&&-2146823252!==t.number||(u.enumerable=!1,n.defineProperty(e,"classList",u))}}else n.prototype.__defineGetter__&&e.__defineGetter__("classList",l)}}(self),function(){"use strict";var t=document.createElement("_");if(t.classList.add("c1","c2"),!t.classList.contains("c2")){var e=function(t){var e=DOMTokenList.prototype[t];DOMTokenList.prototype[t]=function(t){var n,i=arguments.length;for(n=0;n<i;n++)t=arguments[n],e.call(this,t)}};e("add"),e("remove")}if(t.classList.toggle("c3",!1),t.classList.contains("c3")){var n=DOMTokenList.prototype.toggle;DOMTokenList.prototype.toggle=function(t,e){return 1 in arguments&&!this.contains(t)==!e?e:n.call(this,t)}}t=null}());
//indexOf polyfil
Array.prototype.indexOf||(Array.prototype.indexOf=function(i,e){var r=this.length>>>0;if((e|=0)<0)e=Math.max(r-e,0);else if(e>=r)return-1;if(void 0===i){do{if(e in this&&void 0===this[e])return e}while(++e!==r)}else do{if(this[e]===i)return e}while(++e!==r);return-1});
var answers = {
	"_token": document.getElementById("token").value,
	"email": "test@test.com",
	"first_name": "",
	"last_name": "",
	"phone": "",
	"phone_formatted": "",
	"phone_confirmed": false,
	"initial_reveal": false
},
calculate = {
	miles: {
		post: function () {
			console.log("calculating miles")
			var service = new google.maps.DistanceMatrixService();
			service.getDistanceMatrix({
				origins: [String(answers.from_city) + ", " + String(answers.from_state)],
				destinations: [String(answers.to_city) + ", " + String(answers.to_state)],
				travelMode: "DRIVING",
				unitSystem: google.maps.UnitSystem.IMPERIAL,
				region: 'us'
			}, calculate.miles.get);
		},
		get: function (res) {
			var dist
			if (util.isArray(res)) {
				dist = res.rows[0].elements[0].distance.text
				if (dist.indexOf("ft") > -1) {
					answers.miles = "1"
				} else if (dist.indexOf("mi") > -1) {
					answers.miles = dist.replace(" mi", "")
				}
			}
			util.xhr("POST", "/moving/send", [
				{"first_name": answers.first_name},
				{"last_name": answers.last_name},
				{"email": answers.email},
				{"phone": answers.phone},
				{"move_size": answers.move_size},
				{"move_date": answers.move_date},
				{"from_zip_code": answers.from_zip},
				{"to_zip_code": answers.to_zip},
				{"source": answers.source},
				{"_token": answers._token}
			], calculate.quote.post)
		}
	},
	quote: {
		post: function () {
			util.xhr("POST", "/calc", [
				{"rooms": answers.lead.move_size},
				{"miles": answers.miles},
				{"_token": answers._token}
			], calculate.quote.get)
		},
		get: function (res) {
			if (res) {
				console.log(res.min)
				console.log(res.max)
				answers.quote_min = res.min
				answers.quote_max = res.max
				enter("thank_you")
			} else {
				console.error("Failed to calculate quote")
			}
		}
	}
},
enter = function (trg) {
	var main = document.getElementById("main"),
		input, size, str
	trg = util.getElem(trg)
	console.log("Entering: " + trg.id)
	navigation.push(trg.id)
	switch(trg.id) {
		case "name_card":
			transition.card(trg.elem, ["left_in", "instant"], interact.add_listeners, trg.elem)
		break
		case "number_card":
			if (answers.initial_reveal) {
				transition.card("name_card","left_out")
				setTimeout(function (elem) {
					transition.card(elem, "left_in", interact.add_listeners, elem)
				}, 1E2, trg.elem)
			} else {
				transition.card(trg.elem, ["left_in", "instant"], interact.add_listeners, trg.elem)
			}
		break
		case "confirm_number_card":
			document.getElementById("confirm_number_input").value = answers.phone_formatted
			validate.confirm_number.post(false)
			transition.card("number_card","left_out")
			setTimeout(function (elem) {
				transition.card(elem, "left_in", interact.add_listeners, elem)
			}, 1E2, trg.elem)
		break
		case "code_card":
			switch(answers.confirm_type) {
				case "text":
					document.getElementById("code_title").innerHTML = "SMS Code Sent to: " + answers.phone_formatted
				break
				case "call":
					document.getElementById("code_title").innerHTML = "We are Calling: " + answers.phone_formatted
				break
			}
			transition.card("confirm_number_card","left_out")
			setTimeout(function (elem) {
				transition.card(elem, "left_in", interact.add_listeners, elem)
			}, 1E2, trg.elem)
		break
		case "thank_you":
			document.getElementById("quote_min").innerHTML = answers.quote_min
			document.getElementById("quote_max").innerHTML = answers.quote_max
			transition.fade(main, "out")
			setTimeout(function (elem, m, s) {
				document.getElementById("title").innerHTML = "Thank You!"
				util.addStyle("fields", "display", "none")
				s = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName("body")[0].clientHeight
				console.log(s)
				s -= (util.getSize("header", "height") + util.getSize("title", "height") + 16 + util.getSize("footer", "height"))
				util.addStyle(trg.elem, ["display", "minHeight"], ["block", s + "px"])
			}, 6E2, trg.elem, main, size)
			setTimeout(function (elem, m) {
				transition.fade(m, "in")
				transition.fade(elem, "in", interact.add_listeners, elem)
			}, 8E2, trg.elem, main)
		break
	}
	if (!answers.initial_reveal) {
		transition.fade("main", "in")
		transition.fade("footer", "in")
		answers.initial_reveal = true
		interact.add_listeners("edit_details_toggle")

		input = document.getElementById("moving_from_input")
		input.value = answers.from_city + ", " + answers.from_state
		input.dataset.zip = answers.from_zip

		input = document.getElementById("move_size_input")
		size = answers.move_size
		if (size.indexOf("house") > -1) {
			if (size.indexOf("four") > -1) {
				size = "4+ Bedrooms"
			} else if (size.indexOf("three") > -1) {
				size = "3 Bedrooms"
			} else if (size.indexOf("two") > -1) {
				size = "2 Bedrooms"
			} else if (size.indexOf("one") > -1) {
				size = "1 Bedroom"
			} else {
				size = "Studio"
			}
		} else {
			size = "Office/Commercial"
		}
		input.value = size
		input.dataset.formatted = answers.move_size

		input = document.getElementById("move_date_input")
		str = answers.move_date.split("-")
		input.value = str[1] + "/" + str[2] + "/" + str[0]
		input.dataset.formatted = answers.move_date.replace("-", "/")

		input = document.getElementById("moving_to_input")
		input.value = answers.to_city + ", " + answers.to_state
		input.dataset.zip = answers.to_zip

		setTimeout(function () {
			var child = document.createElement("script")
			child.setAttribute("async", "")
			child.setAttribute("defer", "")
			child.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBhed6zw7gtrGzE1qSZ26RaL6ceDL_EVeo"
			child.type = "text/javascript"
			document.getElementById("main").appendChild(child)
		}, 2E3)
	}
},
interact = {
	add_listeners: function (trg) {
		var obj = {}
		trg = util.getElem(trg)
		console.log("Adding listeners to: " + trg.id)
		switch(trg.id) {
			case "name_card":
				obj.first_name = document.getElementById("first_name_input")
				obj.last_name = document.getElementById("last_name_input")
				obj.submit = document.getElementById("name_submit")

				obj.first_name.addEventListener("focus", interact.focus.first_name)
				obj.first_name.addEventListener("keyup", interact.typing.first_name)
				obj.first_name.addEventListener("blur", interact.blur.first_name)

				obj.last_name.addEventListener("focus", interact.focus.last_name)
				obj.last_name.addEventListener("keyup", interact.typing.last_name)
				obj.last_name.addEventListener("blur", interact.blur.last_name)

				obj.submit.addEventListener("click", validate.name.post_first)
			break
			case "number_card":
				obj.input = document.getElementById("number_input")
				obj.submit = document.getElementById("number_submit")

				obj.input.addEventListener("focus", interact.focus.number)
				obj.input.addEventListener("keyup", interact.typing.number)
				obj.input.addEventListener("blur", interact.blur.number)

				obj.submit.addEventListener("click", validate.number.post)
			break
			case "confirm_number_card":
				obj.input = document.getElementById("confirm_number_input")
				obj.submit_text = document.getElementById("confirm_number_submit_text")
				obj.submit_call = document.getElementById("confirm_number_submit_call")

				obj.input.addEventListener("focus", interact.focus.confirm_number)
				obj.input.addEventListener("keyup", interact.typing.confirm_number)
				obj.input.addEventListener("blur", interact.blur.confirm_number)

				obj.submit_text.addEventListener("click", validate.confirm_number.text)
				obj.submit_call.addEventListener("click", validate.confirm_number.call)
			break
			case "code_card":
				obj.input = document.getElementById("code_input")
				obj.submit = document.getElementById("code_submit")

				obj.input.addEventListener("focus", interact.focus.code)
				obj.input.addEventListener("keyup", interact.typing.code)

				obj.submit.addEventListener("click", validate.code.post)
			break
			case "edit_details_toggle":
				obj.prv_btn = document.getElementById("privacy_btn")
				obj.prv_cls = document.getElementById("terms_btn")
				obj.trm_btn = document.getElementById("privacy_policy_close")
				obj.trm_cls = document.getElementById("terms_of_use_close")

				trg.elem.addEventListener("click", transition.accordion)

				obj.prv_btn.addEventListener("click", interact.open.privacy)
				obj.prv_cls.addEventListener("click", interact.close.privacy)
				obj.trm_btn.addEventListener("click", interact.open.terms)
				obj.trm_cls.addEventListener("click", interact.close.terms)
			break
			case "edit_details":
				obj.from = document.getElementById("moving_from_input")
				obj.size = document.getElementById("move_size_input")
				obj.date = document.getElementById("move_date_input")
				obj.to = document.getElementById("moving_to_input")

				obj.from.addEventListener("focus", interact.focus.moving_from)
				obj.from.addEventListener("keyup", interact.dropdown.moving_from.search)
				obj.from.addEventListener("blur", interact.blur.moving_from)

				obj.size.addEventListener("focus", interact.focus.move_size)

				obj.date.addEventListener("focus", interact.focus.move_date)

				obj.to.addEventListener("focus", interact.focus.moving_to)
				obj.to.addEventListener("keyup", interact.dropdown.moving_to.search)
				obj.to.addEventListener("blur", interact.blur.moving_to)
			break
			case "moving_from_dropdown":
				var children = document.getElementById("moving_from_results").childNodes,
					child, i
				trg.elem.addEventListener("click", function (event) {
					util.events.cancel(event)
				})
				for (i = 0; i < children.length; i += 1) {
					child = children[i]
					if (util.isElem(child)) {
						child.addEventListener("click", interact.dropdown.moving_from.choose)
					}
				}
				document.getElementById("dropdowns").removeEventListener("click", interact.dropdown.move_size.close)
				document.getElementById("dropdowns").removeEventListener("click", interact.dropdown.move_date.close)
				document.getElementById("dropdowns").removeEventListener("click", interact.dropdown.moving_to.close)
				document.getElementById("dropdowns").addEventListener("click", interact.dropdown.moving_from.close)
			break
			case "move_size_dropdown":
				var children = document.getElementById("move_size_results").childNodes, child, i
				trg.elem.addEventListener("click", function (event) {
					util.events.cancel(event)
				})
				for (i = 0; i < children.length; i += 1) {
					child = children[i]
					if (util.isElem(child)) {
						child.addEventListener("click", interact.dropdown.move_size.choose)
					}
				}
				document.getElementById("dropdowns").removeEventListener("click", interact.dropdown.moving_from.close)
				document.getElementById("dropdowns").removeEventListener("click", interact.dropdown.move_date.close)
				document.getElementById("dropdowns").removeEventListener("click", interact.dropdown.moving_to.close)
				document.getElementById("dropdowns").addEventListener("click", interact.dropdown.move_size.close)
			break
			case "move_date_dropdown":
				trg.elem.addEventListener("click", function (event) {
					util.events.cancel(event)
				})
				var parent, child, children, i
				parent = document.getElementById("month_0_days")
				if (!util.isElem(parent)) {
					parent = document.getElementById("month_1_days")
				}
				if (util.isElem(parent)) {
					children = parent.childNodes
					for (i = 0; i < children.length; i += 1) {
						child = children[i]
						if (util.isElem(child)) {
							child.addEventListener("click", interact.dropdown.move_date.choose_day)
						}
					}
				}
				document.getElementById("next_month_btn").addEventListener("click", interact.dropdown.move_date.choose_dir)
				document.getElementById("last_month_btn").addEventListener("click", interact.dropdown.move_date.choose_dir)
				document.getElementById("dropdowns").removeEventListener("click", interact.dropdown.moving_from.close)
				document.getElementById("dropdowns").removeEventListener("click", interact.dropdown.move_size.close)
				document.getElementById("dropdowns").removeEventListener("click", interact.dropdown.moving_to.close)
				document.getElementById("dropdowns").addEventListener("click", interact.dropdown.move_date.close)
			break
			case "moving_to_dropdown":
				var children = document.getElementById("moving_to_results").childNodes,
					child, i
				trg.elem.addEventListener("click", function (event) {
					util.events.cancel(event)
				})
				for (i = 0; i < children.length; i += 1) {
					child = children[i]
					if (util.isElem(child)) {
						child.addEventListener("click", interact.dropdown.moving_to.choose)
					}
				}
				document.getElementById("dropdowns").removeEventListener("click", interact.dropdown.moving_from.close)
				document.getElementById("dropdowns").removeEventListener("click", interact.dropdown.move_size.close)
				document.getElementById("dropdowns").removeEventListener("click", interact.dropdown.move_date.close)
				document.getElementById("dropdowns").addEventListener("click", interact.dropdown.moving_to.close)
			break
			case "thank_you":
			break
		}
	},
	blur: {
		first_name: function () {
			var input = document.getElementById("first_name_input"),
				label = document.getElementById("first_name_label")
			if (input.value.length) {
				validate.name.post_first(false)
			} else {
				label.classList.remove("valid")
			}
		},
		last_name: function () {
			var input = document.getElementById("last_name_input"),
				label = document.getElementById("last_name_label")
			if (input.value.length) {
				validate.name.post_last(false)
			} else {
				label.classList.remove("valid")
			}
		},
		number: function () {
			var input = document.getElementById("number_input"),
				label = document.getElementById("number_label")
			if (input.value.length) {
				validate.number.post(false)
			} else {
				label.classList.remove("valid")
			}
		},
		confirm_number: function () {
			var input = document.getElementById("confirm_number_input"),
				label = document.getElementById("confirm_number_label")
			if (input.value.length) {
				validate.confirm_number.post(false)
			} else {
				label.classList.remove("valid")
			}
		},
		moving_from: function () {
			var input = document.getElementById("moving_from_input")
			if (!input.value.length) {
				input.value = input.placeholder
			}
			input.placeholder = ""
		},
		moving_to: function () {
			var input = document.getElementById("moving_to_input")
			if (!input.value.length) {
				input.value = input.placeholder
			}
			input.placeholder = ""
		}
	},
	close: {
		privacy: function () {
			transition.fade("out", "privacy_policy")
		},
		terms: function () {
			transition.fade("out", "terms_of_use")
		}
	},
	dropdown: {
		moving_from: {
			choose: function (event) {
				util.events.cancel(event)
				var input = document.getElementById("moving_from_input"),
					trg, arr
				console.log("clicked li")
				trg = util.getTrg(event)
				trg = util.getElem(trg)
				input.value = trg.elem.innerHTML
				arr = trg.elem.innerHTML.split(", ")
				answers.from_city = arr[0]
				answers.from_state = arr[1]
				input.dataset.zip = trg.elem.dataset.zip
				answers.from_zip = input.dataset.zip
				validate.moving_from.post(interact.dropdown.moving_from.close)
			},
			close: function () {
				document.getElementById("moving_from_label").classList.remove("active")
				transition.expand("moving_from_dropdown", "close", "moving_from_input")
			},
			display_results: function (res) {
				var children, child, i, li, result, results = [], ul = document.getElementById("moving_from_results")
				ul.innerHTML = ""
				if (res !== "[]") {
					for (i in res) {
						results.push([i, res[i]])
					}
				}
				console.log(results)
				if (results.length) {
					console.log("drawing results")
					for (i = 0; i < results.length; i += 1) {
						result = results[i]
						li = document.createElement("li")
						li.innerHTML = result[0]
						li.dataset.zip = result[1]
						ul.appendChild(li)
					}
					interact.add_listeners("moving_from_dropdown")
					setTimeout(function () {
						transition.expand("moving_from_dropdown", "open", "moving_from_input")
					}, 1E2)
				} else {
					li = document.createElement("li")
					li.className = "disabled"
					li.innerHTML = "No Results"
					ul.appendChild(li)
				}
			},
			search: function () {
				util.xhr("POST", "/validate/searchForCityState", [
					{"search_term": document.getElementById("moving_from_input").value},
					{"mobile": false}
				], interact.dropdown.moving_from.display_results)
			},
			show: function () {
				var dir = "open",
					label = document.getElementById("moving_from_label"),
					input = document.getElementById("moving_from_input"),
					trg = util.getElem("moving_from_dropdown")
				if (label.classList.contains("active")) {
					label.classList.remove("active")
					dir = "close"
				} else {
					label.classList.add("active")
					interact.dropdown.moving_from.search()
				}
				setTimeout(function (elem, dir, sib) {
					transition.expand(elem, dir, sib)
					interact.add_listeners("moving_from_dropdown")
				}, 100, trg.elem, dir, input)
			}
		},
		move_size: {
			choose: function (event) {
				util.events.cancel(event)
				console.log("choosing move size")
				var trg = util.getTrg(event), input = document.getElementById("move_size_input")
				trg = util.getElem(trg)
				input.value = trg.elem.innerHTML
				input.dataset.formatted = trg.elem.dataset.formatted
				answers.move_size = input.dataset.formatted
				validate.move_size.post(interact.dropdown.move_size.close)
			},
			close: function () {
				document.getElementById("move_size_label").classList.remove("active")
				transition.expand("move_size_dropdown", "close", "move_size_input")
			},
			show: function () {
				var btn = document.getElementById("move_size_btn"),
					dir = "open",
					label = document.getElementById("move_size_label"),
					input = document.getElementById("move_size_input"),
					size = util.getSize("move_size_input"),
					trg = util.getElem("move_size_dropdown")
				if (label.classList.contains("active")) {
					label.classList.remove("active")
					dir = "close"
				} else {
					label.classList.add("active")
				}
				setTimeout(function (elem, dir, sib) {
					transition.expand(elem, dir, sib)
					interact.add_listeners("move_size_dropdown")
				}, 100, trg.elem, dir, input)
			}
		},
		move_date: {
			choose_day: function (event) {
				util.events.cancel(event)
				var arr, input = document.getElementById("move_date_input")
				trg = util.getTrg(event)
				trg = util.getElem(trg)
				if (!trg.elem.classList.contains("past")) {
					input.value = trg.elem.dataset.date
					arr = trg.elem.dataset.date.split("/")
					input.dataset.formatted = arr[2] + "/" + arr[0] + "/" + arr[1]
					answers.move_date = input.dataset.formatted
					validate.move_date.post(interact.dropdown.move_date.show)
				}
			},
			choose_dir: function (event) {
				util.events.cancel(event)
				var trg = util.getTrg(event)
				trg = util.getElem(trg)
				util.addStyle(trg.elem, "pointerEvents", "none")
				interact.dropdown.move_date.display_month(trg.elem.dataset.month_plus, trg.elem.dataset.year_plus)
			},
			close: function () {
				document.getElementById("move_date_label").classList.remove("active")
				transition.expand("move_date_dropdown", "close", "move_date_input")
			},
			display_month: function (month_plus, year_plus) {
				var d, nd,
					i, j, str,
					day, new_day,
					weekday, new_weekday,
					year, new_year, last_year,
					month, new_month, last_month,
					start_day, total_days = [
						31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
					], month_names = [
						"JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"
					],
					matrix = [],
					btn, lm_btn = document.getElementById("last_month_btn"),
					nm_btn = document.getElementById("next_month_btn"),
					title = document.getElementById("date_title"),
					container = document.getElementById("move_date_body"),
					parent,
					sibling,
					child
				//please dont run two at once, thanks:)
				util.addStyle(nm_btn, "pointerEvents", "none")
				util.addStyle(lm_btn, "pointerEvents", "none")
				  ///////////////////////////////////////////////
				 /////// perform all calculations first ////////
				///////////////////////////////////////////////
				d = new Date()
				day = d.getDate()
				weekday = d.getDay()
				month = d.getMonth()
				year = d.getFullYear()
				if (typeof year_plus !== "undefined") {
					year_plus = Number(year_plus)
				} else {
					year_plus = 0
				}
				if (typeof month_plus !== "undefined") {
					month_plus = Number(month_plus)
					while((month + month_plus) > 11) {
						month_plus -= 12
						year_plus += 1
					}
				} else {
					month_plus = 0
				}
				console.log("Displaying month: (" + month_plus + ", " + year_plus + ")")
				if (year_plus || month_plus) {
					nd = new Date(year + year_plus, month + month_plus)
				} else {
					nd = d
				}
				new_day = nd.getDate()
				new_weekday = nd.getDay()
				new_month = nd.getMonth()
				console.log(new_month)
				new_year = nd.getFullYear()
				if (year_plus || month_plus) {
					i = new_year
				} else {
					i = year
				}
				if (((i % 4 == 0) && (i % 100 != 0)) || (i % 400 == 0)) {
					total_days[1] = 29
				}
				if (year_plus > -1 || month_plus > -1) {
					i = new_day
					j = new_weekday
				} else {
					i = day
					j = weekday
				}
				while(i > 7) {
					i -= 7
				}
				if (i > 1) {
					j -= (i - 1)//subtract to make it fit weekday format
					if (j < 1) {
						j += 7
					}
				}
				start_day = j
				if (year_plus > -1 || month_plus > -1) {
					j = new_month - 1
					if (j < 0) {
						j = 11
					}
				} else {
					j = month
				}
				if (start_day !== 0) {
					switch(start_day) {
						case 7:
							matrix.push(total_days[j] - 6)
						case 6:
							matrix.push(total_days[j] - 5)
						case 5:
							matrix.push(total_days[j] - 4)
						case 4:
							matrix.push(total_days[j] - 3)
						case 3:
							matrix.push(total_days[j] - 2)
						case 2:
							matrix.push(total_days[j] - 1)
						case 1:
							matrix.push(total_days[j])
					}
				}
				for (i = 1; i <= total_days[j]; i += 1) {
					matrix.push(i)
				}
				  /////////////////////////////////////////////////////////////
				 //////////////////then apply changes to the dom//////////////
				/////////////////////////////////////////////////////////////

					//nav buttons
					nm_btn.dataset.month_plus = month_plus + 1
					nm_btn.dataset.year_plus = year_plus
					i = month_plus - 1
					j = year_plus
					while (j > 0) {
						i += 12
						j -= 1
					}
					lm_btn.dataset.month_plus = i
					lm_btn.dataset.year_plus = j
					if (i < 0 && j <= 0) {
						lm_btn.classList.add("disabled")
					} else {
						lm_btn.classList.remove("disabled")
					}
					if (j >= 1 || i >= 2) {
						nm_btn.classList.add("disabled")
					} else {
						nm_btn.classList.remove("disabled")
					}
					//month+year title
					if (year_plus > -1 || month_plus > -1) {
						str = month_names[new_month]
						if (new_year !== year) {
							str += " " + new_year
						}
					} else {
						str = month_names[month]
					}
					child = document.createElement("span")
					child.className = "anim date_title_text"
					child.innerHTML = str
					sibling = document.getElementById("date_title_0")
					if (util.isElem(sibling)) {
						child.id = "date_title_1"
					} else {
						sibling = document.getElementById("date_title_1")
						child.id = "date_title_0"
					}
					if(util.isElem(sibling)) {
						child.style = "opacity:0"
						title.appendChild(child)
						transition.fade(sibling, "out")
						setTimeout(function (c) {
							transition.fade(c, "in")
						}, 2E2, child)
						setTimeout(function (t, s) {
							t.removeChild(s)
						}, 6E2, title, sibling)
					} else {
						title.appendChild(child)
					}
					//month object
					parent = document.createElement("div")
					sibling = document.getElementById("month_0")
					if (util.isElem(sibling)) {
						parent.id = "month_1"
					} else {
						sibling = document.getElementById("month_1")
						parent.id = "month_0"
						if (!util.isElem(sibling)) {
							sibling = false
						}
					}
					if (sibling !== false) {
						last_month = Number(sibling.dataset.month)
						last_year = Number(sibling.dataset.year)
					} else {
						last_month = "undefined"
						last_year = "undefined"
					}

					console.log("lastmonth: " + last_month + ", thismonth: " + new_month)
					console.log("lastyear: " + last_year + ", thisyear: " + new_year)
					if (last_month !== new_month || last_year !== new_year) {
						console.log("it is not the same date")
						parent.className = "anim month"
						parent.innerHTML = "<div class='month_header'><p>Su</p><p>Mo</p><p>Tu</p><p>We</p><p>Th</p><p>Fr</p><p>Sa</p></div><div id='" + parent.id + "_days' class='days'></div>"
						parent.dataset.month = new_month
						parent.dataset.year = new_year
						container.appendChild(parent)
						child = document.getElementById(parent.id + "_days")
						for (i = 0; i < matrix.length; i += 1) {
							btn = document.createElement("button")
							btn.type = "button"
							if (!month_plus && !year_plus) {
								if ((i < 7 && matrix[i] > 7) || (matrix[i] < day)) {
									btn.className = "past"
								}
							}
							if (i < 7 && matrix[i] > 7) {
								btn.dataset.date = Number(new_month)
							} else {
								btn.dataset.date = Number(new_month) + 1
							}
							btn.dataset.date += "/" + matrix[i] + "/" + new_year
							btn.innerHTML = matrix[i]
							child.appendChild(btn)
						}
					}
					if (sibling !== false) {
						if (last_month !== new_month || last_year !== new_year) {
							console.log("building new month, sibling exists")
							console.log("NEW: " + new_month + ", LAST: " + last_month)
							console.log("NEW: " + new_year + ", LAST: " + last_year)
							if (new_year < last_year) {
								i = "right_out"
								j = "right_in"
								console.log("going back because of year")
							} else if (new_year > last_year) {
								i = "left_out"
								j = "left_in"
								console.log("going forward because of year")
							} else if (new_year == last_year) {
								if (new_month < last_month) {
									i = "right_out"
									j = "right_in"
									console.log("going back because of month")
								} else {
									i = "left_out"
									j = "left_in"
									console.log("going forward becasue of month")
								}
							}
							if (j == "right_in") {
								parent.classList.add("right_in")
								setTimeout(function(p) {
									p.classList.remove("right_in")
								}, 7E2, parent)
							}
							setTimeout(function (s, i) {
								transition.month(s, [i, "3E2"])
							}, 2E2, sibling, i)
							setTimeout(function (p, j) {
								transition.month(p, [j, "3E2"])
							}, 3E2, parent, j)
							setTimeout(function (ct, sb) {
								console.log("removing: " + sb.id)
								ct.removeChild(sb)
							}, 6E2, container, sibling)
							setTimeout(function (lm, nm) {
								util.addStyle(nm_btn, "pointerEvents", "")
								util.addStyle(lm_btn, "pointerEvents", "")
								interact.add_listeners("move_date_dropdown")
							}, 7E2, lm_btn, nm_btn)
						}
					} else {
						console.log("building month, sibling doesnt exist")
						setTimeout(function (p) {
							transition.month(p, ["left_in", "instant"])
						}, 1E2, parent)
						setTimeout(function (nm, lm) {
							util.addStyle(nm, "pointerEvents", "")
							util.addStyle(lm, "pointerEvents", "")
							interact.add_listeners("move_date_dropdown")
						}, 2E2, nm_btn, lm_btn)

				}
			},
			show: function () {
				var dir = "open",
					label = document.getElementById("move_date_label"),
					input = document.getElementById("move_date_input"),
					trg = util.getElem("move_date_dropdown")
				if (label.classList.contains("active")) {
					label.classList.remove("active")
					dir = "close"
				} else {
					interact.dropdown.move_date.display_month()
					label.classList.add("active")
				}
				setTimeout(function (elem, dir, sib) {
					transition.expand(elem, dir, sib)
					interact.add_listeners("move_date_dropdown")
				}, 100, trg.elem, dir, input)
			},
		},
		moving_to: {
			choose: function (event) {
				util.events.cancel(event)
				var input = document.getElementById("moving_to_input"),
					trg, arr
				trg = util.getTrg(event)
				trg = util.getElem(trg)
				input.value = trg.elem.innerHTML
				arr = input.value.split(", ")
				answers.to_city = arr[0]
				answers.to_state = arr[1]
				input.dataset.zip = trg.elem.dataset.zip
				answers.to_zip = input.dataset.zip
				validate.moving_to.post(interact.dropdown.moving_to.close)
			},
			close: function () {
				document.getElementById("moving_to_label").classList.remove("active")
				transition.expand("moving_to_dropdown", "close", "moving_to_input")
			},
			display_results: function (res) {
				var children, child, i, li, result, results = [], ul = document.getElementById("moving_to_results")
				ul.innerHTML = ""
				if (res !== "[]") {
					for (i in res) {
						results.push([i, res[i]])
					}
				}
				console.log(results)
				if (results.length) {
					console.log("drawing results")
					for (i = 0; i < results.length; i += 1) {
						result = results[i]
						li = document.createElement("li")
						li.innerHTML = result[0]
						li.dataset.zip = result[1]
						ul.appendChild(li)
					}
					interact.add_listeners("moving_to_dropdown")
					setTimeout(function () {
						transition.expand("moving_to_dropdown", "open", "moving_to_input")
					}, 1E2)
				} else {
					li = document.createElement("li")
					li.className = "disabled"
					li.innerHTML = "No Results"
					ul.appendChild(li)
				}
			},
			search: function () {
				util.xhr("POST", "/validate/searchForCityState", [
					{"search_term": document.getElementById("moving_to_input").value},
					{"mobile": false}
				], interact.dropdown.moving_to.display_results)
			},
			show: function () {
				var btn = document.getElementById("moving_to_btn"),
					dir = "open",
					label = document.getElementById("moving_to_label"),
					input = document.getElementById("moving_to_input"),
					size = util.getSize("moving_to_input"),
					trg = util.getElem("moving_to_dropdown")
				if (label.classList.contains("active")) {
					label.classList.remove("active")
					dir = "close"
				} else {
					interact.dropdown.moving_to.search()
					label.classList.add("active")
				}
				setTimeout(function (elem, dir, sib) {
					transition.expand(elem, dir, sib)
				}, 100, trg.elem, dir, input)
			}
		}
	},
	focus: {
		first_name: function () {
			util.inputPos("first_name_input")
		},
		last_name: function () {
			util.inputPos("last_name_input")
		},
		number: function () {
			util.inputPos("number_input")
		},
		confirm_number: function () {
			util.inputPos("confirm_number_input")
		},
		code: function () {
			util.inputPos("code_input")
		},
		moving_from: function () {
			var input = document.getElementById("moving_from_input")
			input.placeholder = input.value
			input.value = ""
			util.inputPos(input)
			interact.dropdown.moving_from.show()
		},
		move_size: function () {
			util.inputPos("move_size_input")
			interact.dropdown.move_size.show()
		},
		move_date: function () {
			util.inputPos("move_date_input")
			interact.dropdown.move_date.show()
		},
		moving_to: function () {
			var input = document.getElementById("moving_to_input")
			input.placeholder = input.value
			input.value = ""
			util.inputPos(input)
			interact.dropdown.moving_to.show()
		}
	},
	open: {
		privacy: function () {
			console.log("ope n...")
			transition.fade("in", document.getElementById("privacy_policy"))
		},
		terms: function () {
			console.log("ope n...")
			transition.fade("in", "terms_of_use")
		}
	},
	typing: {
		first_name: function (event) {
			var key = util.getKey(event),
				input = document.getElementById("first_name_input"),
				label = document.getElementById("first_name_label")
			if (key === 13) {
				document.getElementById("last_name_input").focus()
			} else {
				input.classList.remove("error")
				transition.fade("first_name_message", "out")
				label.classList.remove("valid")
			}
		},
		last_name: function (event) {
			var key = util.getKey(event),
				input = document.getElementById("last_name_input"),
				label = document.getElementById("last_name_label")
			if (key === 13) {
				validate.name.post_first(true)
			} else {
				input.classList.remove("error")
				transition.fade("last_name_message", "out")
				label.classList.remove("valid")
			}
		},
		number: function (event) {
			var key = util.getKey(event),
				input = document.getElementById("number_input"),
				val = input.value
			if (val.length) {
				if (val.charAt(0) == "1") {
					input.maxLength = "18"
				} else {
					input.maxLength = "16"
				}
			}
			if (key === 13) {
				validate.number.post(true)
			} else {
				input.value = util.formatPhone(val)
				transition.fade("number_message", "out")
				input.classList.remove("error")
				document.getElementById("number_label").classList.remove("valid")
			}
		},
		confirm_number: function (event) {
			var key = util.getKey(event),
				label = document.getElementById("confirm_number_label"),
				input = document.getElementById("confirm_number_input"),
				val = input.value
			if (val.length) {
				if (val.charAt(0) == "1") {
					input.maxLength = "18"
				} else {
					input.maxLength = "16"
				}
			}
			if (key === 13) {
				validate.confirm_number.post(true)
			} else {
				input.value = util.formatPhone(val)
				transition.fade("confirm_number_message", "out")
				input.classList.remove("error")
				label.classList.remove("valid")
			}
		},
		code: function (event) {
			util.events.stop(event)
			var key = util.getKey(event),
				input = document.getElementById("code_input"),
				val = input.value
			if (key === 13) {
				validate.code.post(true)
			} else {
				input.classList.remove("error")
				transition.fade("code_message", "out")
			}
		}
	}
},
navigation = {
	states: {
		current: -1,
		previous: []
	},
	push: function (pg) {
		var ln = navigation.states.previous.length - 1,
			ls = navigation.states.previous[ln]
		navigation.states.current += 1
		if (ls !== pg) {//doesnt matter where we are in the chain, just dont put two in a row
			console.log("PUSH_STATE: " + pg)
			navigation.states.previous.push(pg)
			window.history.pushState({"page": pg, "id": navigation.states.current}, "", "")
		}
	},
	pop: function (event) {
		var pg = event.state.page,
			id = event.state.id,
			cur = navigation.states.current,
			cur_pg = navigation.states.previous[cur]
		console.log("POP")
		console.log("pg: " + pg + ", id: " + id + ", cur: " + cur)
		console.dir(navigation.states.previous)
		navigation.states.current = id
		///////////////////////////////////////
		if (cur > id) {
			console.log("Moving Backwards")
			if (pg == navigation.states.previous[id]) {
				switch(pg) {
					case "name_card":
						switch(cur_pg) {
							case "number_card":
								transition.card("number_card", "right_out")
								setTimeout(function () {
									transition.card("name_card", "right_in")
								}, 1E2)
							break
						}
					break
					case "number_card":
						switch(cur_pg) {
							case "confirm_number_card":
								transition.card("confirm_number_card", "right_out")
								setTimeout(function () {
									transition.card("number_card", "right_in")
								}, 1E2)
							break
							case "thank_you":
								transition.fade("main", "out")
								setTimeout(function () {
									document.getElementById("title").innerHTML = "Your Quote is Ready"
									util.addStyle("thank_you", "display", "none")
									util.addStyle("fields", "display", "block")
								}, 6E2)
								setTimeout(function () {
									transition.fade("main", "in")
								}, 7E2)
							break
						}
					break
					case "confirm_number_card":
						switch(cur_pg) {
							case "code_card":
								transition.card("code_card", "right_out")
								setTimeout(function () {
									transition.card("confirm_number_card", "right_in")
								}, 1E2)
							break
						}
					break
					case "code_card":
						switch(cur_pg) {
							case "thank_you":
								transition.fade("main", "out")
								setTimeout(function () {
									document.getElementById("title").innerHTML = "Your Quote is Ready"
									util.addStyle("thank_you", "display", "none")
									util.addStyle("fields", "display", "block")
								}, 6E2)
								setTimeout(function () {
									transition.fade("main", "in")
								}, 7E2)
							break
						}
					break
					case "thank_you":
						enter(pg)
					break
				}
			} else {
				console.error("Unknown state position")
			}
		} else {
			console.log("moving Forwards")
			if (pg == navigation.states.previous[id]) {
				enter(pg)
			} else {
				console.error("Unknown state position")
			}
		}
	}
},
transition = {
	accordion: function (dir, cb, args) {
		console.log("accordion transition")
		var parent = document.getElementById("edit_details_content"),
			child = document.getElementById("edit_details_fields"),
			btn = document.getElementById("edit_details_toggle"),
			size
		util.addStyle(btn, "pointerEvents", "none")
		if (btn.classList.contains("active")) {
			btn.classList.remove("active")
			util.addStyle(parent, "height", "0px")
			util.addStyle("dropdowns", "height", "0px")
			setTimeout(function (p, b) {
				p.classList.add("will_exit")
				util.addStyle(b, "pointerEvents", "all")
			}, 6E2, parent, btn)
		} else {
			btn.classList.add("active")
			size = util.getSize(child)
			util.addStyle(parent, "height", size.height + "px")
			interact.add_listeners("edit_details")
			setTimeout(function (p, b) {
				p.classList.remove("will_exit")
				util.addStyle(b, "pointerEvents", "all")
			}, 6E2, parent, btn)
		}
		util.callback(cb, args, 6E2)
	},
	card: function (trg, dir, cb, args) {
		var size, speed, wait = 7E2
		trg = util.getElem(trg)
		if (util.isArray(dir)) {
			speed = dir[1]
			dir = dir[0]
		} else {
			speed = false
		}
		console.log("Transitioning card elem: " + trg.id + ", " + dir)
		if (speed == "instant") {
			console.log("Switching to *instant* transition.")
			wait = 0
			util.addStyle("main_flow", "transitionDuration", "0s", "all")
			util.addStyle(trg.elem, "transitionDuration", "0s", "all")
		} else if (speed !== false) {
			wait = speed
			util.addStyle("main_flow", "transitionDuration", speed + "s", "all")
			util.addStyle(trg.elem, "transitionDuration", speed + "s", "all")
		}
		size = util.getSize(trg.elem)
		util.addStyle("main_flow", "height", size.height + "px")
		switch(dir) {
			case "right_in":
			case "right_out":
				if (speed == "instant") {
					if (dir == "right_in") {
						util.addStyle(trg.elem, "left", "0")
					} else {
						util.addStyle(trg.elem, "left", "100%")
					}
					setTimeout(function () {
						util.addStyle(elem, "transitionDuration", "", "all")
						util.addStyle("main_flow", "transitionDuration", "", "all")
					}, 10, trg.elem)
				} else {
					util.addStyle(trg.elem, "Transform", "translateX(" + size.width + "px)", "all")
					setTimeout(function (elem) {
						util.addStyle(elem, "transitionDuration", "0s", "all")
					}, wait, trg.elem)
					setTimeout(function (elem, dir) {
						if (dir == "right_in") {
							util.addStyle(elem, "left", "0")
						} else {
							util.addStyle(elem, "left", "100%")
						}
						util.addStyle(elem, "transform", "", "all")
					}, wait + 1E2, trg.elem, dir)
					setTimeout (function (elem) {
						util.addStyle(elem, "transitionDuration", "", "all")
					}, wait + 2E2, trg.elem)
				}
			break
			case "left_in":
			case "left_out":
			default:
				if (speed == "instant") {
					if (dir == "left_out") {
						util.addStyle(trg.elem, "left", "-100%")
					} else {
						util.addStyle(trg.elem, "left", "0")
					}
					setTimeout(function (elem) {
						util.addStyle(elem, "transitionDuration", "", "all")
						util.addStyle("main_flow", "transitionDuration", "", "all")
					}, 10, trg.elem)
				} else {
					util.addStyle(trg.elem, "transform", "translateX(-" + size.width + "px)", "all")
					setTimeout(function (elem) {
						util.addStyle(elem, "transitionDuration", "0s", "all")
					}, wait, trg.elem)
					setTimeout(function (elem, dir) {
						if (dir == "left_out") {
							util.addStyle(elem, "left", "-100%")
						} else {
							util.addStyle(elem, "left", "0")
						}
						util.addStyle(elem, "transform", "", "all")
					}, wait + 1E2, trg.elem, dir)
					setTimeout (function (elem) {
						util.addStyle(elem, "transitionDuration", "", "all")
					}, wait + 2E2, trg.elem)
				}
			break
		}
		util.callback(cb, args, wait + 3E2)
	},
	expand: function (trg, dir, sib, cb, args) {
		var base_id, btn, children, child, fade = "in", height = 0, i,
		scrollTop = document.body.scrollTop,
		sib, size

		trg = util.getElem(trg)
		console.log("Expanding: " + trg.id)
		base_id = trg.id.replace("_dropdown", "")
		if (typeof sib !== "undefined") {
			sib = util.getElem(sib)
		} else {
			sib = util.getElem(base_id + "_input")
		}
		size = util.getSize("header", "height") + util.getSize("main", "height") + util.getSize("footer", "height")
		util.addStyle("dropdowns", "height", size + "px")
		size = util.getSize(sib.elem)
		btn = util.getElem(base_id + "_btn")
		switch(dir) {
			case "close":
				fade = "out"
			break
			case "open":
			default:
				children = trg.elem.childNodes
				for (i = 0; i < children.length; i += 1) {
					child = children[i]
					if (util.isElem(child)) {
						height += util.getSize(child, "height")
					}
				}
				height += "px"
			break
		}
		util.addStyle(trg.elem, "transitionDuration", "0s", "all")
		setTimeout(function (elem, size) {
			util.addStyle(trg.elem, ["left", "top", "width"], [size.left + "px", (scrollTop + size.top + size.height + 5) + "px", size.width + "px"])
		}, 10, trg.elem, size)
		setTimeout(function (elem) {
			util.addStyle("transitionDuration", "", "all")
		}, 50, trg.elem)
		setTimeout(function (elem, height, fade) {
			transition.fade(elem, fade)
			util.addStyle(elem, "height", height)
			transition.fade("dropdowns", fade)
		}, 100, trg.elem, height, fade)
		//////////////////////////////
		util.callback(cb, args, 6E2)
	},
	fade: function (trg, dir, cb, args) {
		trg = util.getElem(trg)
		switch(dir) {
			case "in":
				util.addStyle(trg.elem, ["opacity", "pointerEvents"], [1, "all"])
			break
			case "out":
				util.addStyle(trg.elem, ["opacity", "pointerEvents"], [0, "none"])
			break
		}
	},
	month: function (trg, dir, cb, args) {
		var size, month_size, header_size, speed, wait = 7E2
		trg = util.getElem(trg)
		if (util.isArray(dir)) {
			speed = dir[1]
			dir = dir[0]
		} else {
			speed = false
		}
		console.log("Transitioning month elem: " + trg.id + ", " + dir)
		month_size = util.getSize(trg.elem)
		size = month_size.height
		util.addStyle("move_date_body", "height", size + "px")
		header_size = util.getSize("move_date_header")
		size += header_size
		util.addStyle("move_date_dropdown", "height", size + "px")
		if (speed == "instant") {
			console.log("Switching to *instant* transition.")
			util.addStyle(trg.elem, "transitionDuration", "0s", "all")
			wait = 0
		} else if (speed !== false) {
			wait = speed
			util.addStyle(trg.elem, "transitionDuration", speed + "s", "all")
		}
		switch(dir) {
			case "right_in":
			case "right_out":
				if (speed == "instant") {
					if (dir == "right_in") {
						util.addStyle(trg.elem, "left", "0")
					} else {
						util.addStyle(trg.elem, "left", "100%")
					}
					setTimeout(function (elem) {
						util.addStyle(elem, "transitionDuration", "", "all")
					}, 10, trg.elem)
				} else {
					size = month_size.width + 20
					util.addStyle(trg.elem, "Transform", "translateX(" + size + "px)", "all")
					setTimeout(function (elem) {
						util.addStyle(elem, "transitionDuration", "0s", "all")
					}, wait, trg.elem)
					setTimeout(function (elem, dir) {
						if (dir == "right_in") {
							util.addStyle(elem, "left", "0")
						} else {
							util.addStyle(elem, "left", "100%")
						}
						util.addStyle(elem, "transform", "", "all")
					}, wait + 1E2, trg.elem, dir)
					setTimeout (function (elem) {
						util.addStyle(elem, "transitionDuration", "", "all")
					}, wait + 2E2, trg.elem)
				}
			break
			case "left_in":
			case "left_out":
			default:
				if (speed == "instant") {
					if (dir == "left_out") {
						util.addStyle(trg.elem, "left", "-100%")
					} else {
						util.addStyle(trg.elem, "left", "0")
					}
					setTimeout(function (elem) {
						util.addStyle(elem, "transitionDuration", "", "all")
					}, 10, trg.elem)
				} else {
					size = month_size.width + 20
					util.addStyle(trg.elem, "transform", "translateX(-" + size + "px)", "all")
					setTimeout(function (elem) {
						util.addStyle(elem, "transitionDuration", "0s", "all")
					}, wait, trg.elem)
					setTimeout(function (elem, dir) {
						if (dir == "left_out") {
							util.addStyle(elem, "left", "-100%")
						} else {
							util.addStyle(elem, "left", "0")
						}
						util.addStyle(elem, "transform", "", "all")
					}, wait + 1E2, trg.elem, dir)
					setTimeout (function (elem) {
						util.addStyle(elem, "transitionDuration", "", "all")
					}, wait + 2E2, trg.elem)
				}
			break
		}
		util.callback(cb, args, wait + 3E2)
	}
},
util = {
	addStyle: function (elem, prop, val, vendors) {
		var i, ii, property, value, vendor
		if (!util.isElem(elem)) {
			elem = document.getElementById(elem)
		}
		if (util.isElem(elem)) {
			if (!util.isArray(prop)) {
				prop = [prop]
				val = [val]
			}
			for (i = 0; i < prop.length; i += 1) {
				var thisProp = String(prop[i]),
					thisVal = String(val[i])
				if (typeof vendors !== "undefined") {
					thisProp = thisProp.charAt(0).toUpperCase() + thisProp.slice(1)
					if (!util.isArray(vendors)) {
						vendors.toLowerCase() == "all" ? vendors = ["webkit", "moz", "ms", "o"] : vendors = [vendors]
					}
					for (ii = 0; ii < vendors.length; ii += 1) {
						vendor = vendors[ii] + thisProp
						elem.style[vendors[ii] + thisProp] = thisVal
					}
				}
				thisProp = thisProp.charAt(0).toLowerCase() + thisProp.slice(1)
				elem.style[thisProp] = thisVal
			}
		}
	},
	callback: function (func, args, wait) {
		if (typeof func !== "undefined") {
			if (typeof wait !== "undefined") {
				setTimeout(function (func, args) {
					typeof args !== "undefined" ? func(args) : func()
					return true
				}, wait, func, args)
				return false
			} else {
				typeof args !== "undefined" ? func(args) : func()
			}
		}
	},
	events: {
		cancel: function (event) {
			util.events.prevent(event)
			util.events.stop(event)
		},
		prevent: function (event) {
			if (event && event !== "undefined" && typeof event !== "undefined") {
				event = event || e.w.event
				event.preventDefault()
			}
		},
		stop: function (event) {
			if (event && event !== "undefined" && typeof event !== "undefined") {
				event = event || e.w.event
				event.stopPropagation()
			}
		}
	},
	formatPhone: function (val) {
		var n = val.match(/[0-9]/g)
		if (n) {
			if (val.charAt(0) == "1") {
				if (n.length < 2) {
					val = n[0]
				} else if (n.length < 5) {
					val = n[0] + " (" + n[1] + ((n[2])?n[2]:"") + ((n[3])?n[3]:"")
				} else if (n.length < 8) {
					val = n[0] + " (" + n[1] + n[2] + n[3] + ") " + n[4] + ((n[5])?n[5]:"") + ((n[6])?n[6]:"")
				} else {
					val = n[0] + "(" + n[1] + n[2] + n[3] + ") " + n[4] + n[5] + n[6] + " - " + n[7] +  ((n[8])?n[8]:"") + ((n[9])?n[9]:"") + ((n[10])?n[10]:"")
				}
			} else {
				if (n.length < 4) {
					val = "(" + n[0] + ((n[1])?n[1]:"") + ((n[2])?n[2]:"")
				} else if(n.length < 7) {
					val = "(" + n[0] + n[1] + n[2] + ") " + n[3] + ((n[4])?n[4]:"") + ((n[5])?n[5]:"")
				} else {
					val = "(" + n[0] + n[1] + n[2] + ") " + n[3] + n[4] + n[5] + " - " + n[6] + ((n[7])?n[7]:"") + ((n[8])?n[8]:"") + ((n[9])?n[9]:"")
				}
			}
		} else {
			val = ""
		}
		return val
	},
	getElem: function (trg) {
		var elem, id
		util.isElem(trg) ? (elem = trg, id = trg.id) : (elem = document.getElementById(trg), id = trg)
		return {"elem": elem, "id": id}
	},
	getKey: function (event) {
		return event.keyCode ? event.keyCode : event.which;
	},
	getSize: function (elem, prop) {
		if (!util.isElem(elem)) {
			elem = document.getElementById(elem)
		}
		var size
		typeof prop !== "undefined" ? size = parseInt(elem.getBoundingClientRect()[prop], 10) : size = elem.getBoundingClientRect()
		return size
	},
	getScreenSize: function (prop) {
		var body = document.getElementsByTagName("body")[0],
			height = window.innerHeight || document.documentElement.clientHeight || body.clientHeight,
			width = window.innerWidth || document.documentElement.clientWidth || body.clientWidth
		if (typeof prop !== "undefined") {
			switch(prop) {
				case "height":
					return height
				break
				case "width":
					return width
				break
			}
		} else {
			return {"height": height, "width": width}
		}
	},
	getTrg: function (event) {
		event = event || e.w.event
		if (event.srcElement) {
			return event.srcElement
		} else {
			return event.target
		}
	},
	inArray: function (arr, key) {
		return (arr.indexOf(key) > -1) ? true : false
	},
	inputPos: function (trg) {
		var screen_height = util.getScreenSize("height"),
			trg_size,
			scroll_top = document.documentElement.scrollTop || document.body.scrollTop || 0

		trg = util.getElem(trg)
		trg_size = util.getSize(trg.elem)
		if (trg_size.top > screen_height / 2) {
			util.scrollTo(scroll_top + (screen_height / 2), 200)
		}
	},
	isArray: function(v) {
		return (v.constructor === Array)
	},
	isElem: function (elem) {
		return (util.isNode(elem) && elem.nodeType == 1)
	},
	isNode: function(elem) {
		return (typeof Node === "object" ? elem instanceof Node : elem && typeof elem === "object" && typeof elem.nodeType === "number" && typeof elem.nodeName==="string" && elem.nodeType !== 3)
	},
	load_css: function () {
		var styles = document.getElementById("deferred-styles"),
			replacement = document.createElement("div")
		replacement.innerHTML = styles.textContent
		document.body.appendChild(replacement)
		styles.parentElement.removeChild(styles)
	},
	resolveTrue: function (v) {
		if (v !== true && v !== false) {
			v = true
		}
		return v
	},
	scrollTo: function(to, dur) {
		if (dur <= 0){
			return
		}
		var scroll_body = document.documentElement || document.body,
			scroll_top = document.documentElement.scrollTop || document.body.scrollTop || 0,
			scroll_dif = to - scroll_top,
			scroll_tick = scroll_dif / dur * 10
		setTimeout(function() {
				document.documentElement.scrollTop = scroll_top + scroll_tick
				document.body.scrollTop = scroll_top + scroll_tick
			util.scrollTo(to, dur - 10)
		}, 10)
	},
	unformatPhone: function (val) {
		var i, n
		if (val.length) {
			n = val.match(/[0-9]/g)
			val = ""
			for (i = 0; i < n.length; i += 1) {
				val += n[i]
			}
			return val
		} else {
			return ""
		}
	},
	xhr: function (method, url, data, cb, args) {
		var req = new XMLHttpRequest(),
			res, i, field, formatted_data = ""
		req.onreadystatechange = function () {
			if (req.readyState === 4) {
				if (req.status === 200) {
					res = req.responseText
					if (res.charAt(0) == "{") {
						res = JSON.parse(res, true)
					}
					console.log(res)
					if (typeof cb !== "undefined") {
						cb(res, args)
					}
				} else {
					if (typeof cb !== "undefined") {
						cb(false, args)
					}
				}
			}
		}
		req.open(method, url, true)
		if (method == "POST") {
			req.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
			req.setRequestHeader("X-Requested-With", "XMLHttpRequest")
		}
		if (util.isArray(data)) {
			for (i = 0; i < data.length; i += 1) {
				for (field in data[i]) {
					if (formatted_data.length) {
						formatted_data += "&"
					}
					formatted_data += field + "=" + data[i][field]
				}
			}
		} else {
			formatted_data = data
		}
		formatted_data += "&_token=" + answers._token
		req.send(formatted_data)
	}
},
validate = {
	name: {
		post_first: function (go) {
			var first_input = document.getElementById("first_name_input"),
				last_input = document.getElementById("last_name_input")
			go = util.resolveTrue(go)
			answers.first_name = first_input.value
			answers.last_name = last_input.value
			if (answers.first_name.length) {
				util.xhr("POST", "/validate/firstname", [{
					"first_name": answers.first_name
				}], validate.name.get_first, go)
			} else {
				if (answers.last_name.length) {
					validate.name.failed("first", go)
				} else {
					validate.name.failed("both", go)
				}
			}
		},
		get_first: function (res, go) {
			if (res !== "true") {
				validate.name.failed("first", go)
				if (go) {
					validate.name.post_last(false)
				}
			} else {
				validate.name.success("first")
				if (go) {
					validate.name.post_last(go)
				}
			}
		},
		post_last: function (go) {
			var first_input = document.getElementById("first_name_input"),
				last_input = document.getElementById("last_name_input")
			go = util.resolveTrue(go)
			answers.first_name = first_input.value
			answers.last_name = last_input.value
			if (answers.last_name.length) {
				util.xhr("POST", "/validate/lastname", [{
					"last_name": answers.last_name
				}], validate.name.get_last, go)
			} else {
				if (answers.first_name.length) {
					validate.name.failed("last", go)
				} else {
					validate.name.failed("both", go)
				}
			}
		},
		get_last: function (res, go) {
			if (res !== "true") {
				go = false
				validate.name.failed("last", go)
			} else {
				validate.name.success("last", go)
			}
		},
		success: function (which, go) {
			var first_input = document.getElementById("first_name_input"),
				first_label = document.getElementById("first_name_label"),
				last_input = document.getElementById("last_name_input"),
				last_label = document.getElementById("last_name_label")
			switch(which) {
				case "first":
					first_label.classList.add("valid")
					first_label.classList.remove("error")
				break
				case "last":
					last_label.classList.add("valid")
					last_label.classList.remove("error")
				break
				case "both":
				default:
					first_label.classList.add("valid")
					last_label.classList.add("valid")
					first_label.classList.remove("error")
					last_label.classList.remove("error")
				break
			}
			if (go) {
				enter("number_card")
			}
		},
		failed: function (which, go) {
			var first_input = document.getElementById("first_name_input"),
				first_label = document.getElementById("first_name_label"),
				last_input = document.getElementById("last_name_input"),
				last_label = document.getElementById("last_name_label")
			switch(which) {
				case "first":
					first_label.classList.remove("valid")
					if (go) {
						first_input.classList.add("error")
						transition.fade("first_name_message", "in")
					}
				break
				case "last":
					last_label.classList.remove("valid")
					if (go) {
						last_input.classList.add("error")
						transition.fade("last_name_message", "in")
					}
				break
				case "both":
				default:
					first_label.classList.remove("valid")
					last_label.classList.remove("valid")
					if (go) {
						first_input.classList.add("error")
						last_input.classList.add("error")
						transition.fade("first_name_message", "in")
						transition.fade("last_name_message", "in")
					}
				break
			}
		}
	},
	number: {
		post: function (go) {
			go = util.resolveTrue(go)
			answers.phone_formatted = document.getElementById("number_input").value
			answers.phone = util.unformatPhone(answers.phone_formatted)
			if (answers.phone.length) {
				util.xhr("POST", "/validate/phone", [{
					"phone_number": answers.phone
				}], validate.number.get, go)
			} else {
				validate.number.failed(true)
			}
		},
		get: function (res, go) {
			if (res == "true") {
				validate.number.success(go)
			} else {
				validate.number.failed(go)
			}
		},
		post_targus: function () {
			util.xhr("POST", "/targus", [
				{"first_name": answers.first_name},
				{"last_name": answers.last_name},
				{"email": answers.email},
				{"phone": answers.phone},
			], validate.number.get_targus)
		},
		get_targus: function (res) {
			calculate.miles.post()
		},
		success: function (go) {
			document.getElementById("number_label").classList.add("valid")
			document.getElementById("number_input").classList.remove("error")
			if (go) {
				validate.number.post_targus()
			}
		},
		failed: function (go) {
			document.getElementById("number_label").classList.remove("valid")
			if (go) {
				document.getElementById("number_input").classList.add("error")
				transition.fade("number_message", "in")
			}
		}
	},
	confirm_number: {
		text: function (go) {
			go = util.resolveTrue(go)
			answers.confirm_type = "text"
			validate.confirm_number.post(go)
		},
		call: function (go) {
			go = util.resolveTrue(go)
			answers.confirm_type = "call"
			validate.confirm_number.post(go)
		},
		post: function (go) {//re-validate the number dont send yet
			go = util.resolveTrue(go)
			answers.phone_formatted = document.getElementById("confirm_number_input").value
			answers.phone = util.unformatPhone(answers.phone_formatted)
			if (answers.phone.length) {
				util.xhr("POST", "/validate/phone", [{
					"phone_number": answers.phone
				}], validate.confirm_number.get, go)
			} else {
				validate.confirm_number.failed()
			}
		},
		get: function (res, go) {
			if (res !== "true") {
				validate.confirm_number.failed(go)
			} else {
				validate.confirm_number.success(go)
			}
		},
		success: function (go) {
			document.getElementById("confirm_number_label").classList.add("valid")
			document.getElementById("confirm_number_input").classList.remove("error")
			transition.fade("confirm_number_message", "out")
			if (go) {
				switch(answers.confirm_type) {
					case "text":
						util.xhr("POST", "/twiliosms", [
							{"phone": answers.phone},
							{"type": "text"}
						], validate.confirm_number.get_code)
					break
					case "call":
						util.xhr("POST", "/twiliocall", [
							{"phone": answers.phone},
							{"type": "call"}
						], validate.confirm_number.get_code)
					break
				}
			}
		},
		get_code: function (res) {
			if (res == "true") {
				enter("code_card")
			} else {
				validate.confirm_number.failed()
			}
		},
		failed: function (go) {
			document.getElementById("confirm_number_label").classList.remove("valid")
			if (go) {
				document.getElementById("confirm_number_input").classList.add("error")
				transition.fade("confirm_number_message", "in")
			}
		}
	},
	code: {
		post: function (go) {
			var input = document.getElementById("code_input"),
				val = input.value
			go = util.resolveTrue(go)
			if (val.length) {
				util.xhr("POST", "/twilio/confirmverification", [
					{"phone": answers.phone},
					{"verify-code": val},
					{"type": answers.confirm_type},
					{"_token": answers._token}
				], validate.code.get, go)
			} else {
				validate.code.failed()
			}
		},
		get: function (res, go) {
			if (res == "true") {
				if (go) {
					calculate.miles.post()
				}
			} else {
				validate.code.failed()
			}
		},
		failed: function () {
			document.getElementById("code_input").classList.add("error")
			transition.fade("code_message", "in")
		}
	},
	moving_from: {
		post: function (cb) {
			var input = document.getElementById("moving_from_input")
			if (input.value.length) {
				util.xhr("POST", "/validate/fromZipcode", [
					{"zip_from": input.dataset.zip}
				], validate.moving_from.get, cb)
			}
		},
		get: function (res, cb) {
			if (res !== "true") {
				console.error("Could not push MOVING_FROM values")
			}
			util.callback(cb)
		}
	},
	move_size: {
		post: function (cb) {
			var input = document.getElementById("move_size_input")
			if (input.value.length) {
				util.xhr("POST", "/validate/rooms", [
					{"number_of_rooms": input.dataset.formatted}
				], validate.move_size.get, cb)
			}
		},
		get: function (res, cb) {
			if (res !== "true") {
				console.error("Could not push MOVE_SIZE values")
			}
			util.callback(cb)
		}
	},
	move_date: {
		post: function (cb) {
			var input = document.getElementById("move_date_input")
			if (input.dataset.formatted.length) {
				util.xhr("POST", "/validate/movedate", [
					{"move_date": input.dataset.formatted}
				], validate.move_date.get, cb)
			}
		},
		get: function (res, cb) {
			if (res !== "true") {
				console.error("Could not push MOVE_DATE values")
			}
			util.callback(cb)
		}
	},
	moving_to: {
		post: function (cb) {
			var input = document.getElementById("moving_to_input")
			if (input.value.length) {
				util.xhr("POST", "/validate/toZipcode", [
					{"zip_to": input.dataset.zip}
				], validate.moving_to.get, cb)
			}
		},
		get: function (res, cb) {
			if (res !== "true") {
				console.error("Could not push MOVING_TO values")
			}
			util.callback(cb)
		}
	}
}

window.onpopstate = navigation.pop
window.onload = function () {
	var main = document.getElementById("main"),
		token = document.getElementById("token"),
		lead = document.getElementById("lead"),
		source = document.getElementById("source"),
		i
	answers._token = token.value
	answers.lead = JSON.parse(lead.value, true)
	for (i in answers.lead) {
		answers[i] = answers.lead[i]
	}
	answers.source = source.value

	main.removeChild(token)
	main.removeChild(lead)
	main.removeChild(source)

	if (answers.lead.first_name != null && answers.lead.last_name != null && answers.lead.first_name.length && answers.lead.last_name.length) {
		enter("number_card")
	} else {
		enter("name_card")
	}
}

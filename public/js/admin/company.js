$(function() {
	$("#msform").validate({
		rules : {
			name : {
				required : true,
			},
			phone : {
				required : true,
				digits : true,
				maxlength : 10
			},
		},
		messages : {},
		submitHandler : function() {
			$.ajax({
				type : 'post',
				url : '/admin/companydetails/addCompany',
				data : $('form').serialize(),
				dataType : 'json',
				success : function(data) {
					$("#response").html(data.result);
					$('#msform')[0].reset();
					$('#response').delay(5000).fadeOut('slow');
				}
			});
		},
	});

	$("#daystime").validate({
		rules : {
		},
		messages : {},
		submitHandler : function() {
			$.ajax({
				type : 'post',
				url : '/admin/companydetails/editDaysTime',
				data : $('form').serialize(),
				dataType : 'json',
				success : function(data) {
					$("#response").html(data.result);
					$('#daystime')[0].reset();
					$('#response').delay(5000).fadeOut('slow');
				}
			});
		},
	});
	
	$(".main-table").on(
			"click",
			"td.edit_company",
			function() {
				if (!$(this).hasClass('clickedCls')) {
					$(this).html(
							"<input type='text' class='company_data' data = '"
									+ $(this).attr('data-id') + "'  value='"
									+ $(this).attr('data') + "' data-type='"
									+ $(this).attr('data-type') + "'/>");
					$(".company_data").focus();
					$(this).addClass('clickedCls');

				}
			});

	$(document.body).on(
			'blur',
			'.company_data',
			function() {
				var thisObj = $(this);
				thisObj.parent().removeClass('clickedCls');
				$("#ajax_msg").removeClass("alert-danger");
				$("#ajax_msg").addClass("alert-success");
				$("#ajax_msg").show();
				$("#ajax_msg").html("Proccessing ...");
				$.ajax({
					type : 'POST',
					url : 'companydetails/editCompany',
					dataType : 'json',
					data : {
						company_val : $(this).val(),
						company_type : $(this).attr('data-type'),
						company_id : $(this).attr('data'),
					},
					success : function(response) {
						thisObj.parent().removeClass('clickedCls');
						if (response.status == true) {
							thisObj.parent().html(thisObj.val()).attr('data',
									thisObj.val());
							$("#ajax_msg").removeClass("alert-danger")
									.addClass("alert-sucess").show().html(
											response.msg);
							setTimeout(function() {
								$("#ajax_msg").hide();
							}, 3000);
						} else {
							$("#ajax_msg").removeClass("alert-success")
									.addClass("alert-danger").show().html(
											response.msg);
							setTimeout(function() {
								$("#ajax_msg").hide();
							}, 3000);
						}

					},
					complete : function(xhr, status) {
						thisObj.parent().removeClass('clickedCls');
					}
				});
			});
	
	$('.toggle--switch').on('click', function(){
		$.ajax({
			type : 'POST',
			url : 'companydetails/editCompany',
			dataType : 'json',
			data : {
				company_val : $(this).children("#toggle--switch").is(':checked') ? '1' : '0',
				company_type : 'status',
				company_id : $(this).attr('data-id'),
			},
			success : function(response) {
			},
		});
	});
});


var table;

window.onload = function () {
    table = $('#datatable_list').dataTable({
        "bPaginate": false,
        "bFilter": false,
        "pagingType": "full_numbers",
        "iDisplayLength": 100,
        "aaSorting": [[0, 'asc']],
    });
}

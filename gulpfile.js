var gulp = require("gulp"),
    gutil = require("gulp-util"),
    sass = require("gulp-sass"),
    success = true

gulp.task("sass_desktop", function () {
    gulp.watch("public/css/quotes/desktop/*.scss", function (e) {
        setTimeout(function (path) {
            success = true
            gulp.src(path)
                .pipe(sass({
                    outputStyle: "expanded"
                }).on("error", function () {
                    gutil.log(gutil.colors.red("Sass Error"))
                    success = false
                }))
                .pipe(gulp.dest("public/css/quotes/desktop"))
                .on("end", function () {
                    if (success) {
                        gutil.log(gutil.colors.green("Compiled Sass"))
                    }
                })
        }, 1E3, e.path)
    })
})
gulp.task("sass_mobile", function () {
    gulp.watch("public/css/quotes/mobile/*.scss", function (e) {
        setTimeout(function (path) {
            success = true
            gulp.src(path)
                .pipe(sass({
                    outputStyle: "expanded"
                }).on("error", function () {
                    gutil.log(gutil.colors.red("Sass Error"))
                    success = false
                }))
                .pipe(gulp.dest("public/css/quotes/mobile"))
                .on("end", function () {
                    if (success) {
                        gutil.log(gutil.colors.green("Compiled Sass"))
                    }
                })
        }, 1E3, e.path)
    })
})

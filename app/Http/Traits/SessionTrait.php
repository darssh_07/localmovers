<?php

namespace App\Http\Traits;

use App\Models\localmovers\Session;
use App\Models\localmovers\UserAgent;
use Jenssegers\Agent\Agent;

trait SessionTrait
{
    private function createSessionId($request)
    {
        $agent = new Agent();
        $session_id = $request->session()->get('session_id');
        if (isset($session_id)
            && !empty($session_id)) {
            return $request->session()->get('session_id');
        }

        if (!$agent->isRobot() && ($request->header('User-Agent') != null || Session::ipCount($request->ip()) <= 10)) {
            $session = $request->session()->get('_token');

            $session_exists = Session::isSessionExists($session);

            if (!$session_exists) {
                $session_data['IP'] = $request->ip();
                $session_data['referer'] =  $request->header('referer');
                $session_data['source_page'] = $request->path();
                $session_data['session'] = $session;
                $session_data['created_date'] = date('Y-m-d H:i:s');

                $session_exists = Session::create($session_data);

                if ($request->header('User-Agent')) {
                    $useragent_data['session_id'] = $session_exists->id;
                    $useragent_data['useragent'] = $request->header('User-Agent');
                    $useragent_data['created_date'] = $session_data['created_date'];

                    UserAgent::create($useragent_data);
                }
            }
            $session_id = $session_exists->id;
            
            $request->session()->put('session_id', $session_id);

            return $session_id;
        } else {
            return null;
        }
    }
}

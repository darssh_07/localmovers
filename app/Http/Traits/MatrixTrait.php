<?php

namespace App\Http\Traits;

use App\Models\localmovers\Targus;
use App\Models\localmovers\TwilioSMSDetails;
use App\Models\localmovers\TwilioCallDetails;
use App\Models\localmovers\MatrixSubmission;
use App\Models\localmovers\EoMatrixSubmission;
use App\Models\localmovers\Lead;
use App\Models\localmovers\AdditionalDetails;
use App\Models\localmovers\AfterHourRoutingLeads;

trait MatrixTrait
{
    public function createXML($row)
    {
        if (!empty($row['domain'])) {
            $medium = strtoupper($row['domain']);
        } else {
            $medium = !empty($row['utm_medium']) ? strtoupper($row['utm_medium']) : 'LM';
        }

        if (!empty($row['utm_source'])) {
            if ($row['utm_source'] == 'goog' || $row['utm_source'] == 'adwords') {
                $origin = '(G)';
            } elseif ($row['utm_source'] == 'bing') {
                $origin = '(Y)';
            } elseif ($row['utm_source'] == 'bing-b') {
                $origin = '(Yn)';
            } elseif ($row['utm_source'] == 'AdRoll') {
                $origin = '(AR)';
            } elseif ($row['utm_source'] == 'gemini') {
                $origin = '(GEM)';
            } elseif ($row['utm_source'] == 'FB') {
                $origin = '(FB)';
            } elseif ($row['utm_source'] == 'Email') {
                $origin = '(Email)';
            } elseif (strtoupper($row['utm_source']) == 'TMD') {
                $origin = '(T)';
            } elseif (strtoupper($row['utm_source']) == 'FL') {
                $origin = '(FL)';
            } else {
                $origin = '';
            }
        } else {
            $origin = '';
        }

        $landing_page = isset($row['source']) ? $row['source'] : $row['landing_path'];

        if (strpos($landing_page, 'quotes') !== false || $landing_page == 'Email' || $landing_page == 'quotes') {
            $origin = '(Email)';
        }

        $origin = $medium . $origin;

        $xverify_phone = $this->targus_score($row['lead_id'], $row['phone'], $row['email']);

        $xvr_phone = $xverify_phone['phone_score'];
        $xvr_email = $xverify_phone['email_score'];

        $additional_details_row = AdditionalDetails::isADExistsForLeadID($row['lead_id']);
        $additional_details = "";

        if ($additional_details_row) {
            $additional_details = $additional_details_row->details;
        }

        $route_after_hour = AfterHourRoutingLeads::where('lead_id', $row['lead_id'])->first();
        $route_ah = "";
        
        if ($route_after_hour) {
            $route_ah = '1';
        }

        $xml = "<?xml version='1.0' standalone='yes'?>\n";
        $xml .= "<lead>\n";
        $xml .= "<affiliate>Charlie</affiliate>\n";
        $xml .= "<origin>".$origin."</origin>\n";
        $xml .= "<landing_page>".$landing_page."</landing_page>\n";
        $xml .= "<kw>".urlencode($row['kw'])."</kw>\n";
        $xml .= "<oii>".$row['oii']."</oii>\n";
        $xml .= "<qs>".urlencode($row['qs'])."</qs>\n";
        $xml .= "<ag>".htmlentities($row['ad_group'])."</ag>\n";
        $xml .= "<ai>".$row['ad_id']."</ai>\n";
        $xml .= "<network>".$row['network']."</network>\n";
        $xml .= "<search_engine>".$row['utm_source']."</search_engine>\n";
        $xml .= "<campaign>".$row['utm_campaign']."</campaign>\n";
        $xml .= "<device>".$row['device_type']."</device>\n";
        $xml .= "<name>".$row['first_name']." ".$row['last_name']."</name>\n";
        $xml .= "<email>".$row['email']."</email>\n";
        $xml .= "<from_zip>".$row['from_zip']."</from_zip>\n";
        $xml .= "<to_zip>".$row['to_zip']."</to_zip>\n";
        $xml .= "<phone>".$row['phone']."</phone>\n";
        $xml .= "<phone_ext></phone_ext>\n";
        $xml .= "<xvr_phone>".$xvr_phone."</xvr_phone>\n";
        $xml .= "<xvr_email>".$xvr_email."</xvr_email>\n";
        $xml .= "<phone2></phone2>\n";
        $xml .= "<move_size>".$row['move_size']."</move_size>\n";
        $xml .= "<IP>".$row['IP']."</IP>\n";
        $xml .= "<GCLID>".$row['gclid']."</GCLID>\n";
        $xml .= "<campid>".$row['campid']."</campid>\n";
        $xml .= "<domain>".$row['domain']."</domain>\n";
        $xml .= "<move_date>".date('Y-m-d', strtotime($row['move_date']))."</move_date>\n";
        $xml .= "<additional_details>".$additional_details."</additional_details>\n";
        $xml .= "<route_ah>".$route_ah."</route_ah>\n";
        $xml .= "</lead>\n";

        return $xml;
    }

    public function submitToMatrix($data)
    {
        dd();
        if ($_SERVER['SERVER_ADDR'] == "172.99.116.4") {
            $xml = $this->createXML($data);
            $matrix_response = $this->curl_submit($xml, $data['lead_id']);

            return $matrix_response;
        }
    }

    public function submitToNewMatrix($data)
    {
        $xml = $this->createXML($data);
        $ch = curl_init("https://matrix.leads.software/leadxmlemailonly");

        curl_setopt_array(
            $ch,
            array(
                CURLOPT_HEADER => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_CONNECTTIMEOUT => 40,
                CURLOPT_USERAGENT => 'Matrix Lead Request',
                CURLOPT_POSTFIELDS => array('xml' => $xml)
            )
        );
    
        return $this->store_response(curl_exec($ch), $data['lead_id'], true);
    }

    public function targus_score($lead_id = null, $phone = null, $email = null)
    {
        $lead_exists_targus = Targus::isLeadIDForPhoneEmail($lead_id, $phone, $email);

        $phone_score = (!empty($lead_exists_targus) && isset($lead_exists_targus->phone_score)) ? $lead_exists_targus->phone_score : '';
        $email_score = (!empty($lead_exists_targus) && isset($lead_exists_targus->email_score)) ? $lead_exists_targus->email_score : '';

        $twilio_lead = TwilioCallDetails::isToNumberExistsForLeadID($phone, $lead_id);

        if (empty($twilio_lead)) {
            $twilio_lead = TwilioSMSDetails::isToNumberExistsForLeadID($phone, $lead_id);
        }

        if (!empty($twilio_lead)) {
            $verified = $twilio_lead->verified;
            $to_number = $twilio_lead->to_number;
            if ($verified == 1) {
                $phone_score = 99;
            } else {
                if ($to_number != $phone) {
                    $phone_score = "";
                }
            }
        }

        return array(
                'phone_score' => $phone_score,
                'email_score' => $email_score
        );
    }


    public function curl_submit($xml, $lead_id)
    {
        $ch = curl_init("https://submit.bvlgw.com/submit.php");
    
        curl_setopt_array(
            $ch,
            array(
                CURLOPT_HEADER => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_CONNECTTIMEOUT => 40,
                CURLOPT_HTTPAUTH => CURLAUTH_DIGEST,
                CURLOPT_USERPWD => "fmq:e\$6ZA)D1x]:Fr8?6A^{/\$\$}?oOhp1'x!_fN=u5Hb_D0u\$)`E8=xuh\\\\zj+ofH|E",
                CURLOPT_POSTFIELDS => array('xml' => $xml)
            )
        );
    
        return $this->store_response(curl_exec($ch), $lead_id, false);
    }



    private function store_response($response, $lead_id, $email_only=false)
    {
        $date = date("Y-m-d H:i:s");
        $matrix_lead_id = '0';
        $error = '';

        if (!empty($response)) {
            $xml = simplexml_load_string(trim($response));
            if (isset($xml->lead_id)) {
                $matrix_lead_id = $xml->lead_id;
            } else {
                $error = $xml->error;
            }
            $valid = $xml->valid;
        } else {
            $valid = '0';
            $response = 'bad xml';
        }

        $data = array();
        $data['lead_id'] = $lead_id;
        $data['matrix_lead_id'] = (int) $matrix_lead_id;
        $data['is_valid'] = $valid;
        $data['error'] = (string) $error;
        $data['raw_xml'] = (string) $response;
        $data['date_time'] = $date;

        if ($email_only) {
            EoMatrixSubmission::updateOrCreate(
                ['lead_id' => $lead_id],
                $data
            );
        } else {
            MatrixSubmission::create($data);
            
            Lead::where('lead_id', $lead_id)->update([ 'matrix_lead_id' => $data['matrix_lead_id'] ]);
        }

        return array(
                'matrix_lead_id' => $matrix_lead_id,
                'time_submitted_to_matrix' => $date
        );
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Jenssegers\Agent\Agent;

use App\Models\localmovers\Lead;
use App\Models\localmovers\LeadNetwork;
use App\Http\Traits\MatrixTrait;
use App\Models\localmovers\Unsubscribe;
use App\Models\localmovers\AdditionalDetails;
use App\Models\localmovers\Matches;
use App\Models\localmovers\LeadMatches;
use App\Models\localmovers\Confirmation;
use App\Models\localmovers\CityState;

class MovingController extends Controller
{
    use MatrixTrait;

    public function index(Request $request, $url='')
    {
        $agent = new Agent();

        $session_id = $request->session()->get('session_id');
        if ($session_id != null) {
            $leadResult = Lead::isLeadExistsForSessionId($session_id);
        } else {
            $leadResult = null;
        }

        if (empty($leadResult) && !$agent->isRobot() && $session_id != null) {
            $lead_data = array();
            $lead_data['session_id'] = $session_id;
            $lead_data['IP'] = $request->ip();
            $lead_data['created_at'] =  date('Y-m-d H:i:s');
            $lead_data['device_type'] = $agent->isDesktop() ? 'Desktop' : ($agent->isMobile() && $agent->isTablet() ? 'Tablet' : 'Mobile');
            $lead_data['landing_path'] = $request->path();
            $lead_data['token'] = md5($request->session()->get('_token'));
            
            $leadResult = Lead::create($lead_data);

            $meta = array();
            $meta['utm_source'] = $request->input('utm_source');
            $meta['utm_medium'] = $request->input('utm_medium');
            $meta['utm_campaign'] = $request->input('utm_campaign');
            $meta['campaign'] = $request->input('_campaign');
            $meta['qs'] = $request->input('qs');
            $meta['ad_id'] = $request->input('adid');
            $meta['ad_group'] = $request->input('ag');
            $meta['network'] = $request->input('network');
            $meta['device_type'] = $request->input('device');
            $meta['device'] = $agent->device();
            $meta['os'] = $agent->platform();
            $meta['browser'] = $agent->browser();
            $meta['kw'] = $request->input('kw');
            $meta['oii'] = $request->input('oii');
            $meta['gclid'] = !empty($request->input('gclid')) ? $request->input('gclid') : (!empty($request->input('msclkid')) ? $request->input('msclkid') : $request->input('fbclid'));
            $meta['campid'] = $request->input('campid');
            $meta['domain'] = $request->input('domain');

            $ref = $request->header('referer');
            if (!isset($meta['utm_source']) && ($request->input('fbclid') || strpos(strtolower($ref), 'facebook') !== false)) {
                $meta['utm_source'] = 'FL';
            }
            $lead_network_exists = LeadNetwork::updateOrCreate(
                ['session_id' => $session_id],
                $meta
            );
        }

        $data['url'] = $url;
        if ($session_id != null) {
            $request->session()->put('lead_id', $leadResult->lead_id);
        }

        if ($agent->isMobile() && !$agent->isTablet()) {
            return view('moving.mobile.index', $data);
        } else {
            return view('moving.desktop.index', $data);
        }
    }

    public function curl_get_contents($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    public function send(Request $request)
    {
        if ($request->ajax()) {
            $lead_id = $request->session()->get('lead_id');
            $session_id = $request->session()->get('session_id');

            $lead_exists = Lead::find($lead_id);
            if (!$lead_exists) {
                $lead_exists = Lead::isLeadExistsForSessionId($session_id);
                if (empty($lead_exists)) {
                    return false;
                } else {
                    $lead_id = $lead_exists->lead_id;
                }
            }

            $post = $request->all();

            $agent = new Agent();

            $phone = $post['phone'];
            $phone = preg_replace("/[^0-9]/", "", $phone);
            
            $lead_data = array();
            $lead_data['first_name'] = (isset($post['first_name']) && !empty($post['first_name'])) ? $post['first_name'] : $lead_exists->first_name;
            $lead_data['last_name'] = (isset($post['last_name']) && !empty($post['last_name'])) ? $post['last_name'] : $lead_exists->last_name;
            $lead_data['phone'] = (isset($phone) && !empty($phone)) ? $phone : $lead_exists->phone;
            $lead_data['email'] = (isset($post['email']) && !empty($post['email'])) ? $post['email'] : $lead_exists->email;
            $lead_data['IP'] = (isset($post['IP']) && !empty($post['IP'])) ? $post['IP'] : $lead_exists->IP;
            $lead_data['from_zip'] = (isset($post['from_zip']) && !empty($post['from_zip'])) ? $post['from_zip'] : $lead_exists->from_zip;
            $lead_data['to_zip'] = (isset($post['to_zip']) && !empty($post['to_zip'])) ? $post['to_zip'] : $lead_exists->to_zip;
            $lead_data['move_size'] = (isset($post['move_size']) && !empty($post['move_size'])) ? $post['move_size'] : $lead_exists->move_size;
            $lead_data['move_date'] =(isset($post['move_date']) && !empty($post['move_date'])) ? date('Y-m-d', strtotime($post['move_date'])) : $lead_exists->move_date;
            $lead_data['device_type'] = $agent->isDesktop() ? 'Desktop' : ($agent->isMobile() && $agent->isTablet() ? 'Tablet' : 'Mobile');
            $lead_data['landing_path'] = isset($post['source']) ? $post['source'] : $lead_exists->landing_path;

            $lead_data['from_zip'] = sprintf('%05d', $lead_data['from_zip']);
            $lead_data['to_zip'] = sprintf('%05d', $lead_data['to_zip']);

            $data['user_agent'] = $request->header('User-Agent');
            $data['type'] = 'moving';
            $data['account_id'] = '1000';
           
            $session_id = isset($session_id) ? $session_id : $lead_exists->session_id;
            $lead_network_exists = LeadNetwork::find($session_id);

            //Google Adwords variables
            $lead_network_data['utm_source'] = (isset($post['utm_source']) && !empty($post['utm_source']))
                                                    ? $post['utm_source'] : $lead_network_exists->utm_source;
            $lead_network_data['utm_medium'] = (isset($post['utm_medium']) && !empty($post['utm_medium']))
                                                    ? $post['utm_medium'] : $lead_network_exists->utm_medium;
            $lead_network_data['utm_campaign'] = (isset($post['utm_campaign']) && !empty($post['utm_campaign']))
                                                    ? $post['utm_campaign'] : $lead_network_exists->utm_campaign;
            $lead_network_data['campaign'] = (isset($post['campaign']) && !empty($post['campaign']))
                                                            ? $post['campaign'] : $lead_network_exists->campaign;
            $lead_network_data['ad_group'] = (isset($post['ag']) && !empty($post['ag'])) ? $post['ag'] : $lead_network_exists->ad_group;
            $lead_network_data['qs'] = (isset($post['qs']) && !empty($post['qs'])) ? $post['qs'] : $lead_network_exists->qs;
            $lead_network_data['network'] = (isset($post['network']) && !empty($post['network'])) ? $post['network'] : $lead_network_exists->network;
            $lead_network_data['device_type'] = (isset($post['device']) && !empty($post['device'])) ? $post['device'] : $lead_network_exists->device_type;
            $lead_network_data['ad_id'] = (isset($post['adid']) && !empty($post['adid'])) ? $post['adid'] : $lead_network_exists->ad_id;
            $lead_network_data['device'] = $agent->device();
            $lead_network_data['os'] = $agent->platform();
            $lead_network_data['browser'] = $agent->browser();
            $lead_network_data['kw'] = (isset($post['kw']) && !empty($post['kw'])) ? $post['kw'] : $lead_network_exists->kw;
            $lead_network_data['oii'] = (isset($post['oii']) && !empty($post['oii'])) ? $post['oii'] : $lead_network_exists->oii;
            $lead_network_data['gclid'] = (isset($post['gclid']) && !empty($post['gclid'])) ? $post['gclid'] : $lead_network_exists->gclid;
            $lead_network_data['campid'] = (isset($post['campid']) && !empty($post['campid'])) ? $post['campid'] : $lead_network_exists->campid;
            $lead_network_data['domain'] = (isset($post['domain']) && !empty($post['domain'])) ? $post['domain'] : $lead_network_exists->domain;

            $leadResult = Lead::updateOrCreate(
                ['lead_id' => $lead_id, 'session_id' => $session_id],
                $lead_data
            );

            $lead_data['session_id'] = $leadResult->session_id;
            $lead_data['lead_id'] = $leadResult->lead_id;
            $lead_data['source'] = $lead_data['landing_path'];
            $matrix_data = array_merge($lead_data, $lead_network_data);

            $lead_network_exists = LeadNetwork::updateOrCreate(
                ['session_id' => $session_id],
                $lead_network_data
            );
            $lead_network_data['session_id'] = $lead_network_exists->session_id;

            if ($phone > 0 && !filter_var($lead_data['email'], FILTER_VALIDATE_EMAIL) === false) {
                $response = $this->submitToMatrix($matrix_data);

                Lead::updateOrCreate(
                    ['lead_id' => $lead_id, 'session_id' => $session_id],
                    ['matrix_lead_id' => $response['matrix_lead_id']]
                );
            }

            if ($lead_data['lead_id']) {
                return 'true';
            } else {
                return 'false';
            }
        } else {
            return 'false';
        }
    }


    public function send_lead_manually()
    {
        $to_date = date('Y-m-d H:i:s', strtotime('-2 minute'));
        $from_date = date('Y-m-d H:i:s', strtotime('-24 hour'));

        $results = Lead::LeadsWithNullMatrixLeadIdBWTime($from_date, $to_date);

        foreach ($results as $lead) {
            $lead_network_exists = LeadNetwork::find($lead->session_id);
            
            unset($lead_network_exists->session_id);
            
            $matrix_data = array_merge($lead->toArray(), $lead_network_exists->toArray());
            
            $matrix_data['from_zip'] = sprintf('%05d', $matrix_data['from_zip']);
            $matrix_data['to_zip'] = sprintf('%05d', $matrix_data['to_zip']);
            
            $response = $this->submitToMatrix($matrix_data);

            Lead::updateOrCreate(
                ['lead_id' => $lead->lead_id, 'session_id' => $lead->session_id],
                ['matrix_lead_id' => $response['matrix_lead_id']]
            );
        }
    }

    public function send_email_only_lead_to_nm()
    {
        $to_date = date('Y-m-d H:i:s', strtotime('-2 minute'));
        $from_date = date('Y-m-d H:i:s', strtotime('-24 hour'));

        $results = Lead::LeadsWithOutPhoneBWTimeNoNewMat($from_date, $to_date);
    
        foreach ($results as $lead) {
            $lead_network_exists = LeadNetwork::find($lead->session_id);
            
            unset($lead_network_exists->session_id);
            
            $matrix_data = array_merge((array) $lead, $lead_network_exists->toArray());
            $matrix_data['from_zip'] = sprintf('%05d', $matrix_data['from_zip']);
            $matrix_data['to_zip'] = sprintf('%05d', $matrix_data['to_zip']);

            $response = $this->submitToNewMatrix($matrix_data);
        }
    }

    public function unsubscribe(Request $request)
    {
        if ($email = $request->input('email')) {
            Unsubscribe::updateOrCreate(
                ['email' => $email],
                ['is_unsubscribed' => '1']
            );
        }
        return view('admin.unsubscribe');
    }

    public function saveAdditionalDetails(Request $request)
    {
        $result = 'false';
        $post = $request->all();

        if (!empty($post)
            && isset($post['details'])) {
            $lead_id = $request->session()->get('lead_id');
            $session_id = $request->session()->get('session_id');

            $lead_exists = Lead::find($lead_id);
            if (!$lead_exists) {
                $lead_exists = Lead::isLeadExistsForSessionId($session_id);
                if (empty($lead_exists)) {
                    return false;
                } else {
                    $lead_id = $lead_exists->lead_id;
                }
            }

            $details = $post['details'];

            if ($lead_id) {
                AdditionalDetails::updateOrCreate(
                    ['lead_id' => $lead_id],
                    ['details' => $details]
                );
            }
            $result = 'true';
        }
        return $result;
    }

    public function getLeadMatches(Request $request)
    {
        $response = array();

        $session_id = $request->session()->get('session_id');
        if ($session_id) {
            $row = DB::select(
                "SELECT
                        l.IP,
                        l.from_zip,
                        l.to_zip,
                        l.move_size,
                        l.move_date,
                        l.session_id as lead_id,
                        ln.utm_campaign as campaign,
                        ln.qs as query_string,
                        csn1.city_name as from_city,
                        csn1.state as from_state,
                        csn2.city_name as to_city,
                        csn2.state as to_state
                FROM leads AS l 
                    LEFT JOIN lead_network AS ln 
                ON l.session_id = ln.session_id 
                    LEFT JOIN  city_state AS csn1 
                ON l.from_zip = csn1.zipcode  
                    LEFT JOIN  city_state AS csn2 
                ON l.to_zip = csn2.zipcode  
                WHERE 
                    l.session_id = :session_id 
                limit 1;",
                ['session_id' => $session_id]
            );

            if (is_array($row)) {
                $row = $row[0];
            }
            $arr = (array) $row;
            $arr['origin'] = 'LM';

            $url = "http://admin:EQMmat200@matrix.bvlgw.com/matches/get_matches1.php";
            $data = http_build_query($arr);
            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $result = curl_exec($ch);
            curl_close($ch);

            $companies = (array) json_decode(trim($result));

            $response = array();
            if (count($companies) > 0) {
                LeadMatches::where('session_id', $session_id)->delete();

                foreach ($companies as $match => $phone) {
                    $match_id = Matches::updateOrCreate(['match_name' => $match], ['phone' => $phone])->match_id;

                    LeadMatches::updateOrCreate(
                        ['session_id' => $session_id, 'match_id' => $match_id],
                        []
                    );

                    $response[$match] = array(
                        'phone' => $phone,
                        'match_id' => $match_id
                    );
                }
            }
        }

        return $response;
    }


    public function getRandomNumber()
    {
        $unique_key1 = (substr(md5(rand(0, 1000000)), 0, 4));
        $unique_key2 = (substr(md5(rand(0, 1000000)), 0, 4));
        $str =  $unique_key1 .' '. $unique_key2;
        return strtoupper($str);
    }


    public function confirmation(Request $request)
    {
        $session_id = $request->session()->get('session_id');

        $conf = Confirmation::isSessionIdExists($session_id);

        if (!$conf) {
            $confirmation_id = $this->getRandomNumber();
            Confirmation::create([
                'session_id' => $session_id,
                'confirmation_id' => $confirmation_id,
                'created_date' => date('Y-m-d H:i:s')
            ]);
            $result = $confirmation_id;
        } else {
            $result =  $conf->confirmation_id;
        }

        return json_encode(array('result' => $result));
    }
}

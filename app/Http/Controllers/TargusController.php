<?php

namespace App\Http\Controllers;

use SoapClient;
use Illuminate\Http\Request;
use App\Models\localmovers\Targus;
use App\Models\localmovers\Lead;

class TargusController extends Controller
{
    public function doTargusNuSOAPCall(Request $request)
    {
        $post = $request->all();
        $session_id = $request->session()->get('session_id');
        $lead_id = $request->session()->get('lead_id');
        if ($lead_id == null) {
            $leadResult = Lead::isLeadExistsForSessionId($session_id);
            if ($leadResult) {
                $lead_id = $leadResult->lead_id;
            }
        }

        $data = array();
        if (!empty($post)) {
            $phone = $post['phone'];
            $phone = preg_replace("/[^0-9]/", "", $phone);

            $data['first_name'] = $post['first_name'];
            $data['last_name'] = $post['last_name'];
            $data['phone'] = $phone;
            $data['email'] = $post['email'];


            $row = Targus::isLeadIDForPhoneEmail($lead_id, $data['phone'], $data['email']);

            if (!empty($row)) {
                if ($row->phone_score < 35 ||
                    in_array($row->phone_score, [37, 42, 47, 52, 57, 62])) {
                    $result = 'false';
                } else {
                    $result = 'true';
                }
            } else {
                $wsdlURL = "http://webapp.targusinfo.com/ws-getdata/query.asmx?WSDL";
                $client = new SoapClient($wsdlURL);
                if ($data['phone'] != '' && isset($data['phone'])) {
                    $soap_body = $this->getTargusSoapBody($data);
                    $targ_response = $client->query($soap_body);
                    if (isset($targ_response) && isset($targ_response->response) && isset($targ_response->response->result->element->value)) {
                        $value = $targ_response->response->result->element->value;
                        $value_xml = simplexml_load_string($value);
                        $phone_score = ((string) $value_xml[0]->Phones->Phone["score"]);
                        $email_score = ((string) $value_xml[0]->eMailAddresses->eMail["score"]);
                    } else {
                        $phone_score = '50';
                        $email_score = '50';
                    }

                    $data['lead_id'] = $lead_id;
                    $data['phone'] = $data['phone'];
                    $data['phone_score'] = $phone_score;
                    $data['email'] = $data['email'];
                    $data['email_score'] = $email_score;
                    $data['date_time'] = date("Y-m-d H:i:s");

                    // Insert Data in Targus Table
                    Targus::create($data);

                    if ($phone_score < 35 ||
                            $phone_score == 37 ||
                            $phone_score == 42 ||
                            $phone_score == 47 ||
                            $phone_score == 52 ||
                            $phone_score == 57 ||
                            $phone_score == 62) {
                        $result = 'false';
                    } else {
                        $result = 'true';
                    }
                } else {
                    $result = 'true';
                }
            }
        } else {
            $result = 'true';
        }

        return $result;
    }


    private function getTargusSoapBody(array $data)
    {
        $namespace = "http://TARGUSinfo.com/WS-GetData";
        $myUN = 'EQMedia';
        $myPW = '^yxI690m';
        $mySI = '9202501407';
        $ip = '173.344.45.454';
        $fone = '<Contact><Names><Name type="C"><First>' . $data['first_name'] . '</First><Middle></Middle><Last>' . $data['last_name'] . '</Last></Name></Names><Addresses></Addresses><Phones><Phone score="1" appends="validation,mobile,tz,dst,st,daconnected,BPI,active">' . $data['phone'] . '</Phone></Phones><eMailAddresses><eMail score="1" appends="validation,reason,repository">' . $data['email'] . '</eMail></eMailAddresses><IPAddresses><IPAddress appends="validation,country,ST">' . $ip . '</IPAddress></IPAddresses></Contact>'; // phone to lookup
        $elemId = '3226';
        $transId = rand(1, 100000000);

        return array(
            'xmlns' => $namespace,
            'origination' => array(
                'username' => $myUN,
                'password' => $myPW
            ),
            'serviceId' => $mySI,
            'transId' => $transId,
            'elements' => array(
                'id' => $elemId
            ),
            'serviceKeys' => array(
                'id' => $elemId,
                'serviceKey' => array(
                    'id' => '875',
                    'value' => $fone
                )
            )
        );
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Admin\TextMatch;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TextMatchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view(
            'admin.textmatch.index',
            [
                'textmatches' => [
                    'exact' => [
                        'name' => 'Exact Match',
                        'data' => TextMatch::fetchByType('exact'),
                    ],
                    'name_set' => [
                        'name' => 'Name Set Match',
                        'data' => TextMatch::fetchByType('name-set'),
                    ],
                    'full_email' => [
                        'name' => 'Full Email Match',
                        'data' => TextMatch::fetchByType('full-email'),
                    ],
                    'address_email' => [
                        'name' => 'Address Email Match',
                        'data' => TextMatch::fetchByType('address-email'),
                    ],
                    'domain_email' => [
                        'name' => 'Domain Email Match',
                        'data' => TextMatch::fetchByType('domain-email'),
                    ],
            ]]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.textmatch.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => [
                'required',
                Rule::in(['exact', 'name-set', 'full-email', 'address-email', 'domain-email'])
            ],
            'text' => 'required',
        ]);

        TextMatch::create([
            'type' => $request->input('type'),
            'text' => $request->input('text'),
            'date_time' => date('Y-m-d H:i:s')
        ]);

        return redirect('admin/textmatch');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\TextMatch  $textMatch
     * @return \Illuminate\Http\Response
     */
    public function edit(TextMatch $textmatch)
    {
        return view('admin.textmatch.edit', compact('textmatch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\TextMatch  $textMatch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TextMatch $textmatch)
    {
        $this->validate($request, [
            'text' => 'required'
        ]);

        $textmatch->text = $request->input('text');
        $textmatch->date_time = date('Y-m-d H:i:s');
        $textmatch->save();

        return redirect('admin/textmatch');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\TextMatch  $textMatch
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, TextMatch $textmatch)
    {
        if ($request->input('del', 'No') == 'Yes') {
            $textmatch->delete();
        }
        
        return redirect('admin/textmatch');
    }


    public function deleteConfirmation(TextMatch $textmatch)
    {
        return view('admin.textmatch.delete-confirmation', compact('textmatch'));
    }
}

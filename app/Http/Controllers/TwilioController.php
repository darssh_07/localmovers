<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Twilio\Rest\Client;

use App\Models\localmovers\TwilioSMSDetails;
use App\Models\localmovers\TwilioCallDetails;


class TwilioController extends Controller
{
    public $sms;
    public $call;

    protected $fromno = '+13072195721';

    private function generateRandomString($length = 2) 
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private function curl_call($url, $value)
    {
        $ch = curl_init($url);
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_POSTFIELDS => $value,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_USERPWD => "ACa48684abc4b99cd1deb4c89693563203:2a93ab79da2976a589f734d388b95916",
            CURLOPT_TIMEOUT => 10
        ));

        $response = curl_exec($ch);
        curl_close($ch);
        return json_decode($response, true);
    }

    public function twiliosms(Request $request) 
    {
 
        $lead_id = $request->session()->get('lead_id');
        $fromno = $this->fromno;
        $post = $request->all();

        if (!empty($post)) {
            $phone = $post['phone'];
            $phone = preg_replace("/[^0-9]/", "", $phone);
            $data['to_number'] = $phone;
            $tonumber = "1" . $data['to_number'];


            $row = TwilioSMSDetails::isToNumberExistsForLeadID($data['to_number'], $lead_id);

            if (!empty($row)) {
                $message = $row->message;
                $text = "Your verification code is " . $message . " - LocalMovers - Your mover matching service";
                $counter = '1';
            } else {
                $message = $this->generateRandomString(2);
                $text = "Your verification code is " . $message . " - LocalMovers - Your mover matching service";
                $counter = '0';
            }

            $params = array(
                'To' => $tonumber,
                'From' => $fromno,
                'Body' => $text
            );

            $value = http_build_query($params);
            $url = 'https://api.twilio.com/2010-04-01/Accounts/ACa48684abc4b99cd1deb4c89693563203/Messages.json';

            $twilioResponse = $this->curl_call($url, $value);

            if ($counter == '0') {
                $resp = array(
                    'lead_id' => $lead_id,
                    'from_number' => $fromno,
                    'to_number' => $data['to_number'],
                    'message' => $message,
                    'sid' => $twilioResponse['sid']
                );
                TwilioSMSDetails::create($resp);
            }
            $result = 'true';
        } else {
            $result = 'false';
        }
        return $result;
    }



    public function twiliocall(Request $request) 
    {

        $lead_id = $request->session()->get('lead_id');
        $fromno = $this->fromno;

        $post = $request->all();

        if (!empty($post)) {
            $phone = $post['phone'];
            $data1['to_number'] = preg_replace("/[^0-9]/", "", $phone);
            $tonumber = "1" . $data1['to_number'];

            $row = TwilioCallDetails::isToNumberExistsForLeadID($data1['to_number'], $lead_id);

            if (!empty($row)) {
                $counter = '1';
            } else {
                $counter = '0';
            }

            $params = array(
                'From' => $fromno,
                'To' => $tonumber,
                'Url' => url('/twilio/answer') . '?lead_id=' . $lead_id,
                'statusCallback' => url('/twilio/hangup'),
                'statusCallbackEvent' => array("completed"),
                'statusCallbackMethod' => 'POST'
            );

            $value = http_build_query($params);
            $url = 'https://api.twilio.com/2010-04-01/Accounts/ACa48684abc4b99cd1deb4c89693563203/Calls.json';

            $twilioResponse = $this->curl_call($url, $value);

            if ($counter == 0) {
                $text = $this->generateRandomString(2);
                $resp = array(
                    'lead_id' => $lead_id,
                    'from_number' => $fromno,
                    'to_number' => $data1['to_number'],
                    'message' => $text,
                    'sid' => $twilioResponse['sid']
                );
                TwilioCallDetails::create($resp);
            } else {
                $resp = array(
                    'sid' => $twilioResponse['sid']
                );
                TwilioCallDetails::where('id', $row->id)->update($resp);
            }
            $result = 'true';
        } else {
            $result = 'false';
        }
        return $result;
    }


    public function twilioanswer(Request $request) 
    {

        $lead_id = $request->input('lead_id');
        $fromno = $this->fromno;


        $resdata = $request->all();
        $row = TwilioCallDetails::isSIDExists($resdata['CallSid']);

        if (!empty($row) && isset($row->message) && $row->message != null) {
            $wordstring = $this->numbertowords($row->message);
        } else {
            $text = $this->generateRandomString(2);
            $calldata = array(
                'lead_id' => $lead_id,
                'from_number' => $fromno,
                'to_number' => $resdata['To'],
                'message' => $text,
                'sid' => $resdata['CallSid']
            );
            TwilioCallDetails::create($calldata);
            $wordstring = $this->numbertowords($text);
        }

        $content = '<?xml version="1.0" encoding="UTF-8"?>
        <Response>
        <Pause length="1"/>
        <Say loop="2">Your Verification code for LocalMovers is ' . $wordstring . '</Say>
        </Response>';

        return response($content, 200)
                ->header('Content-Type', 'text/xml');

    }


    public function twiliohangup(Request $request) 
    {
        $resp = $request->all();

        $calldata = array(
            'call_duration' => $resp['CallDuration'],
        );
        TwilioCallDetails::sid($resp['CallSid'])->update($calldata);
        return 'true';
    }

    private function numbertowords($number) 
    {
        $nums = array(
            0 => ' zero',
            1 => ' one',
            2 => ' two',
            3 => ' three',
            4 => ' four',
            5 => ' five',
            6 => ' six',
            7 => ' seven',
            8 => ' eight',
            9 => ' nine',
        );
        $string = str_replace(array_keys($nums), array_values($nums), $number);
        return $string;
    } 


    public function confirmVerification(Request $request) 
    {
        $lead_id = $request->session()->get('lead_id');
        $post = $request->all();

        $message = $post['verify-code'];
        $type = $post['type'];
        $phone = $post['phone'];
        $to_number = preg_replace("/[^0-9]/", "", $phone);

        if ($type != 'call') {
            $row = TwilioSMSDetails::isLeadIdPhoneMessageExists($lead_id, $to_number, $message);
        } else {
            $row = TwilioCallDetails::isLeadIdPhoneMessageExists($lead_id, $to_number, $message);
        }

        if (!empty($row)) {
            $result = 'true';
        } else {
            $result = 'false';
        }

        if ($result == 'true') {
            if ($type != 'call')
                TwilioSMSDetails::toNumberAndLeadID($to_number, $lead_id)->update(['verified' => '1']);
            else
                TwilioCallDetails::toNumberAndLeadID($to_number, $lead_id)->update(['verified' => '1']);
        }
        return $result;
    }  
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }


    public function login()
    {
        return view('admin.login');
    }

    
    public function auth_user()
    {
        if (!Auth::attempt(request(['username', 'password']))) {
            return back()->withErrors([
                'message' => 'Please check your credentials and try again.'
            ]);
        }
        return redirect('/admin');
    }


    public function logout()
    {
        Auth::logout();
        return redirect('admin/login');
    }
}

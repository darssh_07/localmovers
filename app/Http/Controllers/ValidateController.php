<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\localmovers\CityState;
use App\Models\localmovers\Lead;
use App\Models\Admin\TextMatch;
use App\Models\localmovers\PhoneNumberLog;
use App\Models\localmovers\XverifyPhone;
use App\Models\localmovers\AfterHourRoutingLeads;
use App\Models\localmovers\AHPopupEvents;
use App\Models\localmovers\MatchClicked;

class ValidateController extends Controller
{
    public function searchForCityState(Request $request)
    {
        $search_term = $request->input('search_term');

        if (is_numeric($search_term)) {
            $list = $this->searchByZipcode($search_term);
        } else {
            $list = $this->searchByCityState($search_term);
        }

        return json_encode($list);
    }


    public function searchForCityState2(Request $request)
    {
        $search_term = $request->input('search_term');

        if (is_numeric($search_term)) {
            $list = $this->searchByZipcode2($search_term);
        } else {
            $list = $this->searchByCityState($search_term);
        }
        return json_encode($list);
    }


    public function searchByZipcode($search_term)
    {
        $list = CityState::searchCityStateByZipcode($search_term);
        return $list;
    }

    public function fetchCityStateByZipcode(Request $request)
    {
        $zipcode = $request->input('zipcode');
        $cityState = CityState::cityStateByZipcode($zipcode);
        if (!empty($cityState)) {
            $cityState->city_name = ucwords(strtolower($cityState->city_name));
            $cityState->state = strtoupper($cityState->state);
        }
        return !empty($cityState) ? $cityState : 'false';
    }

    public function searchByZipcode2($search_term)
    {
        $list = CityState::searchCityStateByZipcode2($search_term);
        return $list;
    }

    public function searchByCityState($search_term)
    {
        $list = CityState::searchForCityStateNew($search_term);
        return $list;
    }

    
    public function firstname(Request $request)
    {
        return $this->validate_data($request, 'first_name');
    }

    public function lastname(Request $request)
    {
        return $this->validate_data($request, 'last_name');
    }

    public function phone(Request $request)
    {
        return $this->validate_data($request, 'phone_number');
    }

    public function phone2(Request $request)
    {
        return $this->validate_data($request, 'phone');
    }

    public function email(Request $request)
    {
        return $this->validate_data($request, 'email_address');
    }

    public function rooms(Request $request)
    {
        return $this->validate_data($request, 'number_of_rooms');
    }

    public function fromZipcode(Request $request)
    {
        return $this->validate_data($request, 'zip_from');
    }

    public function toZipcode(Request $request)
    {
        return $this->validate_data($request, 'zip_to');
    }

    public function movedate(Request $request)
    {
        return $this->validate_data($request, 'move_date');
    }


    public function hasAccess(Request $request)
    {
        if ($request->ajax()) {
            return true;
        }
        return false;
    }

    public function routeAfterHour(Request $request)
    {
        $data = $request->all();
        
        $session_id = $request->session()->get('session_id');
        $lead_id = Lead::isLeadExistsForSessionId($session_id);
        
        if (isset($data['route'])) {
            AfterHourRoutingLeads::updateOrCreate(
                ['lead_id' => $lead_id->lead_id],
                []
            );
            return 'true';
        }
        return 'false';
    }

    public function afterHoursPopupEvents(Request $request)
    {
        $data = $request->all();
        
        $session_id = $request->session()->get('session_id');
        $lead_id = Lead::isLeadExistsForSessionId($session_id);
        
        if (isset($data['open'])) {
            AHPopupEvents::updateOrCreate(
                ['lead_id' => $lead_id->lead_id],
                ['open' => '1']
            );
            return 'true';
        } elseif (isset($data['yes'])) {
            AHPopupEvents::updateOrCreate(
                ['lead_id' => $lead_id->lead_id],
                ['yes' => '1']
            );
            return 'true';
        } elseif (isset($data['no'])) {
            AHPopupEvents::updateOrCreate(
                ['lead_id' => $lead_id->lead_id],
                ['no' => '1']
            );
            return 'true';
        } elseif (isset($data['continue_quote'])) {
            AHPopupEvents::updateOrCreate(
                ['lead_id' => $lead_id->lead_id],
                ['continue_quote' => '1']
            );
            return 'true';
        }
        return 'false';
    }


    public function matchClicked(Request $request)
    {
        $data = $request->all();
        
        $session_id = $request->session()->get('session_id');
        $lead_id = Lead::isLeadExistsForSessionId($session_id);
        
        if (isset($data['match_id'])) {
            MatchClicked::updateOrCreate(
                [
                    'lead_id' => $lead_id->lead_id,
                    'match_id' => $data['match_id']
                ],
                []
            );
            return 'true';
        }
        return 'false';
    }

    public function validate_data($request, $field = null)
    {
        if ($this->hasAccess($request)) {
            $data = $request->all();

            $session_id = $request->session()->get('session_id');
            $lead_id = Lead::isLeadExistsForSessionId($session_id);

            if (array_key_exists($field, $data)) {
                $result = 'true';
                $error = '';

                if ($field == 'phone_number'  || $field == 'phone') {
                    $data[$field] = preg_replace("/[^0-9]/", "", $data[$field]);

                    $result = $this->verify_phone($data[$field], $session_id);
                    $result2 = strlen($data[$field]) == 10 ? true : false;
                    $this->store_number($session_id, $data[$field]);
                    if ($result == 'true' && $result2 == true) {
                        $areacodes = array('888', '800', '877', '866', '900','000');
                        if (in_array(substr($data[$field], 0, 3), $areacodes)) {
                            $result = 'false';
                        }
                        switch (substr($data[$field], 3, 7)) {
                            case '0000000':
                            case '1111111':
                            case '2222222':
                            case '3333333':
                            case '4444444':
                            case '5555555':
                            case '6666666':
                            case '7777777':
                            case '8888888':
                            case '9999999':
                            case '5551212':
                                $result = 'false';
                                // no break
                            default:
                        }
                    } else {
                        $result = 'false';
                    }
                }


                if ($field == 'city_to') {
                    $city_to = explode(',', $data[$field]);
                    unset($data[$field]);
                    $data['zip_to'] = $city_to[1];
                }


                if ($field == 'zip_to' || $field == 'zip_from') {
                    if (!CityState::cityStateByZipcode($data[$field]) || strlen($data[$field]) < 4) {
                        $result = 'false';
                    }
                }

                if ($field == 'first_name') {
                    if (TextMatch::isExactName($data[$field]) ||
                        TextMatch::isNameSet($data[$field], isset($lead_id) ? $lead_id->last_name : '')) {
                        $result = 'false';
                    }
                }

                if ($field == 'last_name') {
                    if (TextMatch::isExactName($data[$field]) ||
                        TextMatch::isNameSet(isset($lead_id) ? $lead_id->first_name : '', $data[$field])) {
                        $result = 'false';
                    }
                }

                if ($field == 'first_name' || $field == 'last_name') {
                    if (TextMatch::check_three_repeating_characters($data[$field])) {
                        $result = 'false';
                    }

                    $vowels = array('a', 'e', 'i', 'o', 'u', 'y');
                    $vowelfound = false;
                    foreach ($vowels as $vowel) {
                        if (stripos($data[$field], $vowel) !== false) {
                            $vowelfound = true;
                        }
                    }
                    if (!$vowelfound && strlen($data[$field]) > 2) {
                        $result = 'false';
                    }
                    $re = '/([0-9])|([\/~\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\])/';
                    if (preg_match($re, $data[$field], $match)) {
                        $result = "false";
                    }
                }


                if ($field == 'email_address') {
                    $data[$field] = preg_replace('/\s+/', '', $data[$field]);
                    $domain = stristr($data[$field], '@');
                    $address = stristr($data[$field], '@', true);
                    $address_length = strlen($address);
                    if (TextMatch::isFullEmail($data[$field]) || TextMatch::isAddressEmail($address) || TextMatch::isDomainEmail($domain)) {
                        $result = 'false';
                    }
                    preg_match_all('/(.)\1+/', $data[$field], $matches);
                    $special_chars_occurs = array_combine($matches[1], array_map('strlen', $matches[0]));
                    if (in_array('*', $special_chars_occurs)|| in_array('#', $special_chars_occurs)
                            || in_array('$', $special_chars_occurs) || in_array('&', $special_chars_occurs)) {
                        $result = 'false';
                    }
                       
                    if (!filter_var($data[$field], FILTER_VALIDATE_EMAIL)) {
                        $result = 'false';
                    }

                    if (strcasecmp($domain, '@gmail.com') == 0) {
                        if ($address_length <= 3 || $address_length > 30) {
                            $result = 'false';
                        }
                    } else {
                        if ($address_length <= 3) {
                            $result = 'false';
                        }
                    }
                }


                if ($field == 'move_date') {
                    if (empty($data[$field])) {
                        $result = "false";
                    } else {
                        $data[$field] = date("Y-m-d", strtotime($data[$field]));
                    }
                }


                if (empty($data[$field])) {
                    $result = "false";
                }
                
                $session_id = $request->session()->get('session_id');
                
                if (in_array($field, array('first_name','last_name','email_address','phone_number','phone',
                    'zip_to', 'zip_from', 'move_date','number_of_rooms'))) {
                    if ($field == 'email_address') {
                        $data['email'] = $data[$field];
                        unset($data[$field]);
                    } elseif ($field == 'phone_number') {
                        $data['phone'] = $data[$field];
                        unset($data[$field]);
                    } elseif ($field == 'zip_to') {
                        $data['to_zip'] = $data[$field];
                        unset($data[$field]);
                    } elseif ($field == 'zip_from') {
                        $data['from_zip'] = $data[$field];
                        unset($data[$field]);
                    } elseif ($field == 'number_of_rooms') {
                        if ($data[$field] == '') {
                            $result ='false';
                        }
                        $data['move_size'] = $data[$field];
                        unset($data[$field]);
                    }

                    if ($result == 'true') {
                        unset($data['_token']);
                        Lead::where('session_id', $session_id)->update($data);
                    }
                }
            } else {
                $result = 'false';
            }

            return $result;
        }
    }


    // code for phone validation for trusted validation
    public function validate_phone($phone)
    {
        $url = "http://166.78.135.166/validation.php";
        $data = "domain=budgetvanlines.com&api_key=111457&phone=$phone";
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);

        curl_close($ch);

        return json_decode($result);
    }


    // Below this are verification of phone and email via xverified. It is turned either on or off via the validate function.

    public function verify_phone($phone, $session_id)
    {
        $xverify = $this->phone_has_been_verified_previously($phone);
        $json = $this->validate_phone($phone);

        if ($json) {
            $data = array(
                'session_id' => $session_id,
                'date_time' => date("Y-m-d H:i:s"),
                'phone' => $phone,
                'error' => $json->error,
                'status' => $json->status,
                'message' => $json->message,
                'duration' => '1',
                'areacode' => '',
                'prefix' => substr($phone, 0, 3),
                'suffix' => substr($phone, 3, 3),
                'latitude' => '',
                'longitude' => '',
                'extension' => '',
                'city' => '',
                'state' => '',
                'country' => 'US',
                'timezone' => '',
                'timezonecode' => '',
                'exchange_type' => '',
                'phone_type' => '',
                'ran_curl_call' => '1'
            );
        } else {
            $data = array(
                'session_id' => $session_id,
                'date_time' => date("Y-m-d H:i:s"),
                'phone' => $phone,
                'ran_curl_call' => '0'
            );
        }
        if ($xverify) {
            XverifyPhone::where('id', $xverify->id)
                            ->update($data);
        } else {
            XverifyPhone::create($data);
        }
        if ($json) {
            if ($json->status == 'invalid' && $json->error == '0') {
                return 'false';
            }
        }
        return 'true';
    }


    public function phone_has_been_verified_previously($phone)
    {
        return XverifyPhone::where('phone', $phone)->first();
    }


    public function store_number($session_id, $phone)
    {
        PhoneNumberLog::updateOrCreate(
            ['session_id' => $session_id, 'phone' => $phone],
            ['created_date' => date("Y-m-d H:i:s")]
        );
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CalculateController extends Controller
{
    // for calculating quote range
    public function calc(Request $request)
    {
        $miles = explode(' ', $request->input('miles'));
        $distance = str_replace(",", "", $miles[0]);
        $rooms = array(
            'studio' => array(
                            'min' => 200,
                            'max' => 300
                        ),
            '1 bedroom' => array(
                            'min' => 300,
                            'max' => 500
                        ),
            '2 bedroom' => array(
                            'min' => 500,
                            'max' => 700
                        ),
            '3 bedroom' => array(
                            'min' => 700,
                            'max' => 1000
                        ),
            '4 bedroom' => array(
                            'min' => 1000,
                            'max' => 1500
                        ),
            '5 bedroom' => array(
                            'min' => 1500,
                            'max' => 2000
                        ),
            'Moving Boxes Only' => array(
                            'min' => 200,
                            'max' => 300
                        ),
            'one bedrooms house'	=> array(
                            'min' => 300,
                            'max' => 500
                        ),
            'one bedrooms apartment' => array(
                            'min' => 300,
                            'max' => 500
                        ),
            'two bedrooms house' => array(
                            'min' => 500,
                            'max' => 700
                        ),
            'two bedrooms apartment' => array(
                            'min' => 500,
                            'max' => 700
                        ),
            'three bedrooms house' => array(
                            'min' => 700,
                            'max' => 1000
                        ),
            'three bedrooms apartment' => array(
                            'min' => 700,
                            'max' => 1000
                        ),
            'four bedrooms house' => array(
                            'min' => 1000,
                            'max' => 1500
                        ),
            'four bedrooms apartment' => array(
                            'min' => 1000,
                            'max' => 1500
                        ),
            'five bedrooms house' => array(
                            'min' => 1500,
                            'max' => 2000
                        ),
            'five bedrooms apartment' => array(
                            'min' => 1500,
                            'max' => 2000
                        ),
            'six bedrooms house' => array(
                            'min' => 2000,
                            'max' => 3000
                        ),
            'six bedrooms apartment' => array(
                            'min' => 1500,
                            'max' => 3800
                        ),
            'six bedrooms and more house' => array(
                            'min' => 2000,
                            'max' => 3000
                        ),
            'six bedrooms and more apartment' => array(
                            'min' => 2500,
                            'max' => 3800
                        ),
            'six bedrooms and more' => array(
                            'min' => 2500,
                            'max' => 3800
                        )
        );

        $rooms_value = $request->input('rooms');
        $min = $this->calculate((float) $distance, $rooms[$rooms_value]['min']);
        $max = $this->calculate((float) $distance, $rooms[$rooms_value]['max']);
        $min_price = $min['cost']['70'];
        $max_price = $max['cost']['67'];
        if ($min_price < 200) {
            $min_price = $min_price + 200;
            $max_price = $max_price + 200;
        }
        $price = array(
            'min' => number_format(ceil($min_price)),
            'max' => number_format(ceil($max_price))
        );
        return json_encode($price);
    }


    private function calculate($miles, $cube)
    {
        $trgas = $miles * 2 / 3;
        
        $min = ($miles <= 100 ? (125 + 3.75 * $miles): ($miles <= 200 ? (500 + 2*($miles-100)) : ($miles <= 500 ? (700 + 0.5 * ($miles - 200)) : ($miles <= 600 ? (850 + 0.25 * ($miles - 500)) : 875 + ($miles - 600) * 0.1))));
        
        $RPC = ($miles <= 300 ? (1.25 + ($miles * 0.0025)) : ($miles <= 400 ? (2 + ($miles - 300) * 0.002) : ($miles <= 600 ? (2.2 + ($miles - 400) * 0.001) : ($miles <= 1400 ? (2.4 + ($miles - 600) * 0.0005) : ($miles <= 2100 ? (2.85 + ($miles - 1400) * 0.001) : ($miles <= 2600 ? (3.55 + ($miles - 2100)*0.0007) : (3.9 + ($miles - 2600) * 0.0005)))))));
        
        $COEF = ($cube <= 185 ? 1 : ($cube <= 300 ? (1 - ($cube - 185) * 0.0015) : ($cube <= 400 ? (0.8275 + (($cube - 300) * 0.0008)) : ($cube <= 500 ? (0.9075 + (($cube - 400) * 0.00055)) : ($cube <= 600 ? (0.9625 + (($cube - 500) * 0.000375)) : 1)))));
        
        $SCOEF = (($miles >= 1599 && $cube >= 290 && $cube <= 320) ? $COEF + 0.04 : (($miles >= 1599 && $cube >= 321 && $cube <= 340) ? 0.8835-(($cube-321)*0.001) : $COEF));
        
        if ($cube == 0) {
            $cube = 0.0000000001;
        }

        $CO = ($cube <= 143 ? $min / $cube : ($min + (($cube - 143) * $RPC)) / $cube);
        $RRPC = array(
            '70' => $CO * 2.75 * 0.3 * $SCOEF,
            '67' => $CO * 2.75 * 0.33 * $COEF,
            '63' => $CO * 2.75 * 0.37 * $COEF,
            '55' => $CO * 2.75 * 0.45 * $COEF,
            '45' => $CO * 2.75 * 0.55 * $COEF
        );

        
        foreach ($RRPC as $key => $value) {
            $Ibs[$key] = $value / 7;
            $cost[$key] = $cube * $value * 1.1;
        }
        return array(
            'Ibs' => $Ibs,
            'cost' => $cost,
            'RRPC' => $RRPC
        );
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\Postmark;

use App\Models\localmovers\JoinNetwork;
use App\Models\localmovers\CCPAForms;
use App\Models\localmovers\Lead;
use App\Models\localmovers\PhoneNumberLog;
use App\Models\localmovers\TwilioCallDetails;
use App\Models\localmovers\TwilioSMSDetails;
use App\Models\localmovers\PlivoCallDetails;
use App\Models\localmovers\PlivoSMSDetails;
use App\Models\localmovers\Targus;
use App\Models\localmovers\Unsubscribe;
use App\Models\localmovers\XverifyPhone;

use App\Http\Traits\SessionTrait;

class ContactController extends Controller
{
    use SessionTrait;
   

    public function network_submit(Request $request)
    {
        if ($request->ajax()) {
            $data = array();
            $data['session_id'] = $request->session()->get('session_id');

            if (empty($data['session_id'])) {
                $data['session_id'] = $this->createSessionId($request);
            }
            
            $phone_number = $request->input('lead_number');
            $data['phone'] = preg_replace("/[^0-9]/", "", $phone_number);

            $data['name'] = $request->input('lead_name');
            $data['email'] = $request->input('lead_email');
            
            $subject = 'Join our network (LocalMovers) - '.$data['name'];
            
            $data['business'] = $request->input('lead_business');

            $message = '';
            if ($data['phone']) {
                $message .= 'Phone : ' . $data['phone'] . "<br />";
            }
            if ($data['email']) {
                $message .= 'Email : ' . $data['email']  . "<br />";
            }
            if ($data['name']) {
                $message .= 'Name : ' . $data['name'] . "<br />";
            }
            if ($data['business']) {
                $message .= 'Company Name : ' . $data['business'] . "<br />";
            }

            dd();
            $email = 'moverleads@quoterunner.co';

            $postmark = new Postmark('b5af1821-f53b-4d35-bfe2-025a41d9b317', 'noreply@equatemedia.com');
            $sent = $postmark->to($email)
                    ->subject($subject)
                    ->html_message($message)
                    ->send();
    
            JoinNetwork::create($data);

            if ($sent) {
                echo "true";
            } else {
                echo "false";
            }
        }
    }
    
    public function ca_forms_submit(Request $request)
    {
        if ($request->ajax()) {
            $data = array();
            $data['session_id'] = $request->session()->get('session_id');
            
            if (empty($data['session_id'])) {
                $data['session_id'] = $this->createSessionId($request);
            }
            $data['name'] = $request->input('name');
            $data['email'] = $request->input('email');
            $data['phone'] = preg_replace("/[^0-9]/", "", $request->input('phone'));
            $data['form_type'] = ($request->input('form_type') == 'right_to_know') ? 1 : 2;
            
            CCPAForms::create($data);

            
            $message = '';
            $message .= 'Name : ' . $data['name'] . "<br />";
            $message .= 'Email : ' . $data['email']  . "<br />";
            $message .= 'Phone : ' . $data['phone'] . "<br />";

            dd();
            $email = 'CCPA@localmovers.com';
            $subject = 'CCPA (' . (($request->input('form_type') == 'right_to_know') ? 'Right To Know' : 'Delete My Info') . ') - 2movers.com';
            
            $postmark = new Postmark('b5af1821-f53b-4d35-bfe2-025a41d9b317', 'noreply@equatemedia.com');
            $sent = $postmark->to($email)
            ->subject($subject)
            ->html_message($message)
            ->send();

            echo "true";
        }
    }

    public function deleteUserInfo(Request $request)
    {
        if ($request->ajax()) {
            $data = array();

            $data['email'] = $request->input('email');
            $data['new_email'] = $request->input('new_email');
            $data['phone'] = preg_replace("/[^0-9]/", "", $request->input('phone'));

        
            Lead::where(function ($query) {
                $query->where('email', '=', $data['email'])
                      ->orWhere('phone_number', '=', $data['phone']);
            })->update(['email' => $data['new_email'], 'phone_number' => '9999999999']);


        
            PhoneNumberLog::where('phone', $data['phone'])->update(['phone' => '9999999999']);


        
            TwilioCallDetails::where('to_number', $data['phone'])->update(['phone' => '9999999999']);


        
            TwilioSMSDetails::where('to_number', $data['phone'])->update(['phone' => '9999999999']);


        
            PlivoCallDetails::where('to_number', $data['phone'])->update(['phone' => '9999999999']);


        
            PlivoSMSDetails::where('to_number', $data['phone'])->update(['phone' => '9999999999']);


        
            Unsubscribe::where('email', $data['email'])->update(['email' => $data['new_email']]);


        
            XverifyPhone::where('phone', $data['phone'])->update(['phone' => '9999999999']);


        
            Targus::where(function ($query) {
                $query->where('email', '=', $data['email'])
                      ->orWhere('phone', '=', $data['phone']);
            })->update(['email' => $data['new_email'], 'phone' => '9999999999']);


            CCPAForms::where(function ($query) {
                $query->where('email', '=', $data['email'])
                      ->orWhere('phone', '=', $data['phone']);
            })->update(['email' => $data['new_email'], 'phone' => '9999999999']);

            echo "true";
        }
    }
}

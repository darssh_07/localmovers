<?php

namespace App\Models\Admin;

use App\Models\Model;
use Illuminate\Support\Facades\DB;

class TextMatch extends Model
{
    protected $table = 'textmatch';
    protected $connection = 'admin';
    public $timestamps = false;

    public static $textmatch_settings;

    public static function fetchAll()
    {
        return static::all();
    }


    public static function fetchByType($type)
    {
        $resultSet = static::where('type', $type)
                            ->get();
        return !empty($resultSet) ? $resultSet : false;
    }


    public static function fetchByTypeAndText($type, $text)
    {
        $resultSet = static::where([
                            ['type', $type],
                            ['text', $text]
                        ])
                        ->count();
        return !empty($resultSet) ? true : false;
    }

    public static function isExactName($value)
    {
        if (self::$textmatch_settings && self::$textmatch_settings->exact_name == 0) {
            return false;
        }

        return static::fetchByTypeAndText('exact', $value);
    }

    public static function isFullEmail($value)
    {
        if (self::$textmatch_settings && self::$textmatch_settings->full_email == 0) {
            return false;
        }
        return static::fetchByTypeAndText('full-email', $value);
    }

    public static function isAddressEmail($value)
    {
        if (self::$textmatch_settings && self::$textmatch_settings->address_email == 0) {
            return false;
        }
        return static::fetchByTypeAndText('address-email', $value);
    }

    public static function isDomainEmail($value)
    {
        if (self::$textmatch_settings && self::$textmatch_settings->domain_email == 0) {
            return false;
        }
        return static::fetchByTypeAndText('domain-email', $value);
    }

    public static function check_three_repeating_characters($name)
    {
        $last_letter = '';
        $letter_count = 1;
        $arr_name = str_split($name);
        foreach ($arr_name as $letter) {
            if (strcasecmp($last_letter, $letter) == 0) {
                $letter_count++;
            } else {
                $letter_count = 1;
            }

            if ($letter_count >= 3) {
                return true;
            }

            $last_letter = $letter;
        }
        return false;
    }

    public static function setTextMatchSettingsTable()
    {
        self::$textmatch_settings = DB::table('textmatch_settings')->first();
    }

    public static function isNameSet($first_name, $last_name)
    {
        if (self::$textmatch_settings && self::$textmatch_settings->name_set == 0) {
            return false;
        }

        $value = trim($first_name) . ' ' . trim($last_name);
        return static::fetchByTypeAndText('name-set', $value);
    }
}

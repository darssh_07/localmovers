<?php

namespace App\Models\localmovers;

use App\Models\Model;

class LeadAfterHours extends Model
{
    protected $table = 'lead_after_hours';

    public static function isLeadExistsForMatrixLeadID($matrix_lead_id)
    {
        return static::where('matrix_lead_id', $matrix_lead_id)->first();
    }
}

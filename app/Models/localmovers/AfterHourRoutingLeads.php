<?php

namespace App\Models\localmovers;

use App\Models\Model;

class AfterHourRoutingLeads extends Model
{
    protected $table = 'after_hour_routing_leads';
    const UPDATED_AT = null;
}

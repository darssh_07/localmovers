<?php

namespace App\Models\localmovers;

use App\Models\Model;

class AdditionalDetails extends Model
{
    protected $table = 'additional_details';

    public static function isADExistsForLeadID($lead_id)
    {
        return static::where('lead_id', $lead_id)->first();
    }
}

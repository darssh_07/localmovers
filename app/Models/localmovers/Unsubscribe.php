<?php

namespace App\Models\localmovers;

use App\Models\Model;

class Unsubscribe extends Model
{
    public static function isEmailUnsubscribed($email)
    {
        $email = static::where('email', $email)
                        ->first();
        return (isset($email) && $email->is_unsubscribed == '1');
    }
}

<?php

namespace App\Models\localmovers;

use App\Models\Model;

class LeadNetwork extends Model
{
    protected $primaryKey = 'session_id';
    protected $table = 'lead_network';
}

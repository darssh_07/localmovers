<?php

namespace App\Models\localmovers;

use App\Models\Model;
use Illuminate\Support\Facades\DB;

class CityState extends Model
{
    protected $table = 'city_state';


    public function scopeSearchCityStateByZipcodeQuery($query, $search_term)
    {
        return $query->where('zipcode', 'like', $search_term .'%')
                        ->take(10);
    }


    public static function searchCityStateByZipcode($search_term)
    {
        $result = static::searchCityStateByZipcodeQuery($search_term)->get();

        $city_list = array();
        foreach ($result as $l) {
            $city_list[ucwords(strtolower($l->city_name)) . ', ' . $l->state] = sprintf("%05d", $l->zipcode);
        }
        return $city_list;
    }


    public static function searchCityStateByZipcode2($search_term)
    {
        $result = static::searchCityStateByZipcodeQuery($search_term)->get();

        $city_list = array();
        foreach ($result as $l) {
            $city_list[] = sprintf("%05d", $l->zipcode).' '.ucwords(strtolower($l->city_name)).', '.$l->state;
        }
        return $city_list;
    }


    public static function searchForCityStateNew($term)
    {
        $term = explode(',', $term);
        $result = DB::table('city_state_latlng')
                    ->select('city_name', 'state', 'zipcode')
                    ->where('city_name', 'like', trim($term[0]) . '%')
                    ->when(isset($term[1]), function ($query) use ($term) {
                        return $query->where('state', 'like', trim($term[1]) . '%');
                    })
                    ->groupBy('city_name', 'state')
                    ->take(30)
                    ->orderBy('population', 'desc')
                      ->get();

        $city_list = array();
        foreach ($result as $l) {
            $city_list[ucwords(strtolower($l->city_name)) . ', ' . $l->state] = sprintf("%05d", $l->zipcode);
        }
        return $city_list;
    }


    public static function cityStateByZipcode($zipcode)
    {
        return static::where('zipcode', $zipcode)->first();
    }
}

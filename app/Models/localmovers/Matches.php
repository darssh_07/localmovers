<?php

namespace App\Models\localmovers;

use App\Models\Model;

class Matches extends Model
{
    protected $table = 'matches';
    protected $primaryKey = 'match_id';
    public $timestamps = false;
}

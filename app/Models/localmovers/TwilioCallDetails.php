<?php

namespace App\Models\localmovers;

use App\Models\Model;

class TwilioCallDetails extends Model
{
    public function scopeToNumberAndLeadID($query, $to_number, $lead_id)
    {
        return $query->where([
                ['to_number', $to_number],
                ['lead_id', $lead_id]
            ]);
    }


    public function scopeSID($query, $sid)
    {
        return $query->where('sid', $sid);
    }
    

    public static function isToNumberExistsForLeadID($to_number, $lead_id)
    {
        return static::toNumberAndLeadID($to_number, $lead_id)
            ->first();
    }


    public static function isSIDExists($sid)
    {
        return static::sid($sid)->first();
    }


    public static function isLeadIdPhoneMessageExists($lead_id, $to_number, $message)
    {
        return static::toNumberAndLeadID($to_number, $lead_id)
            ->where('message', $message)
            ->first();
    }
}

<?php

namespace App\Models\localmovers;

use App\Models\Model;

class Emails extends Model
{
    public $timestamps = false;

    public function scopeLeadIDEmailNOCombo($query, $lead_id, $email_no)
    {
        return $query->where([
                ['lead_id', $lead_id],
                ['email_no', $email_no]
            ]);
    }

    public static function isEmailSentForLeadIDEmailNo($lead_id, $email_no)
    {
        $email = static::leadIDEmailNOCombo($lead_id, $email_no)
                        ->first();
        return (isset($email) && $email->is_sent == '1');
    }
}

<?php

namespace App\Models\localmovers;

use App\Models\Model;

class EoMatrixSubmission extends Model
{
    public $timestamps = false;
    protected $table = 'eo_matrix_submissions';
}

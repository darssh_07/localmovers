<?php

namespace App\Models\localmovers;

use App\Models\Model;

class LeadMatches extends Model
{
    protected $table = 'lead_matches';
    protected $primaryKey = 'id';
}

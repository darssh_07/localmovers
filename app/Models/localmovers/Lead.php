<?php

namespace App\Models\localmovers;

use Illuminate\Support\Facades\DB;
use App\Models\Model;

class Lead extends Model
{
    protected $primaryKey = 'lead_id';

    public static function isLeadExistsForSessionId($session_id)
    {
        return static::where('session_id', $session_id)->first();
    }

    public static function isLeadExistsForToken($token)
    {
        return static::where('token', $token)->first();
    }

    public static function isLeadExistsForMatrixLeadID($matrix_lead_id)
    {
        return static::where('matrix_lead_id', $matrix_lead_id)->first();
    }

    public static function LeadsBWTime($from_time, $to_time)
    {
        return static::whereBetween('leads.updated_at', [$from_time, $to_time])
                        ->whereNotNull('email')
                        ->latest()
                        ->get();
    }

    public static function LeadsWithOutPhoneBWTime($from_time, $to_time)
    {
        return static::whereBetween('leads.updated_at', [$from_time, $to_time])
                        ->whereNotNull('email')
                        ->where(function ($query) {
                            $query->whereNull('phone')
                                    ->orWhere('phone', 0);
                        })
                        ->latest()
                        ->get();
    }

    public static function LeadsWithOutPhoneBWTimeNoNewMat($from_time, $to_time)
    {
        return DB::table('leads')
        ->select('leads.*')
            ->leftJoin('eo_matrix_submissions', 'leads.lead_id', '=', 'eo_matrix_submissions.lead_id')
            ->whereBetween('leads.updated_at', [$from_time, $to_time])
            ->whereNull('eo_matrix_submissions.id')
        ->whereNotNull('email')
            ->where(function ($query) {
                $query->whereNull('phone')
                        ->orWhere('phone', 0);
            })
            ->latest()
            ->get();
    }

    public static function LeadsWithNullMatrixLeadIdBWTime($from_time, $to_time)
    {
        return static::whereBetween('leads.updated_at', [$from_time, $to_time])
                        ->whereNotNull('first_name')
                        ->whereNotNull('email')
                        ->whereNotNull('phone')
                        ->where('phone', '!=', 0)
                        ->whereNull('matrix_lead_id')
                        ->latest()
                        ->take(500)
                        ->get();
    }
}

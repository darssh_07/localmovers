<?php

namespace App\Models\localmovers;

use App\Models\Model;

class MatchClicked extends Model
{
    protected $table = 'match_clicked';
    const UPDATED_AT = null;
}

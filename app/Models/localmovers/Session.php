<?php

namespace App\Models\localmovers;

use App\Models\Model;

class Session extends Model
{
    protected $table = 'session';
    public $timestamps = false;

    public static function isSessionExists($session)
    {
        return static::where('session', $session)->first();
    }

    public static function ipCount($ip)
    {
        return static::where('IP', $ip)->count();
    }
}

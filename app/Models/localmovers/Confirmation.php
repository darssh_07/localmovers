<?php

namespace App\Models\localmovers;

use App\Models\Model;

class Confirmation extends Model
{
    protected $table = 'confirmation';
    public $timestamps = false;

    public static function isSessionIdExists($session_id)
    {
        return static::where('session_id', $session_id)->first();
    }
}

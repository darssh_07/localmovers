<?php

namespace App\Models\localmovers;

use App\Models\Model;

class CCPAForms extends Model
{
    protected $table = 'ccpa_forms';
    const UPDATED_AT = null;
}

<?php

namespace App\Models\localmovers;

use App\Models\Model;

class PhoneNumberLog extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'session_id';
}

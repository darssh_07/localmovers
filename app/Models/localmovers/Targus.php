<?php

namespace App\Models\localmovers;

use App\Models\Model;

class Targus extends Model
{
    protected $table = 'targus';
    public $timestamps = false;


    public function scopePhoneEmail($query, $phone, $email)
    {
        return $query->where([
                ['phone', $phone],
                ['email', $email]
            ]);
    }


    public static function isPhoneEmailComboExists($phone, $email)
    {
        return static::phoneEmail($phone, $email)
            ->first();
    }

    public static function isLeadIDForPhoneEmail($lead_id, $phone, $email)
    {
        return static::phoneEmail($phone, $email)
            ->where('lead_id', $lead_id)
            ->first();
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MovingController@index');

// Moving Routes
Route::get('/{url}', 'MovingController@index')
    ->where('url', '(moving|Moving|MOVING|VIEW_MOVERS_RATES|FIND_MOVERS|Find_Movers|find_movers|MOVERS_NEAR_ME|Movers_Near_Me|movers_near_me)');

Route::post('/moving/send', 'MovingController@send');
Route::get('/moving/send_manually', 'MovingController@send_lead_manually');
Route::get('/moving/send_email_only_lead_to_nm', 'MovingController@send_email_only_lead_to_nm');
Route::post('/moving/saveAdditionalDetails', 'MovingController@saveAdditionalDetails');
Route::get('/moving/getLeadMatches', 'MovingController@getLeadMatches');
Route::get('/moving/confirmation', 'MovingController@confirmation');
Route::match(array('GET', 'POST'), '/unsubscribe', 'MovingController@unsubscribe');

// Validate Routes
Route::post('/validate/searchForCityState2', 'ValidateController@searchForCityState2');
Route::post('/validate/searchForCityState', 'ValidateController@searchForCityState');
Route::post('/validate/fetchCityStateByZipcode', 'ValidateController@fetchCityStateByZipcode');
Route::post('/validate/fromZipcode', 'ValidateController@fromZipcode');
Route::post('/validate/rooms', 'ValidateController@rooms');
Route::post('/validate/movedate', 'ValidateController@movedate');
Route::post('/validate/toZipcode', 'ValidateController@toZipcode');
Route::post('/validate/email', 'ValidateController@email');
Route::post('/validate/firstname', 'ValidateController@firstname');
Route::post('/validate/lastname', 'ValidateController@lastname');
Route::post('/validate/phone', 'ValidateController@phone');
Route::post('/validate/phone2', 'ValidateController@phone2');
Route::post('/validate/routeAfterHour', 'ValidateController@routeAfterHour');
Route::post('/validate/ah_popup_events', 'ValidateController@afterHoursPopupEvents');
Route::post('/validate/match_clicked', 'ValidateController@matchClicked');


// Targus Routes
Route::post('/targus', 'TargusController@doTargusNuSOAPCall');


// Twilio Routes
Route::post('/twiliosms', 'TwilioController@twiliosms');
Route::post('/twiliocall', 'TwilioController@twiliocall');
Route::post('/twilio/answer', 'TwilioController@twilioanswer');
Route::post('/twilio/hangup', 'TwilioController@twiliohangup');
Route::post('/twilio/confirmverification', 'TwilioController@confirmVerification');


// Calculate Routes
Route::post('/calc', 'CalculateController@calc');

// Contact Routes
Route::post('/contact/network_submit', 'ContactController@network_submit');
Route::post('/contact/ca_forms_submit', 'ContactController@ca_forms_submit');
Route::view('/Do-not-sell-ca', 'contact/do_not_sell');
Route::view('/right-to-know-ca', 'contact/right_to_know');
Route::view('/delete-my-info-ca', 'contact/delete_my_info');
Route::post('/contact/deleteUserInfo', 'ContactController@deleteUserInfo');


// Admin Routes
Route::get('/admin', 'AdminController@index');
Route::get('/admin/logout', 'LoginController@logout')->name('logout');
Route::get('/admin/login', 'LoginController@login')->name('login');
Route::post('/admin/login', 'LoginController@auth_user')->name('auth_user');


// TextMatch Routes
Route::resource('admin/textmatch', 'TextMatchController', ['except' => [
    'show'
]]);
Route::get('admin/textmatch/delete-confirmation/{textmatch}', 'TextMatchController@deleteConfirmation')->name('textmatch.deleteConfirmation');
